$(function(){
  'use strict'
  if(window.matchMedia('(min-width: 992px)').matches) {
    $('.weddinc-navbar .active').removeClass('show');
  }
  $('.weddinc-header .dropdown > a').on('click', function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  });
  $('.weddinc-navbar .with-sub').on('click', function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  });
  $('.dropdown-menu .weddinc-header-arrow').on('click', function(e){
    e.preventDefault();
    $(this).closest('.dropdown').removeClass('show');
  });
  $('#weddincnav, #weddincnavbar').on('click', function(e){
    e.preventDefault();
    $('body').addClass('weddinc-navbar-show');
  });
  $('body').append('<div class="weddinc-navbar-backdrop"></div>');
  $('.weddinc-navbar-backdrop').on('click touchstart', function(){
    $('body').removeClass('weddinc-navbar-show');
  });
  $(document).on('click touchstart', function(e){
    e.stopPropagation();
    var dropTarg = $(e.target).closest('.weddinc-header .dropdown').length;
    if(!dropTarg) {
      $('.weddinc-header .dropdown').removeClass('show');
    }
    if(window.matchMedia('(min-width: 992px)').matches) {
      var navTarg = $(e.target).closest('.weddinc-navbar .nav-item').length;
      if(!navTarg) {
        $('.weddinc-navbar .nav-item').removeClass('show');
      }
    }
  });
});