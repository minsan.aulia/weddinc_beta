<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactVendor extends Model
{
    protected $table = 'contact_vendors';

    protected $fillable = [
        'vendor_id', 'wa', 'telp', 'fb', 'twitter', 'ig'
    ];

    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
}
