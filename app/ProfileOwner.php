<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profileOwner extends Model
{
    protected $table = 'profile_owners';

    protected $fillable = [
        'owner_id', 'name', 'call_name', 'no_telp', 'address', 'city', 'type_identity', 'no_identity', 'photo_identity', 'photo_withIdentity', 'no_npwp', 'photo_npwp'
    ];

    public function Owner()
    {
        return $this->belongsTo('App\Owner');
    }
}
