<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = [
        'name', 'description', 'vendor_id', 'photo_1', 'photo_2', 'photo_3', 'photo_4', 'photo_5', 'photo_6'
    ];

    public function vendor() 
    {
        return $this->belongsTo('App\Vendor', 'vendor_id');
    }

    public function profileVendor() 
    {
        return $this->belongsTo('App\ProfileVendor', 'vendor_id', 'vendor_id');
    }
}
