<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileVendor extends Model
{
    protected $table = 'profile_vendors';

    protected $fillable = [
        'vendor_id', 'photo', 'cover', 'alamat', 'tentang',
    ];

    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
}
