<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public function category()
    {
        return $this->belongsTo('App\FaqCategory');
    }
}
