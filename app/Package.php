<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Vendor;
use App\ProfileVendor;

class Package extends Model
{
    protected $fillable = [
        'vendors_id', 'name', 'description', 'price', 'photo', 'category_id', 'city_id'
    ];

    public function vendor() 
    {
        return $this->belongsTo(Vendor::class, 'vendors_id');
    }

    public function profile_vendor() 
    {
        return $this->belongsTo(ProfileVendor::class, 'vendors_id');
    }
    
}
