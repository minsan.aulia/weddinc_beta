<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Auth;

class Wishlist
{
    public static function isPackageExists($package_id)
    {
        $user = Auth::user();

        $status=DB::table('wishlists')->where('users_id', $user->id)
                ->where('packages_id', $package_id)
                ->first();
        
        if(isset($status->users_id) and isset($status->packages_id)) {
            return true;
        }
                
        return false;
    }
    
}