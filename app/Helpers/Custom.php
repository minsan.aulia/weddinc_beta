<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Custom {
    public static function changeNameImage($image) {
        $input['photo'] = $image->getClientOriginalName();

        // Cek upload image
        $extensionImage = explode('.', $input['photo']);
        $extensionImage = strtolower(end($extensionImage));

        // Create email as user photo
        $email = explode('@', session('email'));
        $email = strtolower($email[0]);

        //generate unique name for photos
        $newNameImage = $email;
        $newNameImage .= '_';
        $newNameImage .= uniqid();
        $newNameImage .= '.';
        $newNameImage .= $extensionImage;

        return $newNameImage;
    }

    public static function storeImage($image, $nameImage, $directory){
        $destinationPath = public_path('/uploads/' . $directory); // location saving images

        $image->move($destinationPath , $nameImage);
    }

    public static function convertNormalDate($date)
    {
        $origDate = strtotime($date);
        return date("j F Y", $origDate);
    }
}
