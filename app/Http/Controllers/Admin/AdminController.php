<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\UserOwner;
use App\Owner;
use App\Vendor;
use App\Rekening;
use App\ProfileVendor;
use App\ContactVendor;
use App\FaqCategory;
use App\Faq;

class AdminController extends Controller
{   
    public function index() {
        $user = DB::table('users')->get();
        $admin = DB::table('admins')->get();
        $vendor = DB::table('vendors')->get();
        $owner = DB::table('owners')->get();
        return view('admin.index',['user' => $user, 'admin' => $admin, 'vendor' => $vendor, 'owner' => $owner]);
    }
    public function data_user() {
        $user = DB::table('users')->get();
        return view('admin.masterdata.user.index',['user' => $user]);
    }
    public function data_user_store(Request $request){
        DB::table('users')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'jenis_kelamin' => $request->jenis_kelamin,
            'telepon' => $request->telepon,
            'password' => Hash::make($request['password'])
        ]);
        return redirect()->route('data_user.admin');
    }
    public function data_user_edit($id) {
	    $user = DB::table('users')->where('id',$id)->get();
        return view('admin.masterdata.user.edit',['user' => $user]);
    }
    public function data_user_update(Request $request)
    {
        DB::table('users')->where('id',$request->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'jenis_kelamin' => $request->jenis_kelamin,
            'telepon' => $request->telepon
        ]);
        return redirect()->route('data_user.admin');
    }
    public function data_user_destroy($id) {
    	DB::table('users')->where('id',$id)->delete();
        return redirect()->route('data_user.admin');
    }
    public function data_vendor() {
        $vendor = DB::table('vendors')->get();
        return view('admin.masterdata.vendor.index',['vendor' => $vendor]);
    }
    public function data_vendor_store(Request $request){
        $request->validate([
            'email' => 'required|string|email|max:255|unique:user_owners',
            'password' => 'required|string|min:6',
            'name' => 'required|string|max:50',
            'call_name' => 'required|string|max:10',
            'no_telp' => 'required|string|min:5|max:14',
            'alamat' => 'required|string',
            'kota' => 'required|string|max:50',
            'type_identity' => 'required|string|max:15',
            'no_identity' => 'required|string|max:50',
            'photo_identity' => 'required|image|mimes:jpeg,png,jpg|max:3072',
            'no_npwp' => 'required|string|max:50',
            'photo_npwp' => 'required|image|mimes:jpeg,png,jpg|max:3072',
            'name_vendor' => 'required|string|max:100',
            'category' => 'required|string|max:50',
            'area' => 'required|string|max:50',
            'name_bank' => 'required|string|max:25',
            'no_rek' => 'required|string|max:255',
            'name_owner_bank' => 'required|string|max:50',
            'photo_rek' => 'required|image|mimes:jpeg,png,jpg|max:3072',
            'photo_suratKuasa' => 'image|mimes:jpeg,png,jpg|max:3072',
        ]);
            
        $photo_identity = $this->changeNameImage($request->file('photo_identity'), 'identity');
        $photo_withIdentity = $this->changeNameImage($request->file('photo_withIdentity'), 'withIdentity');
        $photo_npwp = $this->changeNameImage($request->file('photo_npwp'), 'npwp');
        $photo_rek = $this->changeNameImage($request->file('photo_rek'), 'rekening');
        if ($request->hasFile('photo_suratKuasa')) { //check available file
            $photo_suratKuasa = $this->changeNameImage($request->file('photo_suratKuasa'), 'suratKuasa');
        } else {
            $photo_suratKuasa = NULL;
        }
    
        $user_owner = userOwner::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
            
        $owner = owner::create([
            'user_owners_id' => $user_owner->id,
            'name' => $request->name,
            'call_name' => $request->call_name,
            'no_telp' => $request->no_telp,
            'alamat' => $request->alamat,
            'kota' => $request->kota,
            'type_identity' => $request->type_identity,
            'no_identity' => $request->no_identity,
            'photo_identity' => $photo_identity,
            'photo_withIdentity' => $photo_withIdentity,
            'no_npwp' => $request->no_npwp,
            'photo_npwp' => $photo_npwp,
        ]);
        $vendor = vendor::create([
            'owners_id' => $owner->id,
            'name' => $request->name_vendor,
            'faq_category_id' => $request->faq_category_id,
            'area' => $request->area,
        ]);
    
        $rekening = rekening::create([
            'owners_id' => $owner->id,
            'vendors_id' => $vendor->id,
            'name_bank' => $request->name_bank,
            'no_rek' => $request->no_rek,
            'name_owner_bank' => $request->name_owner_bank,
            'dp' => $request->dp,
            'photo_rek' => $photo_rek,
            'status_rek' => $request->status_rek,
            'photo_suratKuasa' => $photo_suratKuasa,
        ]);
    
        profileVendor::create([
            'vendors_id' => $vendor->id,
        ]);
    
        contactVendor::create([
            'vendors_id' => $vendor->id,
        ]);
    
        // function upload images to storage
            
        $this->uploadImage($request->file('photo_identity'), $photo_identity, 'identity');
        $this->uploadImage($request->file('photo_withIdentity'), $photo_withIdentity, 'withIdentity');
        $this->uploadImage($request->file('photo_npwp'), $photo_npwp, 'npwp');
        $this->uploadImage($request->file('photo_rek'), $photo_rek, 'rekening');
        if ($request->hasFile('photo_suratKuasa')) { //check available file
            $this->uploadImage($request->file('photo_suratKuasa'), $photo_suratKuasa, 'suratKuasa');
        }
            
        return redirect()->route('data_vendor.admin');
    }
    public function changeNameImage($image){
        $input['fotoname'] = $image->getClientOriginalName();
		
		// Cek upload gambar
		$extensionImageValid = ['jpg', 'jpeg', 'png'];
		$extensionImage = explode('.', $input['fotoname']);
		$extensionImage = strtolower(end($extensionImage));

        //generate unique name for photos
		$newNameImage = uniqid();
		$newNameImage .= '.';
		$newNameImage .= $extensionImage;

        return $newNameImage;
    }
    public function uploadImage($image, $nameImage, $status){
        switch ($status){
            case 'identity':
                $destinationPath = public_path('/images/identity'); // location saving images
                break;
            case 'withIdentity':
                $destinationPath = public_path('/images/withIdentity'); // location saving images
                break;
            case 'npwp':
                $destinationPath = public_path('/images/npwp'); // location saving images
                break;
            case 'rekening':
                $destinationPath = public_path('/images/rekening'); // location saving images
                break;
            case 'suratKuasa':
                $destinationPath = public_path('/images/suratKuasa'); // location saving images
                break;
        }
        
        $image->move($destinationPath , $nameImage);
    }
    public function data_vendor_edit($id) {
	    $vendor = DB::table('vendors')->where('id',$id)->get();
        return view('admin.masterdata.vendor.edit',['vendor' => $vendor]);
    }
    public function data_vendor_update(Request $request)
    {
        DB::table('vendors')->where('id',$request->id)->update([
            'name' => $request->name,
            'owners_id' => $request->owners_id,
            'faq_category_id' => $request->faq_category_id,
            'type' => $request->type,
            'area' => $request->area
        ]);
        return redirect()->route('data_vendor.admin');
    }
    public function data_vendor_destroy($id) {
    	DB::table('vendors')->where('id',$id)->delete();
        return redirect()->route('data_vendor.admin');
    }
    public function data_admin() {
        $admin = DB::table('admins')->get();
        return view('admin.masterdata.admin.index',['admin' => $admin]);
    }
    public function data_admin_store(Request $request){
        DB::table('admins')->insert([
            'user_admins_id' => $request->user_admins_id,
            'name' => $request->name,
            'no_telp' => $request->no_telp,
            'alamat' => $request->alamat,
            'position' => $request->position,
            'email' => $request->email,
            'password' => Hash::make($request['password'])
        ]);
        return redirect()->route('data_admin.admin');
    }
    public function data_admin_edit($id) {
	    $admin = DB::table('admins')->where('id',$id)->get();
        return view('admin.masterdata.admin.edit',['admin' => $admin]);
    }
    public function data_admin_update(Request $request)
    {
        DB::table('admins')->where('id',$request->id)->update([
            'user_admins_id' => $request->user_admins_id,
            'name' => $request->name,
            'no_telp' => $request->no_telp,
            'alamat' => $request->alamat,
            'position' => $request->position,
            'email' => $request->email
        ]);
        return redirect()->route('data_admin.admin');
    }
    public function data_admin_destroy($id) {
    	DB::table('admins')->where('id',$id)->delete();
        return redirect()->route('data_admin.admin');
    }
    public function about_user() {
        $about_user = DB::table('documents')->where('id_document','10011')->get();
        return view('admin.documentforuser.about.index',['about_user' => $about_user]);
    }
    public function about_user_update(Request $request)
    {
        DB::table('documents')->where('id_document','10011')->update([
            'content' => $request->content
        ]);
        return redirect()->route('about_user.admin');
    }
    public function about_vendor() {
        $about_vendor = DB::table('documents')->where('id_document','20011')->get();
        return view('admin.documentforvendor.about.index',['about_vendor' => $about_vendor]);
    }
    public function about_vendor_update(Request $request)
    {
        DB::table('documents')->where('id_document','20011')->update([
            'content' => $request->content
        ]);
        return redirect()->route('about_vendor.admin');
    }
    public function termscondition_user() {
        $termscondition_user = DB::table('documents')->where('id_document','10013')->get();
        return view('admin.documentforuser.termscondition.index',['termscondition_user' => $termscondition_user]);
    }
    public function termscondition_user_update(Request $request)
    {
        DB::table('documents')->where('id_document','10013')->update([
            'content' => $request->content
        ]);
        return redirect()->route('termscondition_user.admin');
    }
    public function termscondition_vendor() {
        $termscondition_vendor = DB::table('documents')->where('id_document','20013')->get();
        return view('admin.documentforvendor.termscondition.index',['termscondition_vendor' => $termscondition_vendor]);
    }
    public function termscondition_vendor_update(Request $request)
    {
        DB::table('documents')->where('id_document','20013')->update([
            'content' => $request->content
        ]);
        return redirect()->route('termscondition_vendor.admin');
    }
    public function privacypolicy_user() {
        $privacypolicy_user = DB::table('documents')->where('id_document','10012')->get();
        return view('admin.documentforuser.privacypolicy.index',['privacypolicy_user' => $privacypolicy_user]);
    }
    public function privacypolicy_user_update(Request $request)
    {
        DB::table('documents')->where('id_document','10012')->update([
            'content' => $request->content
        ]);
        return redirect()->route('privacypolicy_user.admin');
    }
    public function privacypolicy_vendor() {
        $privacypolicy_vendor = DB::table('documents')->where('id_document','20012')->get();
        return view('admin.documentforvendor.privacypolicy.index',['privacypolicy_vendor' => $privacypolicy_vendor]);
    }
    public function privacypolicy_vendor_update(Request $request)
    {
        DB::table('documents')->where('id_document','20012')->update([
            'content' => $request->content
        ]);
        return redirect()->route('privacypolicy_vendor.admin');
    }
    public function faq_user() {
        $faq_category_user = DB::table('faq_categories')->where('categories_for','0')->get();
        $faq_user = DB::table('faqs')
                        ->join('faq_categories', 'faq_categories.id', '=', 'faqs.faq_category_id')
                        ->select('faqs.*','faq_categories.*', 'faq_categories.id as id_faq_categories', 'faqs.id as id_faqs')
                        ->where('faq_for','0')->get();
        $list_category_user = DB::table('faq_categories')->where('categories_for','0')->get();
        return view('admin.documentforuser.faq.index',['faq_category_user' => $faq_category_user,'faq_user' => $faq_user,'list_category_user' => $list_category_user]);
    }
    public function faq_category_user_store(Request $request){
        DB::table('faq_categories')->insert([
            'categories_for' => $request->categories_for,
            'name' => $request->name
        ]);
        return redirect()->route('faq_user.admin');
    }
    public function faq_category_user_edit($id) {
	    $faq_category_user = DB::table('faq_categories')->where('id',$id)->get();
        return view('admin.documentforuser.faq.edit_category',['faq_category_user' => $faq_category_user]);
    }
    public function faq_category_user_update(Request $request)
    {
        DB::table('faq_categories')->where('id',$request->id)->update([
            'name' => $request->name
        ]);
        return redirect()->route('faq_user.admin');
    }
    public function faq_category_user_destroy($id) {
    	DB::table('faq_categories')->where('id',$id)->delete();
        return redirect()->route('faq_user.admin');
    }
    public function faq_user_store(Request $request){
        DB::table('faqs')->insert([
            'faq_for' => $request->faq_for,
            'faq_category_id' => $request->faq_category_id,
            'ask' => $request->ask,
            'question' => $request->question
        ]);
        return redirect()->route('faq_user.admin');
    }
    public function faq_user_edit($id) {
	    $faq_user = DB::table('faqs')->where('id',$id)->get();
        $list_category_user = DB::table('faq_categories')->where('categories_for','0')->get();
        return view('admin.documentforuser.faq.edit',['faq_user' => $faq_user,'list_category_user' => $list_category_user]);
    }
    public function faq_user_update(Request $request)
    {
        DB::table('faqs')->where('id',$request->id)->update([
            'faq_category_id' => $request->faq_category_id,
            'ask' => $request->ask,
            'question' => $request->question
        ]);
        return redirect()->route('faq_user.admin');
    }
    public function faq_user_destroy($id) {
    	DB::table('faqs')->where('id',$id)->delete();
        return redirect()->route('faq_user.admin');
    }
    public function faq_vendor() {
        $faq_category_vendor = DB::table('faq_categories')->where('categories_for','1')->get();
        $faq_vendor = DB::table('faqs')
                        ->join('faq_categories', 'faq_categories.id', '=', 'faqs.faq_category_id')
                        ->select('faqs.*','faq_categories.*', 'faq_categories.id as id_faq_categories', 'faqs.id as id_faqs')
                        ->where('faq_for','1')->get();
        $list_category_vendor = DB::table('faq_categories')->where('categories_for','1')->get();
        return view('admin.documentforvendor.faq.index',['faq_category_vendor' => $faq_category_vendor,'faq_vendor' => $faq_vendor,'list_category_vendor' => $list_category_vendor]);
    }
    public function faq_category_vendor_store(Request $request){
        DB::table('faq_categories')->insert([
            'categories_for' => $request->categories_for,
            'name' => $request->name
        ]);
        return redirect()->route('faq_vendor.admin');
    }
    public function faq_category_vendor_edit($id) {
	    $faq_category_vendor = DB::table('faq_categories')->where('id',$id)->get();
        return view('admin.documentforvendor.faq.edit_category',['faq_category_vendor' => $faq_category_vendor]);
    }
    public function faq_category_vendor_update(Request $request)
    {
        DB::table('faq_categories')->where('id',$request->id)->update([
            'name' => $request->name
        ]);
        return redirect()->route('faq_vendor.admin');
    }
    public function faq_category_vendor_destroy($id) {
    	DB::table('faq_categories')->where('id',$id)->delete();
        return redirect()->route('faq_vendor.admin');
    }
    public function faq_vendor_store(Request $request){
        DB::table('faqs')->insert([
            'faq_for' => $request->faq_for,
            'faq_category_id' => $request->faq_category_id,
            'ask' => $request->ask,
            'question' => $request->question
        ]);
        return redirect()->route('faq_vendor.admin');
    }
    public function faq_vendor_edit($id) {
	    $faq_vendor = DB::table('faqs')->where('id',$id)->get();
        $list_category_vendor = DB::table('faq_categories')->where('categories_for','1')->get();
        return view('admin.documentforvendor.faq.edit',['faq_vendor' => $faq_vendor,'list_category_vendor' => $list_category_vendor]);
    }
    public function faq_vendor_update(Request $request)
    {
        DB::table('faqs')->where('id',$request->id)->update([
            'faq_category_id' => $request->faq_category_id,
            'ask' => $request->ask,
            'question' => $request->question
        ]);
        return redirect()->route('faq_vendor.admin');
    }
    public function faq_vendor_destroy($id) {
    	DB::table('faqs')->where('id',$id)->delete();
        return redirect()->route('faq_vendor.admin');
    }
}