<?php

namespace App\Http\Controllers\AuthOwner;

use App\Category;
use App\City;
use App\profileOwner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Owner;
use App\Vendor;
use App\Rekening;
use App\ProfileVendor;
use App\ContactVendor;

class RegisterController extends Controller
{
    use AuthenticatesUsers;

    protected $guard = 'owner';

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function guard()
    {
        return auth()->guard('owner');
    }

    public function show()
    {
        $city = city::all();
        $category = category::all();
        $privacypolicy_vendor = DB::table('documents')->where('id_document','20012')->get();
        return view('authUser_owner.register',['privacypolicy_vendor' => $privacypolicy_vendor,'city' => $city, 'category' => $category]);
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|unique:owners',
            'password' => 'required|string|min:6',
            'name' => 'required|string|max:50',
            'no_telp' => 'required|string|min:5|max:14',
            'name_vendor' => 'required|string|max:100',
            'category' => 'required|string|max:50',
            'area' => 'required|string|max:50',
            ]);

        $owner = owner::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $vendor = vendor::create([
            'owner_id' => $owner->id,
            'name' => $request->name_vendor,
            'category' => $request->category,
            'area' => $request->area,
        ]);

        profileOwner::create([
            'owner_id' => $owner->id,
            'name' => $request->name,
            'no_telp' => $request->no_telp,
        ]);

        rekening::create([
            'owner_id' => $owner->id,
            'vendor_id' => $vendor->id,
        ]);

        profileVendor::create([
            'vendor_id' => $vendor->id,
        ]);

        contactVendor::create([
            'vendor_id' => $vendor->id,
        ]);

        return redirect()->route('loginPage.owner')->with('success','Registration Success');
    }
}
