<?php

namespace App\Http\Controllers\AuthOwner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $guard = 'owner';

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function guard()
    {
        return Auth::guard('owner');
    }

    public function show()
    {
        return view('authUser_owner.login');
    }

    public function login(Request $request)
    {
        if (Auth::guard('owner')->attempt(['email' => $request->email, 'password' => $request->password ])) {
            return redirect()->intended(route('index.owner'));
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('owner')->logout();
        return redirect()->route('loginPage.owner');
    }
}
