<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\FaqCategory;
use App\Faq;

class HomepageController extends Controller
{
    public function about()
    {
        $about_user = DB::table('documents')->where('id_document','10011')->get();
        $about_vendor = DB::table('documents')->where('id_document','20011')->get();
    	return view('about',['about_user' => $about_user, 'about_vendor' => $about_vendor]);
    }

    public function budgeting()
    {
    	return view('budgeting');
    }
    
    public function contact_us()
    {
    	return view('contact_us');
    }

    public function faq()
    {
    	$categories = FaqCategory::where('categories_for','0')->get();
        return view('faq',['categories' => $categories]);
    }

    public function privacy_policy()
    {
        $privacypolicy_user = DB::table('documents')->where('id_document','10012')->get();
    	return view('privacy_policy', ['privacypolicy_user' => $privacypolicy_user]);
    }

    public function terms_condition()
    {
        $termscondition_user = DB::table('documents')->where('id_document','10013')->get();
    	return view('terms_condition', ['termscondition_user' => $termscondition_user]);
    }
}