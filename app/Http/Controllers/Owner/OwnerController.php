<?php

namespace App\Http\Controllers\Owner;

use App\Category;
use App\City;
use App\Helpers\Custom;
use App\Http\Controllers\Controller;
use App\Owner;
use App\ProfileOwner;
use App\ProfileVendor;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OwnerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function index()
    {
        $id = Auth::user()->id;

        $info = ProfileOwner::select('name')->where('owner_id', $id)->get()->first();
        session(['name' => $info->name]); //set global session for name, used on owner_layout

        $info = Owner::select('email')->where('id', $id)->get()->first();
        session(['email' => $info->email]); //set global session for email, used on custom Helper

        return view('owner.index');
    }

    public function logout()
    {
//        Auth::guard('owner')->logout();
        \auth()->logout();
    }

    public function detail()
    {
        $id = Auth::user()->id;

        $vendor = Vendor::select('*')->where('owner_id', $id)->get()->first();
        $profileVendor = ProfileVendor::select('*')->where('vendor_id', $vendor->id)->get()->first();
        $city = City::select('name')->where('id', $vendor->area)->get()->first();
        $category = Category::select('name')->where('id', $vendor->category)->get()->first();

        return view('owner.account_detail', compact('vendor', 'profileVendor', 'city', 'category'));
    }

    public function profile()
    {
        $id = Auth::user()->id;

        $data = ProfileOwner::addSelect(['email' => Owner::select('email')
                    ->whereColumn('owner_id', 'owners.id')
                    ])->where('owner_id', $id)->get()->first();
        $city = City::all();

        return view('owner.account_owner', compact('data', 'city'));
    }

    public function updateProfile(Request $request)
    {
        $id_owner = Auth::user()->id;
        $id = ProfileOwner::select('id')->where('owner_id', $id_owner)->get()->first();

        $profileOwner = ProfileOwner::findOrFail($id->id);

        $photoIdentity = $profileOwner->photo_identity; //remove photo from storage if change or remove request
        if($request->remove_id || $request->hasFile('photo_id')) {
            $imgPath = public_path().'/uploads/photoIdentity/'. $photoIdentity;
            $photoIdentity = null;

            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }

        $photoNpwp = $profileOwner->photo_npwp; //remove photo from storage if change or remove request
        if($request->remove_npwp || $request->hasFile('photoNpwp')) {
            $imgPath = public_path().'/uploads/photoNpwp/'. $photoNpwp;
            $photoNpwp = null;

            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }

        //upload photo if have file change
        if($request->hasFile('photo_id')){
            $photoIdentity = Custom::changeNameImage($request->file('photo_id'));
            Custom::storeImage($request->file('photo_id'), $photoIdentity, 'photoIdentity');
        }
        if($request->hasFile('photoNpwp')){
            $photoNpwp = Custom::changeNameImage($request->file('photoNpwp'));
            Custom::storeImage($request->file('photoNpwp'), $photoNpwp, 'photoNpwp');
        }

        // Record to database
        ProfileOwner::updateOrCreate(['id' => $id->id], [
            'name' => $request->name,
            'no_telp' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'type_identity' => $request->type_id,
            'photo_identity' => $photoIdentity,
            'no_npwp' => $request->no_npwp,
            'photo_npwp' => $photoNpwp,
        ]);

        return redirect()->back()->with(['status' => 'The profile owner updated successfully.']);
    }
}
