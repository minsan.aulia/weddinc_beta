<?php

namespace App\Http\Controllers\Owner;

use App\Bank;
use App\Helpers\Custom;
use App\Http\Controllers\Controller;
use App\Rekening;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function index()
    {
        $id = Auth::user()->id;

        $data = Rekening::all()->where('owner_id', $id)->first();
        $bank = Bank::all();

        return view('owner.account_payment', compact('data', 'bank'));
    }

    public function update(Request $request)
    {
        $id_owner = Auth::user()->id;
        $id_vendor = Vendor::select('id')->where('owner_id', $id_owner)->get()->first();
        $id = Rekening::select('id')->where('owner_id', $id_owner)->where('vendor_id', $id_vendor->id)->get()->first();

        $rekening = Rekening::findOrFail($id->id);

        $photoRek = $rekening->photo_rek; //remove photo from storage if change or remove request
        if($request->remove_rek || $request->hasFile('photo_rek')) {
            $imgPath = public_path().'/uploads/photoRekening/'. $photoRek;
            $photoRek = null;

            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }

        $photoKuasa = $rekening->photo_suratKuasa; //remove photo from storage if change or remove request
        if($request->remove_kuasa || $request->hasFile('photo_suratKuasa')) {
            $imgPath = public_path().'/uploads/photoSuratKuasa/'. $photoKuasa;
            $photoKuasa = null;

            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }

        //upload photo if have file change
        if($request->hasFile('photo_rek')){
            $photoRek = Custom::changeNameImage($request->file('photo_rek'));
            Custom::storeImage($request->file('photo_rek'), $photoRek, 'photoRekening');
        }
        if($request->hasFile('photo_suratKuasa')){
            $photoKuasa = Custom::changeNameImage($request->file('photo_suratKuasa'));
            Custom::storeImage($request->file('photo_suratKuasa'), $photoKuasa, 'photoSuratKuasa');
        }

        Rekening::updateOrCreate(['id' => $id->id], [
            'name_bank' => $request->name_bank,
            'no_rek' => $request->no_rek,
            'photo_rek' => $photoRek,
            'photo_suratKuasa' => $photoKuasa,
        ]);

        return redirect()->back()->with(['status' => 'Successfully']);
    }
}
