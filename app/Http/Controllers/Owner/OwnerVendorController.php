<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Vendor;
use App\ProfileVendor;
use App\ContactVendor;
use App\Category;
use App\City;
use App\FaqCategory;
use App\Faq;

class OwnerVendorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function statistics()
    {
        return view('owner.statistics');
    }

    public function discussion()
    {
        return view('owner.discussion');
    }

    public function gallery()
    {
        return view('owner.account_gallery');
    }

    public function view_gallery()
    {
        return view('owner.view_gallery');
    }

    public function gallery_detail()
    {
        return view('owner.account_gallerydet');
    }

    public function rating()
    {
        return view('owner.account_star_rate');
    }

    public function review()
    {
        return view('owner.account_review');
    }

    public function order()
    {
        return view('owner.view_order');
    }

    public function view_order()
    {
        return view('owner.view_order');
    }

    public function view_order_all()
    {
        return view('owner.view_order_all');
    }

    public function notification()
    {
        return view('owner.notification');
    }

    public function notification_detail()
    {
        return view('owner.notification_detail');
    }

    public function faq()
    {
        $categories = FaqCategory::where('categories_for','1')->get();
        return view('owner.faq',['categories' => $categories]);
    }

    public function terms_condition() {
        $termscondition_vendor = DB::table('documents')->where('id_document','20013')->get();
        return view('owner.terms_condition',['termscondition_vendor' => $termscondition_vendor]);
    }

    public function privacy_policy() {
        $privacypolicy_vendor = DB::table('documents')->where('id_document','20012')->get();
        return view('owner.privacy_policy',['privacypolicy_vendor' => $privacypolicy_vendor]);
    }

    public function about()
    {
        return view('owner.about');
    }

    public function contact_us()
    {
        return view('owner.contact_us');
    }
}
