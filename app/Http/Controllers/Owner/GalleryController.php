<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Gallery;
use Auth;

class GalleryController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function index()
    {
        $vendor_id = Auth::id();
        $galleries = Gallery::where('vendor_id', $vendor_id)->get();
        return view('owner.account_gallery', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    private function saveImage(Request $request, $name, $remove_name)
    {
        $file_name = null;
        // check remove image
        if($request->input($remove_name) == 1) {
            $imgPath = public_path().'/uploads/galleries/'. $file_name;

            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }
        else if ($request->has($name)) { // Check if an image has been uploaded
            // Get image file
            $image = $request->file($name);
            // Make a image name based on user name and current timestamp
            $img_name = Str::slug($name).'_'.time();
            // Define folder path
            $folder = '/uploads/galleries/';
            // Upload image
            $this->uploadOne($image, $folder, 'public', $img_name);
            // Make a file image will be stored [ file name + file extension]
            $file_name = $img_name. '.' . $image->getClientOriginalExtension();
        }

        return $file_name;
    }

    public function store(Request $request)
    {
        // Form validation
        $validator = Validator::make($request->all(), [
            'gallery_name'    => 'required',
            'description'     => 'required',
            'photo_1'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_2'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_3'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_4'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_5'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_6'         => 'mimes:jpeg,png,jpg|max:2048'
        ]);

        if($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('addGalleryFailed', 'Failed');
        }

        // Set
        $vendor_id = Auth::id();
        $name = $request->input('gallery_name');
        $description = $request->input('description');

        for($i=1; $i<=6; $i++) {
            $photo_name = "photo_".$i;
            $remove_name = "removephoto_".$i;
            $file_name[$i] = $this->saveImage($request, $photo_name, $remove_name);
        }

        // Record to database
        $gallery = Gallery::create([
            'vendor_id'    => $vendor_id,
            'name'          => $name,
            'description'   => $description,
            'photo_1'         => $file_name[1],
            'photo_2'         => $file_name[2],
            'photo_3'         => $file_name[3],
            'photo_4'         => $file_name[4],
            'photo_5'         => $file_name[5],
            'photo_6'         => $file_name[6]
        ]);

        // Return user back and show a flash message
        return redirect()->back();
    }

    public function detail($id)
    {
        $gallery = Gallery::find($id);
        return view('owner.account_gallerydet', compact('gallery'));
    }

    public function show(Request $request)
    {
        $id = $request->input('id');
        $gallery = Gallery::find($id);
        return response()->json($gallery);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // Form validation
        $validator = Validator::make($request->all(), [
            'gallery_name'    => 'required',
            'description'     => 'required',
            'photo_1'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_2'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_3'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_4'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_5'         => 'mimes:jpeg,png,jpg|max:2048',
            'photo_6'         => 'mimes:jpeg,png,jpg|max:2048'
        ]);

        if($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('updateGalleryFailed', 'Failed');
        }

        // Set
        $id = $request->input('gallery_id');
        $name = $request->input('gallery_name');
        $description = $request->input('description');

        // Record to database
        $gallery = Gallery::find($id);
        $gallery->update([
            'name'          => $name,
            'description'   => $description
        ]);

        for($i=1; $i<=6; $i++) {
            $photo_name = "photo_".$i;
            $remove_name = "removephoto_".$i;
            if($request->has($photo_name) || $request->input($remove_name)==1) {
                $file_name = $this->saveImage($request, $photo_name, $remove_name);
                $gallery->update([$photo_name => $file_name]);
                // echo "<script>alert('Berhasil')</script>";
            }
        }

        // Return user back and show a flash message
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $id = $request->input('gallery_id');
        $gallery = Gallery::findOrFail($id);

        $imgName[1] = $gallery->photo_1;
        $imgName[2] = $gallery->photo_2;
        $imgName[3] = $gallery->photo_3;
        $imgName[4] = $gallery->photo_4;
        $imgName[5] = $gallery->photo_5;
        $imgName[6] = $gallery->photo_6;

        for($i=1; $i<=6; $i++) {
            $imgPath = public_path().'/uploads/galleries/'. $imgName[$i];
            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }

        $gallery->delete();

        return redirect()->route('gallery.owner');
    }
}
