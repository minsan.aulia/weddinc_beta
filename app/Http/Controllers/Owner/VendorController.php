<?php

namespace App\Http\Controllers\Owner;

use App\Category;
use App\City;
use App\ContactVendor;
use App\Helpers\Custom;
use App\Http\Controllers\Controller;
use App\ProfileVendor;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function index()
    {
        $id = Auth::user()->id;

        $data = DB::table('vendors')
            ->join('profile_vendors', 'vendors.id', '=', 'profile_vendors.vendor_id')
            ->join('contact_vendors', 'vendors.id', '=', 'contact_vendors.vendor_id')
            ->select('vendors.*', 'contact_vendors.*', 'profile_vendors.*')
            ->where('vendors.owner_id', $id)
            ->get()->first();
        $city = city::all();
        $category = category::all();

        return view('owner.vendor', compact('data', 'city', 'category'));
    }

    public function update(Request $request)
    {
        $id_owner = Auth::user()->id;
        $id = Vendor::select('id')->where('owner_id', $id_owner)->get()->first();

        $profileVendor = ProfileVendor::findOrFail($id->id);

        $photoProfile = $profileVendor->photo; //remove photo from storage if change or remove request
        if($request->remove_photo || $request->hasFile('photo')) {
            $imgPath = public_path().'/uploads/photoProfile/'. $photoProfile;
            $photoProfile = null;

            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }

        $photoCover = $profileVendor->cover; //remove photo from storage if change or remove request
        if($request->remove_cover || $request->hasFile('cover')) {
            $imgPath = public_path().'/uploads/photoCover/'. $photoCover;
            $photoCover = null;

            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }

        //upload photo if have file change
        if ($request->hasFile('photo')) { //check available file
            $photoProfile = Custom::changeNameImage($request->file('photo'));
            Custom::storeImage($request->file('photo'), $photoProfile, 'photoProfile');
        }
        if ($request->hasFile('cover')) { //check available file
            $photoCover = Custom::changeNameImage($request->file('cover'));
            Custom::storeImage($request->file('cover'), $photoCover, 'photoCover');
        }

        Vendor::updateOrCreate(['id' => $id->id], [
        'category' => $request->category,
        'area' => $request->area,
        'activated' => $request->activated == "on" ? '1':'0',
        ]);

        ProfileVendor::updateOrCreate(['vendor_id' => $id->id], [
            'photo' => $photoProfile,
            'cover' => $photoCover,
            'alamat' => $request->alamat,
            'tentang' => $request->tentang,
        ]);


        $contactVendor = contactVendor::find($id->id);
        $contactVendor->wa = $request->wa;
        $contactVendor->telp = $request->telp;
        $contactVendor->fb = $request->fb;
        $contactVendor->twitter = $request->twitter;
        $contactVendor->ig = $request->ig;
        $contactVendor->save();

        return redirect()->back()->with('success','Registration Success');
    }
}
