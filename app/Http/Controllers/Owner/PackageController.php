<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Package;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\Category;
use App\City;

class PackageController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $vendor_id = $user->id;
        $packages = Package::where('vendors_id', $vendor_id)->get();
        $categories = Category::all('id', 'name');
        $cities = City::all('id', 'name');

        return view('owner.account_pricelist', compact('packages', 'categories', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form validation
        $request->validate([
            'add_name'          => 'required',
            'add_description'   => 'required',
            'add_price'         => 'required',
            'add_photo'         => 'mimes:jpeg,png,jpg|max:2048',
            'add_category'      => 'required',
            'add_city'         => 'required'
        ]);

        $user = Auth::user();
        // Set
        $vendor_id = $user->id;
        $name = $request->input('add_name');
        $description = $request->input('add_description');
        $price = str_replace(".", "", $request->input('add_price'));
        $category = $request->input('add_category');
        $city = $request->input('add_city');
        $file_name = null;

        // Check if a profile image has been uploaded
        if ($request->has('add_photo')) {
            // Get image file
            $image = $request->file('add_photo');
            // Make a image name based on user name and current timestamp
            $img_name = Str::slug($name).'_'.time();
            // Define folder path
            $folder = '/uploads/packages/';
            // Upload image
            $this->uploadOne($image, $folder, 'public', $img_name);
            // Make a file image will be stored [ file name + file extension]
            $file_name = $img_name. '.' . $image->getClientOriginalExtension();
        }

        // Record to database
        $package = Package::create([
            'vendors_id'    => $vendor_id,
            'name'          => $name,
            'description'   => $description,
            'price'         => $price,
            'photo'         => $file_name,
            'category_id'   => $category,
            'city_id'       => $city
        ]);

        // Return user back and show a flash message
        return redirect()->back()->with(['status' => 'Package added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::find($id);
        return $package;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Form validation
        $request->validate([
            'edt_name'          => 'required',
            'edt_description'   => 'required',
            'edt_price'         => 'required',
            'edt_photo'         => 'mimes:jpeg,png,jpg|max:2048',
            'edt_category'      => 'required',
            'edt_city'          => 'required'
        ]);

        $name = $request->input('edt_name');
        $description = $request->input('edt_description');
        $price = str_replace(".", "", $request->input('edt_price'));
        $category = $request->input('edt_category');
        $city = $request->input('edt_city');

        $package = Package::findOrFail($id);
        $file_name = $package->photo;

        if($request->input('edt_remove_photo') || $request->has('edt_photo')) {
            $imgPath = public_path().'/uploads/packages/'. $file_name;
            $file_name = null;

            if (is_file($imgPath)) {
                unlink($imgPath);
            }
        }

        if ($request->has('edt_photo')) { // Check if a profile image has been uploaded
            // Get image file
            $image = $request->file('edt_photo');
            // Make a image name based on user name and current timestamp
            $img_name = Str::slug($name).'_'.time();
            // Define folder path
            $folder = '/uploads/packages/';
            // Upload image
            $this->uploadOne($image, $folder, 'public', $img_name);
            // Make a file image will be stored [ file name + file extension]
            $file_name = $img_name. '.' . $image->getClientOriginalExtension();
        }

        // Record to database
        $package = Package::updateOrCreate(['id' => $id], [
            'name'          => $name,
            'description'   => $description,
            'price'         => $price,
            'photo'         => $file_name,
            'category_id'   => $category,
            'city_id'       => $city
        ]);

        // Return user back and show a flash message
        return redirect()->back()->with(['status' => 'The package updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Package::findOrFail($id);
        $pkgName = $package->name;
        $imgName = $package->photo;
        $imgPath = public_path().'/uploads/packages/'. $imgName;

        if (is_file($imgPath)) {
            unlink($imgPath);
        }

        $package->delete();

        return redirect()->back()->with('success', "Package '". $pkgName ."' was successfully removed");
    }
}
