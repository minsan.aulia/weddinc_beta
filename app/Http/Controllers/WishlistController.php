<?php

namespace App\Http\Controllers;

use App\Wishlist;
use Auth;

class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $wishlists = Wishlist::where('users_id', $user->id)->get();

        return view('user/wishlist', compact('wishlists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $user = Auth::user();

        $status=Wishlist::where('users_id', $user->id)
                ->where('packages_id',$id)
                ->first();

        if(isset($status->users_id) and isset($status->packages_id)) {
            return redirect()->back()->with('warning', "");
        }
        else {
            $wishlist = new Wishlist;

            $wishlist->users_id = $user->id;
            $wishlist->packages_id = $id;

            $wishlist->save();

            return redirect()->back()->with('success', "Package '". $wishlist->package->name."' added to your wishlist.");
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wishlist = Wishlist::findOrFail($id);
        $pkgName = $wishlist->package->name;
        $wishlist->delete();

        return redirect()->back()->with('success', "Package '". $pkgName ."' was successfully removed from wishlist");
    }
}
