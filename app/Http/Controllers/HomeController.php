<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Category;
use App\City;
use App\FaqCategory;
use App\Faq;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all('id', 'name');
        $cities = City::all('id', 'name');
        return view('user/home', compact('categories', 'cities'));
    }

    public function about()
    {   
        return view('/user/about');
    }
    
    public function account_detail()
    {   
        return view('/user/account_detail');
    }

    public function account_edit(Request $request, $id)
    {   
        // $user = User::where('id', $id)->firstOrFail();

        // $this->validate($request, [
        //     'name' => 'required',
        //     'email' => 'required',
        //     'jenis_kelamin' => 'required',
        //     'telepon' => 'required'
        // ]);

        // User::where('id', $user->id)
        //     ->update([
        //         'name' => $request->name,
        //         'email' => $request->email,
        //         'jenis_kelamin' => $request->jenis_kelamin,
        //         'telepon' => $request->telepon
        //     ]);

        // \Session::flash('flash_message', 'data anda berhasil ditambahkan');
        
        return view('/user/account_edit');
    }
    
    public function account()
    {   
        return view('/user/account');
    }

    public function cart_view()
    {   
        return view('/user/cart_view');
    }

    public function cart()
    {   
        return view('/user/cart');
    }

    public function contact_us()
    {   
        return view('/user/contact_us');
    }

    public function discussion()
    {   
        return view('/user/discussion');
    }
    
    public function faq()
    {   
        $categories = FaqCategory::where('categories_for','0')->get();
        return view('user.faq',['categories' => $categories]);
    }
    
    public function notification_detail()
    {   
        return view('/user/notification_detail');
    }

    public function notification()
    {   
        return view('/user/notification');
    }
    
    public function plan_budgetting()
    {   
        return view('/user/plan_budgetting');
    }

    public function order()
    {   
        return view('/user/order');
    }

    public function privacy_policy()
    {   
        return view('/user/privacy_policy');
    }

    public function reset_password()
    {   
        return view('/user/reset_password');
    }

    public function terms_condition()
    {   
        return view('/user/terms_condition');
    }

    public function vendors(Request $request)
    {   
        $qry_packages = Package::query();
        $current_category = '';
        $current_city = '';
        $current_price = '';

        if($request->input('category')) {
            $qry_packages->where('category_id', $request->input('category'));
            $current_category = $request->input('category');
        }
        if($request->input('city')) {
            $qry_packages->where('city_id', $request->input('city'));
            $current_city = $request->input('city');
        }
        if($request->input('price')) {
            switch($request->input('price')) {
                case "<1" :
                    $qry_packages->where('price', '>=', 0);
                    $qry_packages->where('price', '<=', 1000000);
                    break;
                case "1-2" :
                    $qry_packages->where('price', '>=', 1000000);
                    $qry_packages->where('price', '<=', 2000000);
                    break;
                case "2-3" :
                    $qry_packages->where('price', '>=', 2000000);
                    $qry_packages->where('price', '<=', 3000000);
                    break;
                case "3-4" :
                    $qry_packages->where('price', '>=', 3000000);
                    $qry_packages->where('price', '<=', 4000000);
                    break;
                case "4-5" :
                    $qry_packages->where('price', '>=', 4000000);
                    $qry_packages->where('price', '<=', 5000000);
                    break;
                case ">5" :
                    $qry_packages->where('price', '>', 5000000);
                    break;
            }
            $current_price = $request->input('price');
        }

        $packages = $qry_packages->get();
        $categories = Category::all('id', 'name');
        $cities = City::all('id', 'name');
        return view('/user/vendors', compact('packages', 'categories', 'cities', 'current_category', 'current_city', 'current_price'));
    }

    public function venues()
    {   
        return view('/user/venues');
    }

}
