<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = [
        'owner_id', 'name', 'category', 'type', 'area', 'activated'
    ];

    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }

    public function profileVendor()
    {
        return $this->hasOne('App\ProfileVendor');
    }

    public function contactVendor()
    {
        return $this->hasOne('App\ContactVendor');
    }

    public function rekening()
    {
        return $this->hasOne('App\Rekening');
    }
}
