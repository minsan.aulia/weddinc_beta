<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = [
        'users_id', 'packages_id'
    ];

    public function package() 
    {
        return $this->belongsTo('\App\Package', 'packages_id');
    }

    public function vendor() 
    {
        return $this->belongsTo('\App\Vendor', 'vendors_id');
    }
}
