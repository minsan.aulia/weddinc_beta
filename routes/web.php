<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Routing for Homepage
Route::get('/', function () {
    return view('homepage');
});
Route::get('about', 'HomepageController@about');
Route::get('budgeting', 'HomepageController@budgeting');
Route::get('contact_us', 'HomepageController@contact_us');
Route::get('faq', 'HomepageController@faq');
Route::get('privacy_policy', 'HomepageController@privacy_policy');
Route::get('terms_condition', 'HomepageController@terms_condition');

Auth::routes();

// Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('redirect/{driver}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
Route::get('{driver}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.callback');

//Routing for User
Route::get('/user/profile', 'ProfileController@index')->name('profile');
Route::post('/user/profile/update', 'ProfileController@updateProfile')->name('profile.update');
Route::get('user/about', 'HomeController@about')->name('about.user');
Route::get('user/account_detail', 'HomeController@account_detail')->name('account_detail.user');
Route::get('user/account_edit', 'HomeController@account_edit')->name('account_edit.user');
Route::get('user/account', 'HomeController@account')->name('account.user');
Route::get('user/cart_view', 'HomeController@cart_view')->name('cart_view.user');
Route::get('user/cart', 'HomeController@cart')->name('cart.user');
Route::get('user/contact_us', 'HomeController@contact_us')->name('contact_us.user');
Route::get('user/discussion', 'HomeController@discussion')->name('discussion.user');
Route::get('user/faq', 'HomeController@faq')->name('faq.user');
Route::get('user/notification_detail', 'HomeController@notification_detail')->name('notification_detail.user');
Route::get('user/notification', 'HomeController@notification')->name('notification.user');
Route::get('user/order', 'HomeController@order')->name('order.user');
Route::get('user/privacy_policy', 'HomeController@privacy_policy')->name('privacy_policy.user');
Route::get('user/reset_password', 'HomeController@reset_password')->name('reset_password.user');
Route::get('user/terms_condition', 'HomeController@terms_condition')->name('terms_condition.user');
Route::match(['get', 'post'], 'user/vendors', 'HomeController@vendors')->name('vendors.user');
Route::get('user/venues', 'HomeController@venues')->name('venues.user');
Route::get('user/wishlist/{id}/store', 'WishlistController@store')->name('wishlist.store');
Route::get('user/wishlist', 'WishlistController@index')->name('wishlist.user');
Route::get('user/wishlist/{id}/destroy', 'WishlistController@destroy')->name('wishlist.destroy');

//Routing for Owner
Route::prefix('owner')->group(function () {
    Route::namespace('AuthOwner')->group(function () {
    // Controllers Within The "App\Http\Controllers\AuthOwner" Namespace
        Route::get('register', 'RegisterController@show')->name('registerPage.owner');
        Route::post('register', 'RegisterController@register')->name('register.owner');
        Route::get('login', 'LoginController@show')->name('loginPage.owner');
        Route::post('login', 'LoginController@login')->name('login.owner');
        Route::get('logout', 'LoginController@logout')->name('logout.owner');
    });
    Route::namespace('Owner')->group(function () {
        // Controllers Within The "App\Http\Controllers\Owner" Namespace
        Route::get('/', 'OwnerController@index')->name('index.owner');
        Route::get('profile', 'OwnerController@profile')->name('profile.owner');
        Route::post('profile', 'OwnerController@updateProfile')->name('editProfile.owner');
        Route::get('detail', 'OwnerController@detail')->name('owner.owner');
        Route::get('vendor', 'VendorController@index')->name('vendor.owner');
        Route::post('vendor', 'VendorController@update')->name('editVendor.owner');
        Route::get('payment', 'PaymentController@index')->name('pembayaran.owner');
        Route::post('payment', 'PaymentController@update')->name('editPayment.owner');
        // Controllers Within The "App\Http\Controllers\Owner\PackageController" Namespace
        Route::get('daftarharga', 'PackageController@index')->name('daftarharga.owner');
        Route::post('daftarharga', 'PackageController@store')->name('daftarharga.owner.store');
        Route::get('daftarharga/{id}/show', 'PackageController@show')->name('daftarharga.owner.show');
        Route::get('daftarharga/{id}/destroy', 'PackageController@destroy')->name('daftarharga.owner.destroy');
        Route::match(['get', 'post'], 'daftarharga/{id}/update', 'PackageController@update')->name('daftarharga.owner.update');
        Route::get('statistics', 'OwnerVendorController@statistics')->name('statistics.owner');
        // Controllers Within The "App\Http\Controllers\Owner\GalleryController" Namespace
        Route::get('gallery', 'GalleryController@index')->name('gallery.owner');
        Route::post('gallery', 'GalleryController@store')->name('gallery.owner.store');
        Route::get('gallery/{id}/detail', 'GalleryController@detail')->name('gallery.owner.detail');
        Route::post('gallery/show', 'GalleryController@show')->name('gallery.owner.show');
        Route::post('gallery/update', 'GalleryController@update')->name('gallery.owner.update');
        Route::post('gallery/delete', 'GalleryController@delete')->name('gallery.owner.delete');
        Route::get('view_gallery', 'OwnerVendorController@view_gallery')->name('view_gallery.owner');
        Route::get('discussion', 'OwnerVendorController@discussion')->name('discussion.owner');
        Route::get('rating', 'OwnerVendorController@rating')->name('rating.owner');
        Route::get('review', 'OwnerVendorController@review')->name('review.owner');
        Route::get('order', 'OwnerVendorController@order')->name('order.owner');
        Route::get('view_order', 'OwnerVendorController@view_order')->name('view_order.owner');
        Route::get('view_order_all', 'OwnerVendorController@view_order_all')->name('view_order_all.owner');
        Route::get('notification', 'OwnerVendorController@notification')->name('notification.owner');
        Route::get('notification_detail', 'OwnerVendorController@notification_detail')->name('notification_detail.owner');
        Route::get('about', 'OwnerVendorController@about')->name('about.owner');
        Route::get('terms_condition', 'OwnerVendorController@terms_condition')->name('terms_condition.owner');
        Route::get('privacy_policy', 'OwnerVendorController@privacy_policy')->name('privacy_policy.owner');
        Route::get('contact_us', 'OwnerVendorController@contact_us')->name('contact_us.owner');
        Route::get('faq', 'OwnerVendorController@faq')->name('faq.owner');

    });
});

//Routing for Admin
Route::prefix('admin')->group(function () {
    Route::namespace('AuthAdmin')->group(function () {
    // Controllers Within The "App\Http\Controllers\AuthAdmin" Namespace
        Route::get('register', 'RegisterController@show')->name('registerPage.admin');
        Route::post('register', 'RegisterController@register')->name('register.admin');;
        Route::get('login', 'LoginController@show')->name('loginPage.admin');
        Route::post('login', 'LoginController@login')->name('login.admin');
        Route::post('logout', 'LoginController@logout')->name('logout.admin');
    });
    Route::namespace('Admin')->group(function () {
        // Controllers Within The "App\Http\Controllers\Admin" Namespace
        Route::get('dashboard', 'AdminController@index')->name('dashboard.admin');
        Route::get('masterdata/admin', 'AdminController@data_admin')->name('data_admin.admin');
        Route::post('masterdata/admin/store', 'AdminController@data_admin_store')->name('data_admin_store.admin');
        Route::get('masterdata/admin/edit/{id}', 'AdminController@data_admin_edit')->name('data_admin_edit.admin');
        Route::post('masterdata/admin/update', 'AdminController@data_admin_update')->name('data_admin_update.admin');
        Route::get('masterdata/admin/destroy/{id}', 'AdminController@data_admin_destroy')->name('data_admin_destroy.admin');
        Route::get('masterdata/user', 'AdminController@data_user')->name('data_user.admin');
        Route::post('masterdata/user/store', 'AdminController@data_user_store')->name('data_user_store.admin');
        Route::get('masterdata/user/edit/{id}', 'AdminController@data_user_edit')->name('data_user_edit.admin');
        Route::post('masterdata/user/update', 'AdminController@data_user_update')->name('data_user_update.admin');
        Route::get('masterdata/user/destroy/{id}', 'AdminController@data_user_destroy')->name('data_user_destroy.admin');
        Route::get('masterdata/vendor', 'AdminController@data_vendor')->name('data_vendor.admin');
        Route::post('masterdata/vendor/store', 'AdminController@data_vendor_store')->name('data_vendor_store.admin');
        Route::get('masterdata/vendor/edit/{id}', 'AdminController@data_vendor_edit')->name('data_vendor_edit.admin');
        Route::post('masterdata/vendor/update', 'AdminController@data_vendor_update')->name('data_vendor_update.admin');
        Route::get('masterdata/vendor/destroy/{id}', 'AdminController@data_vendor_destroy')->name('data_vendor_destroy.admin');
        Route::get('document/user/about', 'AdminController@about_user')->name('about_user.admin');
        Route::post('document/user/about/update', 'AdminController@about_user_update')->name('about_user_update.admin');
        Route::get('document/vendor/about', 'AdminController@about_vendor')->name('about_vendor.admin');
        Route::post('document/vendor/about/update', 'AdminController@about_vendor_update')->name('about_vendor_update.admin');
        Route::get('document/user/faq', 'AdminController@faq_user')->name('faq_user.admin');
        Route::post('document/user/faq/category/store', 'AdminController@faq_category_user_store')->name('faq_category_user_store.admin');
        Route::get('document/user/faq/category/edit/{id}', 'AdminController@faq_category_user_edit')->name('faq_category_user_edit.admin');
        Route::post('document/user/faq/category/update', 'AdminController@faq_category_user_update')->name('faq_category_user_update.admin');
        Route::get('document/user/faq/category/destroy/{id}', 'AdminController@faq_category_user_destroy')->name('faq_category_user_destroy.admin');
        Route::post('document/user/faq/store', 'AdminController@faq_user_store')->name('faq_user_store.admin');
        Route::get('document/user/faq/edit/{id}', 'AdminController@faq_user_edit')->name('faq_user_edit.admin');
        Route::post('document/user/faq/update', 'AdminController@faq_user_update')->name('faq_user_update.admin');
        Route::get('document/user/faq/destroy/{id}', 'AdminController@faq_user_destroy')->name('faq_user_destroy.admin');
        Route::get('document/vendor/faq', 'AdminController@faq_vendor')->name('faq_vendor.admin');
        Route::post('document/vendor/faq/category/store', 'AdminController@faq_category_vendor_store')->name('faq_category_vendor_store.admin');
        Route::get('document/vendor/faq/category/edit/{id}', 'AdminController@faq_category_vendor_edit')->name('faq_category_vendor_edit.admin');
        Route::post('document/vendor/faq/category/update', 'AdminController@faq_category_vendor_update')->name('faq_category_vendor_update.admin');
        Route::get('document/vendor/faq/category/destroy/{id}', 'AdminController@faq_category_vendor_destroy')->name('faq_category_vendor_destroy.admin');
        Route::post('document/vendor/faq/store', 'AdminController@faq_vendor_store')->name('faq_vendor_store.admin');
        Route::get('document/vendor/faq/edit/{id}', 'AdminController@faq_vendor_edit')->name('faq_vendor_edit.admin');
        Route::post('document/vendor/faq/update', 'AdminController@faq_vendor_update')->name('faq_vendor_update.admin');
        Route::get('document/vendor/faq/destroy/{id}', 'AdminController@faq_vendor_destroy')->name('faq_vendor_destroy.admin');
        Route::get('document/user/privacypolicy', 'AdminController@privacypolicy_user')->name('privacypolicy_user.admin');
        Route::post('document/user/privacypolicy/update', 'AdminController@privacypolicy_user_update')->name('privacypolicy_user_update.admin');
        Route::get('document/vendor/privacypolicy', 'AdminController@privacypolicy_vendor')->name('privacypolicy_vendor.admin');
        Route::post('document/vendor/privacypolicy/update', 'AdminController@privacypolicy_vendor_update')->name('privacypolicy_vendor_update.admin');
        Route::get('document/user/termscondition', 'AdminController@termscondition_user')->name('termscondition_user.admin');
        Route::post('document/user/termscondition/update', 'AdminController@termscondition_user_update')->name('termscondition_user_update.admin');
        Route::get('document/vendor/termscondition', 'AdminController@termscondition_vendor')->name('termscondition_vendor.admin');
        Route::post('document/vendor/termscondition/update', 'AdminController@termscondition_vendor_update')->name('termscondition_vendor_update.admin');
    });
});
