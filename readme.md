## About Weddinc

Weddinc adalah platform Teknologi Wedding Planner & Vendor Commerce yang bertujuan untuk mewujudkan pernikahan impian anda.

## Development Guide

Several quick start options are available:

- Clone the repo: `git clone https://gitlab.com/minsan.aulia/weddinc_beta`
- Install [composer](https://www.composer.org) first if you don't have it in your machine
- Run the `composer install` or `composer update` command to install vendor package !
- Copy `.env.example` and rename to `.env` to connet database Database
- Run the `php artisan migrate` command to install Database
- Run the `php artisan db:seed` command to seeder data
- Run the `php artisan serve` command to start the project!

## Roadmap

Database Relationship

<a href="https://www.weddinc.id">
  <img src="https://gitlab.com/minsan.aulia/weddinc_beta/raw/master/Diagram.png" alt="Database">
</a>
