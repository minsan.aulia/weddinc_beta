@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Dashboard</h1>
          </div>
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Total Admin</h4>
                  </div>
                  <div class="card-body">
                    @if (count($admin)==0)
                      0
                    @endif
                    @foreach ($admin as $admin)
                      @if ($loop->first)
                        {{ $loop->count }}
                      @endif
                    @endforeach
                  </div>
                </div>
                <p><a href="{{ route('data_admin.admin') }}">Detail</a></p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-dustypink">
                  <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Total Users</h4>
                  </div>
                  <div class="card-body">
                    @if (count($user)==0)
                      0
                    @endif
                    @foreach ($user as $user)
                      @if ($loop->first)
                        {{ $loop->count }}
                      @endif
                    @endforeach
                  </div>
                </div>
                <p><a href="{{ route('data_user.admin') }}">Detail</a></p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                  <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Total Owners</h4>
                  </div>
                  <div class="card-body">
                    @if (count($owner)==0)
                      0
                    @endif
                    @foreach ($owner as $owner)
                      @if ($loop->first)
                        {{ $loop->count }}
                      @endif
                    @endforeach
                  </div>
                </div>
                <p><a href="#">Detail</a></p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-tosca">
                  <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Total Vendors</h4>
                  </div>
                  <div class="card-body">
                    @if (count($vendor)==0)
                      0
                    @endif
                    @foreach ($vendor as $vendor)
                      @if ($loop->first)
                        {{ $loop->count }}
                      @endif
                    @endforeach
                  </div>
                </div>
                <p><a href="{{ route('data_vendor.admin') }}">Detail</a></p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Lorem ipsum</h4>
                  </div>
                  <div class="card-body pb-0">
                    Lorem ipsum
                  </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Lorem ipsum</h4>
                  </div>
                  <div class="card-body pb-0">
                    Lorem ipsum
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection