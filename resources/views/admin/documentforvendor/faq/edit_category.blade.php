@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      @foreach($faq_category_vendor as $faq_category_vendor)
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Document / For Vendor / FAQ Category / Edit / {{ $faq_category_vendor->name }}</h1>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Edit data FAQ category</h4>
                  </div>
                  <div class="card-body pb-0">
                  <form action="{{ route('faq_category_vendor_update.admin') }}" method="POST">
                  @csrf
                    <input type="hidden" id="id" name="id" value="{{ $faq_category_vendor->id }}"> <br/>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Name : </div>
                        <div class="col-md-6">
                            <input id="name" name="name" type="text" class="form-control" value="{{ $faq_category_vendor->name }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      @endforeach
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection