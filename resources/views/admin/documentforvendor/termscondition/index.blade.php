@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Document / For Vendor / Terms & Condition</h1>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <form action="{{ route('termscondition_vendor_update.admin') }}" method="POST">
                @csrf
                <div class="card">
                  <div class="card-header">
                    <h4>Terms Condition for Vendor</h4>
                  </div>
                  <div class="card-body pb-0">
                    <div class="col-lg-12 nopadding mb-4">
                      @foreach ($termscondition_vendor as $termscondition_vendor)
                        <textarea id="summernote" name="content">{{ $termscondition_vendor->content }}</textarea>
                      @endforeach
                    </div>
                    <div class="col-lg-12 col-md-12 col-12 col-sm-12 mb-4">
                        <button type="submit" class="btn btn-success btn-sm">Update</button>
                    <div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </div>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection