@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Document / Master Data / Admin</h1>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Data Admin</h4>
                  </div>
                  <div class="card-body pb-0">
                    <div class="col-lg-2">
                        <button type="button" class="btn btn-warning btn-sm mb-3 btn-block" data-toggle="modal" data-target="#add">Add</button>
                    </div>
                  <div class="table-responsive">
                  <table class="datatables table table-bordered table-md" cellspacing="0">
                    <thead class="bg-primary">
                      <tr>
                        <th class="text-center text-white">No</th>
                        <th class="text-center text-white">Photo</th>
                        <th class="text-center text-white">ID</th>
                        <th class="text-center text-white">Name</th>
                        <th class="text-center text-white">Position</th>
                        <th class="text-center text-white">Detail</th>
                        <th class="text-center text-white">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($admin as $index => $admin)
                      <tr>
                        <td class="text-center">{{ $index +1 }}</td>
                        <td class="text-center">
                          <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="width: 30px; height: auto;">
                        </td>
                        <td class="text-center">{{ $admin->user_admins_id }}</td>
                        <td class="text-center">{{ $admin->name }}</td>
                        <td class="text-center">{{ $admin->position }}</td>
                        <td class="text-left">
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#detail{{ $index }}" aria-expanded="false" aria-controls="detail{{ $index }}">Show</button>
                            <div class="collapse" id="detail{{ $index }}">
                                <table class="table mt-2">
                                  <thead>
                                    <tr>
                                      <td class="bg-primary text-white">Phone</td>
                                      <td>{{ $admin->no_telp }}</td>
                                    </tr>
                                    <tr>
                                      <td class="bg-primary text-white">Address</td>
                                      <td>{{ $admin->alamat }}</td>
                                    </tr>
                                    <tr>
                                      <td class="bg-primary text-white">Email</td>
                                      <td>{{ $admin->email }}</td>
                                    </tr>
                                  </thead>
                                </table>
                            </div>
                        </td>
                        <td class="text-center">
                          <a href="{{ route('data_admin_edit.admin', $admin->id) }}"><button type="button" class="btn btn-success btn-sm">Edit</button></a>
                          <a href="{{ route('data_admin_destroy.admin', $admin->id) }}"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="add">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form action="{{ route('data_admin_store.admin') }}" method="POST">
          @csrf
          <div class="modal-header">
            <h5 class="modal-title">Add Admin</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input id="user_admins_id" name="user_admins_id" type="text" class="form-control mb-2" placeholder="id">
            <input id="name" name="name" type="text" class="form-control mb-2" placeholder="name">
            <input id="no_telp" name="no_telp" type="text" class="form-control mb-2" placeholder="phone">
            <input id="alamat" name="alamat" type="text" class="form-control mb-2" placeholder="address">
            <input id="position" name="position" type="text" class="form-control mb-2" placeholder="position">
            <input id="email" name="email" type="email" class="form-control mb-2" placeholder="email">
            <input id="password" name="password" type="password" class="form-control mb-2" placeholder="password">
          </div>
          <div class="modal-footer bg-whitesmoke br">
            <button type="submit" class="btn btn-warning">Save</button>
          </div>
          </form>
        </div>
      </div>
    </div>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection