@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      @foreach($admin as $admin)
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Document / Master Data / Admin / Edit / {{ $admin->user_admins_id }}</h1>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Edit data admin</h4>
                  </div>
                  <div class="card-body pb-0">
                  <form action="{{ route('data_admin_update.admin') }}" method="POST">
                  @csrf
                    <input type="hidden" id="id" name="id" value="{{ $admin->id }}"> <br/>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">ID : </div>
                        <div class="col-md-6">
                            <input id="user_admins_id" name="user_admins_id" type="text" class="form-control" value="{{ $admin->user_admins_id }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Name : </div>
                        <div class="col-md-6">
                            <input id="name" name="name" type="text" class="form-control" value="{{ $admin->name }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Phone : </div>
                        <div class="col-md-6">
                            <input id="no_telp" name="no_telp" type="text" class="form-control" value="{{ $admin->no_telp }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Position : </div>
                        <div class="col-md-6">
                            <input id="position" name="position" type="text" class="form-control" value="{{ $admin->position }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Address : </div>
                        <div class="col-md-6">
                            <textarea id="alamat" name="alamat" type="text" class="form-control">{{ $admin->alamat }}</textarea>
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Email : </div>
                        <div class="col-md-6">
                            <input id="email" name="email" type="text" class="form-control" value="{{ $admin->email }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      @endforeach
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection