@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      @foreach($vendor as $vendor)
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Document / Master Data / Vendor / Edit / {{ $vendor->name }}</h1>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Edit data vendor</h4>
                  </div>
                  <div class="card-body pb-0">
                  <form action="{{ route('data_vendor_update.admin') }}" method="POST">
                  @csrf
                    <input type="hidden" id="id" name="id" value="{{ $vendor->id }}"> <br/>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Name : </div>
                        <div class="col-md-6">
                            <input id="name" name="name" type="text" class="form-control" value="{{ $vendor->name }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Owners ID : </div>
                        <div class="col-md-6">
                            <input id="owners_id" name="owners_id" type="text" class="form-control" value="{{ $vendor->owners_id }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Category : </div>
                        <div class="col-md-6">
                            <select id="category" name="category" class="custom-select mb-2">
                                <option value="1">Documentation</option>
                                <option value="2">Wedding Organizer</option>
                                <option value="3">Make Up</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Type : </div>
                        <div class="col-md-6">
                            <select id="type" name="type" class="custom-select mb-2">
                                <option value="1">Free</option>
                                <option value="2">Gold</option>
                                <option value="3">Platinum</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Area : </div>
                        <div class="col-md-6 text-right font-weight-bold">
                            <select id="area" name="area" class="custom-select mb-2">
                                <option value="1">Jakarta Barat</option>
                                <option value="2">Jakarta Timur</option>
                                <option value="3">Jakarta Selatan</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      @endforeach
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection