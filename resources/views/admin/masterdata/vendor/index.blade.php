@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Document / Master Data / Vendor</h1>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Data Vendor (not fix yet, don't use at this time)</h4>
                  </div>
                  <div class="card-body pb-0">
                    <div class="col-lg-2">
                        <button type="button" class="btn btn-warning btn-sm mb-3 btn-block" data-toggle="modal" data-target="#add">Add</button>
                    </div>
                  <div class="table-responsive">
                  <table class="datatables table table-bordered table-md" cellspacing="0">
                    <thead class="bg-primary">
                      <tr>
                        <th class="text-center text-white">No</th>
                        <th class="text-center text-white">Photo</th>
                        <th class="text-center text-white">Name</th>
                        <th class="text-center text-white">Category</th>
                        <th class="text-center text-white">Detail</th>
                        <th class="text-center text-white">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($vendor as $index => $vendor)
                      <tr>
                        <td class="text-center">{{ $index +1 }}</td>
                        <td class="text-center">
                          <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="width: 30px; height: auto;">
                        </td>
                        <td class="text-center">{{ $vendor->name }}</td>
                        <td class="text-center">{{ $vendor->category }}</td>
                        <td class="text-left">
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#detail{{ $index }}" aria-expanded="false" aria-controls="detail{{ $index }}">Show</button>
                            <div class="collapse" id="detail{{ $index }}">
                                <table class="table mt-2">
                                  <thead>
                                    <tr>
                                      <td class="bg-primary text-white">Type</td>
                                      <td>{{ $vendor->type }}</td>
                                    </tr>
                                    <tr>
                                      <td class="bg-primary text-white">Area</td>
                                      <td>{{ $vendor->area }}</td>
                                    </tr>
                                    <tr>
                                      <td class="bg-primary text-white">Activated?</td>
                                      <td>{{ $vendor->activated }}</td>
                                    </tr>
                                  </thead>
                                </table>
                            </div>
                        </td>
                        <td class="text-center">
                          <a href="{{ route('data_vendor_edit.admin', $vendor->id) }}"><button type="button" class="btn btn-success btn-sm">Edit</button></a>
                          <a href="{{ route('data_vendor_destroy.admin', $vendor->id) }}"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="add">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form action="{{ route('data_vendor_store.admin') }}" method="POST">
          @csrf
          <div class="modal-header">
            <h5 class="modal-title">Add Vendor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <div class="tab-content py-3 px-3 px-sm-0 bg-light" id="nav-tabContent">
            <div class="tab-pane fade show active bg-light" id="nav-registration" role="tabpanel" aria-labelledby="nav-registration-tab">
              <div class="accordion" id="myAccordion">
                <div class="card">
                  <a data-toggle="collapse" data-target="#collapseOne" style="cursor: pointer;">
                    <div class="card-header bg-tosca p-4" id="headingOne">
                      <h6 class="mb-0 text-white">1. Profil Vendor</h6>
                    </div>
                  </a>
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#myAccordion">
                    <div class="card-body">
                      <table class="table table-striped">
                        <tbody>
                          <tr>
                            <td>Nama Vendor</td>
                            <td class="input-group">
                              <input type="text" class="form-control" name="name_vendor" placeholder="nama vendor" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nama vendor</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Kategori Vendor</td>
                            <td class="input-group">
                              <select class="selectpicker show-tick form-control" name="category" data-width="100%" data-container="body" data-live-search="true" title="Pilih kategori" data-hide-disabled="true" required>
                                <option value="1">Wedding Planer & Organizer</option>
                                <option value="2">Dokumentasi</option>
                                <option value="3">Rias dan Tata Rambut</option>
                                <option value="4">Dokumentasi</option>
                                <option value="5">Detail & Aksesoris Pernikahan (souvenir, kartu undangan, cincin, dll.)</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih kategori vendor</small></i></div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a data-toggle="collapse" data-target="#collapseTwo" style="cursor: pointer;">
                    <div class="card-header bg-tosca p-4" id="headingTwo">
                      <h6 class="mb-0 text-white">2. Data Personal</h6>
                    </div>
                  </a>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#myAccordion">
                    <div class="card-body">
                      <table class="table table-striped">
                        <tbody>
                          <tr>
                            <td>Nama Pemilik</td>
                            <td class="input-group">
                              <input type="text" class="form-control" name="name" placeholder="nama" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nama pemilik</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Nomor Telepon</td>
                            <td class="input-group">
                              <input type="text" class="form-control" name="no_telp" placeholder="telepon" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nomor telepon</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Email</td>
                            <td class="input-group">
                              <input type="email" class="form-control" name="email" placeholder="email" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan email</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Password</td>
                            <td class="input-group">
                              <input type="password" class="form-control" name="password" placeholder="password" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan password</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Jenis Identitas</td>
                            <td class="input-group">
                              <select class="selectpicker show-tick form-control" name="type_identity" data-width="100%" data-container="body" data-live-search="true" title="Pilih jenis" data-hide-disabled="true" required>
                                <option value="KTP">KTP</option>
                                <option value="SIM">SIM</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih jenis identitas</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Nomor Identitas</td>
                            <td class="input-group">
                              <input type="text" class="form-control" name="no_identity" placeholder="nomor identitas" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nomor identitas</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Upload Scan Identitas</td>
                          </tr>
                          <tr>
                            <td><img src="{{ asset('assets/img/icon/card/card_id.png') }}" class="img-fluid"></td>
                            <td>Pilih file scan/foto kartu identitas Anda. KTP/Foto Identitas yang dilampirkan masih berlaku saat ini, tidak boleh terpotong di sisi manapun, harus terbaca dan tidak blur. Maksimal 3MB per file.</td>
                          </tr>
                          <tr>
                            <td colspan="2" class="text-center">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" name="photo_identity" id="customFile" required>
                                <label class="custom-file-label" for="customFile">Pilih file</label>
                              </div>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>upload foto identitas</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">Upload foto wajah dengan kartu identitas</td>
                          </tr>
                          <tr>
                            <td colspan="2" class="text-center">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" name="photo_withIdentity" id="customFile" required>
                                <label class="custom-file-label" for="customFile">Pilih file</label>
                              </div>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih foto</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Kota</td>
                            <td class="input-group">
                              <select class="selectpicker show-tick form-control" name="kota" data-width="100%" data-container="body" data-live-search="true" title="Pilih kota (Khusus Jabodetabek)" data-hide-disabled="true" required>
                                <option value="1">Jakarta Selatan</option>
                                <option value="2">Jakarta Barat</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih kota</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Alamat</td>
                            <td class="input-group">
                              <textarea type="text" class="form-control" name="alamat" placeholder="alamat" required></textarea>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan alamat</small></i></div>
                              </td>
                            </tr><tr>
                            <td>Nomor NPWP Pemilik</td>
                            <td class="input-group">
                              <input type="text" class="form-control" name="no_npwp" placeholder="nomor npwp" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nomor npwp</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Upload Scan NPWP</td>
                          </tr>
                          <tr>
                            <td><img src="{{ asset('assets/img/icon/card/card_npwp.png') }}" class="img-fluid"></td>
                            <td>Pilih file scan/foto kartu NPWP Anda. Foto NPWP yang dilampirkan masih berlaku saat ini, tidak boleh terpotong di sisi manapun, harus terbaca dan tidak blur. Maksimal 3MB per file.</td>
                          </tr>
                          <tr>
                            <td colspan="2" class="text-center">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" name="photo_npwp" id="customFile" required>
                                <label class="custom-file-label" for="customFile">Pilih file</label>
                              </div>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih foto</small></i></div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a data-toggle="collapse" data-target="#collapseThree" style="cursor: pointer;">
                    <div class="card-header bg-tosca p-4" id="headingThree">
                      <h6 class="mb-0 text-white">3. Data Pembayaran</h6>
                    </div>
                  </a>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#myAccordion">
                    <div class="card-body">
                      <table class="table table-striped">
                        <tbody>
                          <tr>
                            <td>DP awal yang diperlukan</td>
                            <td class="input-group">
                              <select class="selectpicker show-tick form-control" name="dp" data-width="100%" data-container="body" data-live-search="true" title="Pilih DP" data-hide-disabled="true" required>
                                <option value="0">0%</option>
                                <option value="5">5%</option>
                                <option value="10">10&</option>
                                <option value="15">15%</option>
                                <option value="20">20%</option>
                                <option value="25">25&</option>
                                <option value="30">30%</option>
                                <option value="35">35%</option>
                                <option value="40">40%</option>
                                <option value="45">45%</option>
                                <option value="50">50%</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih dp awal</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Nama Bank</td>
                            <td class="input-group">
                              <select class="selectpicker show-tick form-control" name="name_bank" data-width="100%" data-container="body" data-live-search="true" title="Pilih bank" data-hide-disabled="true" required>
                                <option value="1">BCA</option>
                                <option value="2">BNI</option>
                                <option value="3">BRI</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih nama bank</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Nomor Rekening</td>
                              <td class="input-group">
                              <input type="text" class="form-control" name="no_rek" placeholder="nomor rekening" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nomor rekening</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Panggilan</td>
                            <td class="input-group">
                              <select class="selectpicker show-tick form-control" name="call_name" data-width="100%" data-container="body" data-live-search="true" title="Pilih panggilan" data-hide-disabled="true" required>
                                <option value="Tn.">Tn.</option>
                                <option value="Ny.">Ny.</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih nama panggilan</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Nama Pemilik Rekening</td>
                            <td class="input-group">
                              <input type="text" class="form-control" name="name_owner_bank" placeholder="nama" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nama pemilik rekening</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">Upload Scan Buku Tabungan</td>
                          </tr>
                          <tr>
                            <td><img src="{{ asset('assets/img/icon/card/card_savings.png') }}" class="img-fluid"></td>
                            <td>Informasi rekening dapat diambil dari Foto halaman pertama dan kedua buku tabungan atau eBanking yang mencakup informasi Nomor Rekening. Nama Pemilik rekening (harus sama dengan nama pemilik store), dan Informasi bank (nama bank & cabang). Foto tidak blur. Maksimal 3 MB per file.</td>
                          </tr>
                          <tr>
                            <td colspan="2" class="text-center">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" name="photo_rek" id="customFile" required>
                                <label class="custom-file-label" for="customFile">Pilih file</label>
                              </div>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih foto</small></i></div>
                            </td>
                          </tr>
                          <tr>
                            <td>Status Rekening</td>
                            <td>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="chkPribadi" name="status_rek" value="0" checked>
                                <label class="custom-control-label" for="chkPribadi">Pribadi</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="chkLain" name="status_rek" value="1" onclick="ShowHideDiv()">
                                <label class="custom-control-label" for="chkLain">Orang lain</label>
                              </div>
                            </td>
                          </tr>
                          <tr id="statusRek3" style="display: none;">
                            <td colspan="2">Upload Scan Surat Kuasa</td>
                          </tr>
                          <tr id="statusRek1" style="display: none;">
                            <td><img src="{{ asset('assets/img/icon/card/card_power.png') }}" class="img-fluid"></td>
                            <td>Informasi surat kuasa diperlukan jika nama pemegang rekening dan yang tercantum dalam buku tabungan berbeda. Pastikan nama vendor terdaftar sebagai pihak pertama, nama orang yang berwenang sebagai yang kedua, nomor rekening, dan nama bank. Foto tidak buram. Maksimal 3 MB per file.</td>
                          </tr>
                          <tr>
                            <td colspan="2" class="text-center">
                              <div id="statusRek2" class="custom-file" style="display: none;">
                                <input type="file" name="photo_suratKuasa" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Pilih file</label>
                              </div>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih foto</small></i></div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a data-toggle="collapse" data-target="#collapseFour" style="cursor: pointer;">
                    <div class="card-header bg-tosca p-4" id="headingFour">
                      <h6 class="mb-0 text-white">4. Jangkauan Layanan</h6>
                    </div>
                  </a>
                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#myAccordion">
                    <div class="card-body">
                      <table class="table table-striped">
                        <tbody>
                          <tr>
                            <td colspan="2">Pilih venue yang sudah rekanan sama kamu ya.</td>
                          </tr>
                            <td>
                            <td class="input-group">
                              <select class="selectpicker show-tick form-control" name="area" data-width="100%" data-container="body" data-live-search="true" title="Pilih venue" data-hide-disabled="true" required>
                                <option value="1">Jakarta Pusat</option>
                                <option value="2">Jakarta Barat</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>pilih venue</small></i></div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <div class="modal-footer bg-whitesmoke br">
            <button type="submit" class="btn btn-warning">Save</button>
          </div>
          </form>
        </div>
      </div>
    </div>
<script>
function ShowHideDiv() {
  var chkLain = document.getElementById("chkLain");
  var statusRek3 = document.getElementById("statusRek3");
  var statusRek1 = document.getElementById("statusRek1");
  var statusRek2 = document.getElementById("statusRek2");

  if(chkLain.checled){
    console.log('yo');
  }

  statusRek3.style.display = chkLain.checked ? "" : "none";
  statusRek1.style.display = chkLain.checked ? "" : "none";
  statusRek2.style.display = chkLain.checked ? "" : "none";
}

var checker = document.getElementById('customCheck2');
var sendbtn = document.getElementById('btnRegis');

checker.onchange = function(){
  if(this.checked){
      sendbtn.disabled = false;
  } else {
      sendbtn.disabled = true;
  }
}
</script>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection