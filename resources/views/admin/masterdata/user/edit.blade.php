@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      @foreach($user as $user)
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Document / Master Data / User / Edit / {{ $user->name }}</h1>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Edit data user</h4>
                  </div>
                  <div class="card-body pb-0">
                  <form action="{{ route('data_user_update.admin') }}" method="POST">
                  @csrf
                    <input type="hidden" id="id" name="id" value="{{ $user->id }}"> <br/>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Name : </div>
                        <div class="col-md-6">
                            <input id="name" name="name" type="text" class="form-control" value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Email : </div>
                        <div class="col-md-6">
                            <input id="email" name="email" type="text" class="form-control" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Jenis Kelamin : </div>
                        <div class="col-md-6">
                            <select id="jenis_kelamin" name="jenis_kelamin" class="custom-select col-md-12">
                                <option value="1">Laki-laki</option>
                                <option value="2">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Phone : </div>
                        <div class="col-md-2 text-right font-weight-bold">
                            <select class="custom-select" disabled>
                                <option value="+62">+62</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                        <input id="telepon" name="telepon" type="text" class="form-control" value="{{ $user->telepon }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      @endforeach
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection