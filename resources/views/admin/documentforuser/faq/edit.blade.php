@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      @foreach($faq_user as $faq_user)
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Document / For User / FAQ / Edit / {{ $faq_user->ask }}</h1>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Edit data FAQ category</h4>
                  </div>
                  <div class="card-body pb-0">
                  <form action="{{ route('faq_user_update.admin') }}" method="POST">
                  @csrf
                    <input type="hidden" id="id" name="id" value="{{ $faq_user->id }}"> <br/>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Category: </div>
                        <div class="col-md-6">
                          <select class="selectpicker show-tick form-control" value="{{ $faq_user->faq_category_id }}" name="faq_category_id" data-width="100%" data-container="body" data-live-search="true" title="Pilih kategori FAQ" data-hide-disabled="true" required="" tabindex="-98">
                            @foreach($list_category_user as $value)
                              @if($faq_user->faq_category_id==$value->id)
                                <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                              @else
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Ask : </div>
                        <div class="col-md-6">
                            <input id="ask" name="ask" type="text" class="form-control" value="{{ $faq_user->ask }}">
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <div class="col-md-2 text-right font-weight-bold">Question : </div>
                        <div class="col-md-6">
                            <textarea id="summernote" name="question">{{ $faq_user->question }}</textarea>
                        </div>
                    </div>
                    <div class="row col-md-12 mb-2">
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      @endforeach
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection