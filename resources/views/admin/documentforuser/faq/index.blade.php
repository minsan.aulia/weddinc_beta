@extends('layouts.admin_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
          <h1>Document / For User / FAQ</h1>
          </div><div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header bg-dustypink">
                    <h4 class="text-white">List of Category FAQ for user</h4>
                  </div>
                  <div class="card-body pb-0">
                    <div class="col-lg-2">
                        <button type="button" class="btn btn-warning btn-sm mb-3 btn-block" data-toggle="modal" data-target="#addcategory">Add</button>
                    </div>
                  <div class="table-responsive">
                  <table class="datatables table table-bordered table-md" cellspacing="0">
                    <thead class="bg-dustypink">
                      <tr>
                        <th class="text-center text-white">No</th>
                        <th class="text-center text-white">Category</th>
                        <th class="text-center text-white">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($faq_category_user as $index => $faq_category_user)
                      <tr>
                        <td class="text-center">{{ $index +1 }}</td>
                        <td class="text-center">{{ $faq_category_user->name }}</td>
                        <td class="text-center">
                          <a href="{{ route('faq_category_user_edit.admin', $faq_category_user->id) }}"><button type="button" class="btn btn-success btn-sm">Edit</button></a>
                          <a href="{{ route('faq_category_user_destroy.admin', $faq_category_user->id) }}"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                  </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header bg-info">
                    <h4 class="text-white">List of FAQ for user</h4>
                  </div>
                  <div class="card-body pb-0">
                    <div class="col-lg-2">
                        <button type="button" class="btn btn-warning btn-sm mb-3 btn-block" data-toggle="modal" data-target="#addfaq">Add</button>
                    </div>
                  <div class="table-responsive">
                  <table class="datatables table table-bordered table-md" cellspacing="0">
                    <thead class="bg-info">
                      <tr>
                        <th class="text-center text-white" width="5%">No</th>
                        <th class="text-center text-white" width="10%">Category</th>
                        <th class="text-center text-white" width="25%">Ask</th>
                        <th class="text-center text-white" width="50%">Question</th>
                        <th class="text-center text-white" width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($faq_user as $index => $faq_user)
                      <tr>
                        <td class="text-center">{{ $index +1 }}</td>
                        <td class="text-center">{{ $faq_user->name }}</td>
                        <td class="text-left">{{ $faq_user->ask }}</td>
                        <td class="text-left">
                            <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#question{{ $index }}" aria-expanded="false" aria-controls="question{{ $index }}">Show</button>
                            <div class="collapse" id="question{{ $index }}">
                              {!! $faq_user->question !!}
                            </div>
                        </td>
                        <td class="text-center">
                          <a href="{{ route('faq_user_edit.admin', $faq_user->id_faqs) }}"><button type="button" class="btn btn-success btn-sm">Edit</button></a>
                          <a href="{{ route('faq_user_destroy.admin', $faq_user->id_faqs) }}"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="addcategory">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form action="{{ route('faq_category_user_store.admin') }}" method="POST">
            @csrf
            <div class="modal-header">
              <h5 class="modal-title">Add FAQ Category</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input id="categories_for" name="categories_for" type="hidden" class="form-control mb-2" value="0">
              <input id="name" name="name" type="text" class="form-control mb-2" placeholder="name">
            </div>
            <div class="modal-footer bg-whitesmoke br">
              <button type="submit" class="btn btn-warning">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="addfaq">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form action="{{ route('faq_user_store.admin') }}" method="POST">
            @csrf
            <div class="modal-header">
              <h5 class="modal-title">Add FAQ User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input id="faq_for" name="faq_for" type="hidden" class="form-control mb-2" value="0">
              <select id="faq_category_id" name="faq_category_id" class="custom-select mb-2">
                @foreach($list_category_user as $index => $list_category_user)
                <option value="{{ $list_category_user->id }}">{{ $list_category_user->name }}</option>
                @endforeach
              </select>
              <input id="ask" name="ask" type="text" class="form-control mb-2" placeholder="ask">
              <textarea id="summernote" name="question" placeholder="question"></textarea>
            </div>
            <div class="modal-footer bg-whitesmoke br">
              <button type="submit" class="btn btn-warning">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection