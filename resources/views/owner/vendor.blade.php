@extends('layouts.owner_layout')

@section('title', 'Vendor Settings')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="row col-md-12 justify-content-center mb-4">
                    <div class="col-md-3">
                        <div class="border">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><a href="{{ route('vendor.owner') }}" class="text-darktosca"><i class="fa fa-home"></i> Profil Vendor</a></li>
                                <li class="list-group-item"><a href="{{ route('profile.owner') }}" class="text-darktosca"><i class="fa fa-user"></i> Profil Pemilik</a></li>
                                <li class="list-group-item"><a href="{{ route('pembayaran.owner') }}" class="text-darktosca"><i class="fa fa-dollar-sign"></i> Pembayaran</a></li>
                                <li class="list-group-item">Profile Strength</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <form action="{{ route('editVendor.owner') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-12 mb-4">
                                    <div class="card text-center">
                                        <div class="card-header bg-tosca text-white">
                                            <div class="row">
                                                <div class="col-6 text-left font-weight-bold">FOTO PROFIL & COVER</div>
                                                <div class="col-6 text-right"><i class="fa fa-user-circle"></i></div>
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <img id="previewprofile" src="{!! ($photo = $data->photo) === NULL ? asset('assets/img/icon/dummy_user.png') : asset('uploads/photoProfile/'.$photo) !!}" width="180px" height="180px"/><br/>
                                                    <input type="file" name="photo" id="imageprofile" style="display: none;"/>
                                                    <input type="hidden" style="display: none" name="remove_photo" id="remove_photo">
                                                    <a class="btn btn-success btn-sm mt-2" href="javascript:changeProfile()">Ganti</a>
                                                    <a class="btn btn-danger btn-sm mt-2" href="javascript:removeProfile()">Hapus</a>
                                                </div>
                                                <div class="col-md-8">
                                                    <img id="previewcover" src="{!! ($cover = $data->cover) === NULL ? asset('assets/img/icon/dummy_cover.png') : asset('uploads/photoCover/'.$cover) !!}" width="100%" height="180px"/><br/>
                                                    <input type="file" name="cover" id="imagecover" style="display: none;"/>
                                                    <input type="hidden" style="display: none" name="remove_cover" id="remove_cover">
                                                    @if($data->type == "FREE")
                                                        <p>Fitur ini tidak tersedia untuk FREE account, <a href="">Upgrade?</a></p>
                                                    @else
                                                        <a class="btn btn-success btn-sm mt-2" href="javascript:changeCover()">Ganti</a>
                                                        <a class="btn btn-danger btn-sm mt-2" href="javascript:removeCover()">Hapus</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="card text-center">
                                        <div class="card-header bg-tosca text-white">
                                            <div class="row">
                                                <div class="col-6 text-left font-weight-bold">DETAIL VENDOR</div>
                                                <div class="col-6 text-right"><i class="fa fa-wallet"></i></div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div
                                                    class="col-md-12 text-left font-weight-bold border border-top-0 border-left-0 border-right-0 mb-3">
                                                    Status vendor
                                                </div>
                                                <div class="col-md-12 text-left mb-3 input-group">
                                                    <div class="custom-control custom-switch">
                                                        <input type="checkbox" name="activated"
                                                               class="custom-control-input"
                                                               id="customSwitches" {{ $data->activated == 0?: "checked"}}>
                                                        <label class="custom-control-label" for="customSwitches">Aktif/Nonaktif</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 text-left font-weight-bold border border-top-0 border-left-0 border-right-0 mb-3">Nama Vendor
                                                </div>
                                                <div class="col-md-12 text-left mb-3"><input type="text" name="name" value="{!! $data->name; !!}" disabled="" class="form-control"></div>
                                                <div class="col-md-12 text-left font-weight-bold border border-top-0 border-left-0 border-right-0 mb-3">Tentang Vendor
                                                </div>
                                                <div class="col-md-12 text-left mb-3"><textarea id="summernote" name="tentang" class="form-control" style="height: 200px;">{{ $data->tentang }}</textarea>
                                                </div>
                                                <div class="col-md-12 text-left font-weight-bold border border-top-0 border-left-0 border-right-0 mb-3">Alamat Vendor
                                                </div>
                                                <div class="col-md-12 text-left mb-3"><input type="text" name="alamat" value="{!! $data->alamat; !!}" class="form-control"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="card text-center">
                                        <div class="card-header bg-tosca text-white">
                                            <div class="row">
                                                <div class="col-6 text-left font-weight-bold">KONTAK & SOSIAL MEDIA
                                                </div>
                                                <div class="col-6 text-right"><i class="fa fa-address-book"></i></div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <img src="{{ asset('assets/img/icon/social/whatsapp.png') }}" style="width: 25px; height: 25px;">
                                                    </td>
                                                    <td>WhatsApp</td>
                                                    <td>
                                                        <input class="form-control" type="text" name="wa" value="{!! $data->wa; !!}">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="{{ asset('assets/img/icon/social/phone.png') }}" style="width: 25px; height: 25px;">
                                                    </td>
                                                    <td>Telepon</td>
                                                    <td>
                                                        <input class="form-control" type="text" name="telp" value="{!! $data->telp; !!}">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="{{ asset('assets/img/icon/social/facebook.png') }}" style="width: 25px; height: 25px;">
                                                    </td>
                                                    <td>URL Facebook</td>
                                                    <td>
                                                        <input class="form-control" type="text" name="fb" value="{!! $data->fb; !!}">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="{{ asset('assets/img/icon/social/twitter.png') }}" style="width: 25px; height: 25px;">
                                                    </td>
                                                    <td>URL Twitter</td>
                                                    <td>
                                                        <input class="form-control" type="text" name="twitter" value="{!! $data->twitter; !!}">
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="{{ asset('assets/img/icon/social/instagram.png') }}" style="width: 25px; height: 25px;">
                                                    </td>
                                                    <td>URL Instagram</td>
                                                    <td>
                                                        <input class="form-control" type="text" name="ig" value="{!! $data->ig; !!}">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="card text-center">
                                        <div class="card-header bg-tosca text-white">
                                            <div class="row">
                                                <div class="col-6 text-left font-weight-bold">KATEGORI VENDOR & LOKASI
                                                </div>
                                                <div class="col-6 text-right"><i class="fa fa-map-marker-alt"></i></div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="row col-6">
                                                    <div class="col-md-12 font-weight-bold">Kategori Vendor</div>
                                                    <div class="col-md-12">
                                                        <select class="selectpicker show-tick form-control" value="{!! $data->category; !!}" name="category" data-width="100%" data-container="body" data-live-search="true" title="Pilih area layanan" data-hide-disabled="true" required="" tabindex="-98">
                                                            @foreach($category as $value)
                                                                @if($data->category==$value->id)
                                                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                                                @else
                                                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="col-md-12 font-weight-bold">Lokasi</div>
                                                    <div class="col-md-12">
                                                        <select class="selectpicker show-tick form-control" value="{!! $data->area; !!}" name="area" data-width="100%" data-container="body" data-live-search="true" title="Pilih area layanan" data-hide-disabled="true" required="" tabindex="-98">
                                                            @foreach($city as $value)
                                                                @if($data->area==$value->id)
                                                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                                                @else
                                                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <div class="card text-center">
                                        <input type="submit" class="btn btn-tosca" value="Simpan">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
    </section>
@endsection

@section('footer')
    @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->
<script>
    function changeProfile() {
        $('#imageprofile').click();
    }
    $('#imageprofile').change(function () {
        var imgPath = this.value;
        var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
            readURLProfile(this);
        else
            alert("Please select image file (jpg, jpeg, png).")
    });
    function readURLProfile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                $('#previewprofile').attr('src', e.target.result);
                //              $("#remove").val(0);
            };
        }
    }
    function removeProfile() {
        $('#previewprofile').attr('src', '{{ asset('assets/img/icon/dummy_user.png') }}');
        $("#remove_photo").val(1);
    }

    function changeCover() {
        $('#imagecover').click();
    }
    $('#imagecover').change(function () {
        var imgPath = this.value;
        var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
            readURLCover(this);
        else
            alert("Please select image file (jpg, jpeg, png).")
    });
    function readURLCover(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                $('#previewcover').attr('src', e.target.result);
                //              $("#remove").val(0);
            };
        }
    }
    function removeCover() {
        $('#previewcover').attr('src', '{{ asset('assets/img/icon/dummy_cover.png') }}');
        $("#remove_cover").val(1);
    }
</script>
@endsection
