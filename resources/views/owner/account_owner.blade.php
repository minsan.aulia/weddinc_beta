@extends('layouts.owner_layout')

@section('title', 'Dashboard Owner')

@section('navigation')
    @parent
@endsection

@section('content')
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="row col-md-12 justify-content-center mb-4">
          <div class="col-md-3">
            <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('vendor.owner') }}" class="text-darktosca"><i class="fa fa-home"></i> Profil Vendor</a></li>
                <li class="list-group-item"><a href="{{ route('profile.owner') }}" class="text-darktosca"><i class="fa fa-user"></i> Profil Pemilik</a></li>
                <li class="list-group-item"><a href="{{ route('pembayaran.owner') }}" class="text-darktosca"><i class="fa fa-dollar-sign"></i> Pembayaran</a></li>
                <li class="list-group-item">Profile Strength</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-7">
            <div class="row">
              <div class="col-md-12 mb-4">
                <div class="card text-center">
                <form action="{{ route('editProfile.owner') }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="card-header bg-tosca text-white">
                    <div class="row">
                      <div class="col-6 text-left font-weight-bold">DETAIL PEMILIK</div>
                      <div class="col-6 text-right"><i class="fa fa-wallet"></i></div>
                    </div>
                  </div>

                    <div class="card-body">
                      <div class="row">
                        <table class="table table-bordered">
                            <tbody>
                              <tr>
                                <td class="font-weight-bold">Nama</td>
                                <td><input type="text" name="name" value="{!! $data->name; !!}" class="form-control"></td>
                              </tr>
                              <tr>
                                <td class="font-weight-bold">Telepon</td>
                                <td><input type="text" name="phone" value="{!! $data->no_telp; !!}" class="form-control"></td>
                              </tr>
                              <tr>
                                <td class="font-weight-bold">Email</td>
                                <td><input type="text" name="email" value="{!! $data->email; !!}" class="form-control" disabled></td>
                              </tr>
                              <tr>
                                <td class="font-weight-bold">Jenis identitas</td>
                                <td>
                                  <select class="selectpicker show-tick form-control" name="type_id" data-width="100%" data-container="body" data-live-search="true" title="Pilih jenis" data-hide-disabled="true">
                                    <option value="1" {{ $data->type_identity == '1' ? 'selected="selected"' : ''}}>KTP</option>
                                    <option value="2" {{ $data->type_identity == '2' ? 'selected="selected"' : ''}}>SIM</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td class="font-weight-bold">Scan identitas</td>
                                <td>
                                  <img id="previewid" src="{!! ($photo = $data->photo_identity) === NULL ? asset('assets/img/icon/card/card_id.png') : asset('uploads/photoIdentity/'.$photo) !!}" width="100%" height="200px"/><br/>
                                  <input type="file" name="photo_id" id="imageid" style="display: none;"/>
                                  <input type="hidden" style="display: none" name="remove_id" id="remove_id">
                                  <a class="btn btn-success btn-sm mt-2" href="javascript:changeId()">Ganti</a>
                                  <a class="btn btn-danger btn-sm mt-2" href="javascript:removeId()">Hapus</a>
                                </td>
                              </tr>
                              <tr>
                                <td class="font-weight-bold">Kota</td>
                                <td>
                                  <select class="selectpicker show-tick form-control" name="city" data-width="100%" data-container="body" data-live-search="true" title="Pilih kota" data-hide-disabled="true" required="" tabindex="-98">
                                  @foreach($city as $value)
                                    @if($data->city==$value->id)
                                      {<option value="{{ $value->id }}" selected>{{ $value->name }}</option>}
                                    @else
                                      {<option value="{{ $value->id }}">{{ $value->name }}</option>}
                                    @endif
                                  @endforeach
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td class="font-weight-bold">Alamat</td>
                                <td><input type="text" name="address" value="{!! $data->address; !!}" class="form-control"></td>
                              </tr>
                              <tr>
                                <td class="font-weight-bold">Nomor NPWP</td>
                                <td><input type="text" name="no_npwp" value="{!! $data->no_npwp; !!}" class="form-control"></td>
                              </tr>
                              <tr>
                                <td class="font-weight-bold">Scan NPWP</td>
                                <td>
                                  <img id="previewnpwp" src="{!! ($photo = $data->photo_npwp) === NULL ? asset('assets/img/icon/card/card_npwp.png') : asset('uploads/photoNpwp/'.$photo) !!}" width="100%" height="200px"/><br/>
                                  <input type="file" name="photoNpwp" id="imagenpwp" style="display: none;"/>
                                  <input type="hidden" style="display: none" value="0" name="remove_npwp" id="removeNpwp">
                                  <a class="btn btn-success btn-sm mt-2" href="javascript:changeNpwp()">Ganti</a>
                                  <a class="btn btn-danger btn-sm mt-2" href="javascript:removeNpwp()">Hapus</a>
                                </td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 mb-4">
                <div class="card text-center">
                  <input type="submit" class="btn btn-tosca" value="Simpan">
                </div>
              </div>
            </form>
            </div>
          </div>
      </div>
  </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->
<script>
    function changeId() {
        $('#imageid').click();
    }
    $('#imageid').change(function () {
        var imgPath = this.value;
        var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
            readURLId(this);
        else
            alert("Please select image file (jpg, jpeg, png).")
    });
    function readURLId(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                $('#previewid').attr('src', e.target.result);
                //              $("#remove").val(0);
            };
        }
    }
    function removeId() {
        $('#previewid').attr('src', '{{ asset('assets/img/icon/card/card_id.png') }}');
        $("#remove_id").val(1);
    }

    function changeNpwp() {
        $('#imagenpwp').click();
    }
    $('#imagenpwp').change(function () {
        var imgPath = this.value;
        var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
            readURLNpwp(this);
        else
            alert("Please select image file (jpg, jpeg, png).")
    });
    function readURLNpwp(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                $('#previewnpwp').attr('src', e.target.result);
                //              $("#remove").val(0);
            };
        }
    }
    function removeNpwp() {
        $('#previewnpwp').attr('src', '{{ asset('assets/img/icon/card/card_npwp.png') }}');
        $("#removeNpwp").val(1);
    }
</script>
@endsection
