@extends('layouts.owner_layout')

@section('title', 'Dashboard Pricelist')

@section('navigation')
    @parent
@endsection

@section('content')


  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="row col-md-12 justify-content-center mb-4">
          <div class="col-md-3">
          <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-7">
            <div class="row">
              <div class="col-md-12 mb-4">
                <div class="card text-center">
                  <div class="card-header bg-tosca text-white">
                    <div class="row">
                      <div class="col-6 text-left font-weight-bold">DAFTAR PAKET & HARGA</div>
                      <div class="col-6 text-right"><i class="fa fa-plus text-white" data-toggle="modal" data-target="#add_pricelist" style="cursor: pointer;"></i></div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="card-group">

                    @if(count($packages)==0)
                      <div class="col-12 mt-4 text-center">
                        <h4 class="text-secondary">No Data Found</h4>
                      </div>
                    @endif
                    
                    @foreach($packages as $package)
                      <div class="col-sm-6 col-xl-4 mb-3">
                      <div class="card h-100">
                        <div class="card-body">
                          <h5 class="card-title">{{ $package->name }}</h5>
                          <img src="{{ ($package->photo)? asset('uploads/packages/'.$package->photo) : asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                          <p class="card-text"><small class="text-muted">{{ str_limit($package->description, 50, ' . . .') }}</small></p>
                        </div>
                        <div class="card-footer bg-tosca text-white text-muted">
                          <a class="text-white" onclick="modalEdit({{ $package->id }})" style="cursor: pointer;">Edit</a>
                        </div>
                        <div class="card-footer bg-tosca text-white text-muted">
                          <a class="text-white" onclick="modalDelete({{ $package->id }})" style="cursor: pointer;">Delete</a>
                        </div>
                      </div>
                      </div>
                    @endforeach
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
  </section>

    <div class="container">
      <div class="modal fade" id="add_pricelist">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <form action="{{route('daftarharga.owner.store')}}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="modal-header text-center display-6">
                Tambah Paket
              </div>
              <div class="modal-body">
                <div class="row">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>Foto paket</td>
                        <td>
                          <div class="custom-file">
                            <input type="file" name="add_photo" class="custom-file-input {{ $errors->has('add_photo') ? 'is-invalid' : '' }}" id="customFileLangHTML" value="{{ old('add_photo') }}">
                            <label class="custom-file-label" for="customFileLangHTML" data-browse="pilih">Pilih foto</label>
                            @if($errors->has('add_photo'))
                            <div class="invalid-feedback">{{ $errors->first('add_photo') }}</div>
                            @endif
                          </div>
                          
                        </td>
                      </tr>
                      <tr>
                        <td>Nama paket</td>
                        <td>
                          <input class="form-control {{ $errors->has('add_name') ? 'is-invalid' : '' }}" type="text" name="add_name" id="add_name" value="{{ old('add_name') }}">
                          @if($errors->has('add_name'))
                          <div class="invalid-feedback">{{ $errors->first('add_name') }}</div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Kategori</td>
                        <td>
                          <select name="add_category" class="form-control {{ $errors->has('add_category') ? 'is-invalid' : '' }}" id="">
                            <option value="" selected disabled>Pilih Kategori</option>
                            @foreach($categories as $category)
                              <option value="{{ $category->id }}" {{ $category->id==old('add_category') ? 'selected' : '' }}>{{ $category-> name}}</option>
                            @endforeach
                          </select>
                          @if($errors->has('add_category'))
                          <div class="invalid-feedback">{{ $errors->first('add_category') }}</div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Kota</td>
                        <td>
                          <select name="add_city" class="form-control {{ $errors->has('add_city') ? 'is-invalid' : '' }}" id="">
                            <option value="" selected disabled>Pilih Kota</option>
                            @foreach($cities as $city)
                              <option value="{{ $city->id }}" {{ $city->id==old('add_city') ? 'selected' : '' }}>{{ $city->name}}</option>
                            @endforeach
                          </select>
                          @if($errors->has('add_city'))
                          <div class="invalid-feedback">{{ $errors->first('add_city') }}</div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Keterangan</td>
                        <td>
                          <input class="form-control {{ $errors->has('add_description') ? 'is-invalid' : '' }}" type="text" name="add_description" id="add_description" value="{{ old('add_description') }}">
                          @if($errors->has('add_description'))
                          <div class="invalid-feedback">{{ $errors->first('add_description') }}</div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Harga</td>
                        <td>
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Rp</div>
                            </div>
                            <input class="form-control money {{ $errors->has('add_price') ? 'is-invalid' : '' }}" type="text" name="add_price" id="add_price" value="{{ old('add_price') }}">
                            @if($errors->has('add_price'))
                            <div class="invalid-feedback">{{ $errors->first('add_price') }}</div>
                            @endif
                          </div>
                        </td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                <input type="submit" class="btn btn-tosca btn-sm" value="Simpan">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      
      <div class="modal fade" id="edit_pricelist">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <form id="form_edit" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="modal-header text-center display-4">
                Edit Paket
              </div>
              <div class="modal-body">
                <div class="row">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>Foto paket</td>
                        <td>
                          <img id="previewpricelist" height="180px"/><br/>
                          <input type="file" id="imagepricelist" style="display: none;" name="edt_photo"/>
                          <input type="hidden" name="edt_remove_photo" id="removeimagepricelist">
                          <input type="hidden" name="edt_id">
                          <a class="btn btn-success btn-sm mt-2" href="javascript:changePricelist()">Ganti</a> 
                          <a class="btn btn-danger btn-sm mt-2" href="javascript:removePricelist()">Hapus</a>
                        </td>
                      </tr>
                      <tr>
                        <td>Nama paket</td>
                        <td>
                          <input class="form-control {{ $errors->has('edt_name') ? 'is-invalid' : '' }}" type="text" name="edt_name" id="edt_name">
                          @if($errors->has('edt_name'))
                          <div class="invalid-feedback">{{ $errors->first('edt_name') }}</div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Kategori</td>
                        <td>
                          <select name="edt_category" class="form-control {{ $errors->has('edt_category') ? 'is-invalid' : '' }}" id="">
                            <option value="" disabled>Pilih Kategori</option>
                            @foreach($categories as $category)
                              <option value="{{ $category->id }}" id="opt_edt_category_{{ $category->id }}">{{ $category-> name}}</option>
                            @endforeach
                          </select>
                          @if($errors->has('edt_category'))
                          <div class="invalid-feedback">{{ $errors->first('add_category') }}</div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Kota</td>
                        <td>
                          <select name="edt_city" class="form-control {{ $errors->has('edt_city') ? 'is-invalid' : '' }}" id="">
                            <option value="" disabled>Pilih Kota</option>
                            @foreach($cities as $city)
                              <option value="{{ $city->id }}" id="opt_edt_city_{{ $city->id }}">{{ $city->name}}</option>
                            @endforeach
                          </select>
                          @if($errors->has('edt_city'))
                          <div class="invalid-feedback">{{ $errors->first('edt_city') }}</div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Keterangan</td>
                        <td>
                          <input class="form-control {{ $errors->has('edt_description') ? 'is-invalid' : '' }}" type="text" name="edt_description" id="edt_description">
                          @if($errors->has('edt_description'))
                          <div class="invalid-feedback">{{ $errors->first('edt_description') }}</div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Harga</td>
                        <td>
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Rp</div>
                            </div>
                            <input class="form-control money {{ $errors->has('edt_price') ? 'is-invalid' : '' }}" type="text" name="edt_price" id="edt_price">
                            @if($errors->has('edt_price'))
                            <div class="invalid-feedback">{{ $errors->first('edt_price') }}</div>
                            @endif
                          </div>
                        </td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="delete_pricelist">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header text-center display-4">
              Hapus paket
              <h4 class="modal-title" id="modalMdTitle"></h4>
            </div>
            <div class="modal-body">
              <div class="row">
                Yakin menghapus paket ini?
                <table class="table table-bordered table-hover mt-3">
                  <thead>
                    <tr>
                      <td>Nama paket</td>
                      <td id="name"></td>
                    </tr>
                    <tr>
                      <td>Keterangan</td>
                      <td id="description"></td>
                    </tr>
                    <tr>
                      <td>Harga</td>
                      <td id="price" class="currency-id"></td>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <a href="" id="deletePackage" class="btn btn-danger btn-sm">Hapus</a>
              <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            </div>
          </div>
        </div>
      </div>

    </div>
    


@endsection


@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->
<script>
  @if ($errors->has('edt*'))
    modalEdit({{old('edt_id')}});
  @endif
  
  @if ($errors->has('add*'))
    $('#add_pricelist').modal('show');
  @endif

  function currencyFormat() {
    $('.currency-id').each(function() { 
      var monetary_value = $(this).text(); 
      var i = new Intl.NumberFormat('id-ID', { 
          style: 'currency', 
          currency: 'IDR',
          minimumFractionDigits: 0 
      }).format(monetary_value); 
      $(this).text(i); 
    }); 
  }

  function modalDelete(id) {
      var url = "{{ route('daftarharga.owner') }}";
      $.get(url + '/' + id + '/show', function (data) {
          $('#name').html(data.name);
          $('#description').html(data.description);
          $('#price').html(data.price);
          $('#deletePackage').attr('href', url+'/'+id+'/destroy');
          currencyFormat();
          $('#delete_pricelist').modal('show');
      });
  }
  
  function modalEdit(id) {
      var url = "{{ route('daftarharga.owner') }}";
      $.get(url + '/' + id + '/show', function (data) {
          $("input[name='edt_id']").val(id);
          $("input[name='edt_name']").val(data.name);
          $("input[name='edt_description']").val(data.description);
          $("input[name='edt_price']").val(data.price);
          $("#opt_edt_category_"+data.category_id).attr("selected","selected");
          $("#opt_edt_city_"+data.city_id).attr("selected","selected");
          $("input[name='edt_remove_photo']").val(0);
          if(data.photo) {
            var imgPath = "{{ asset('uploads/packages/') }}/" + data.photo;
          } 
          else {
            var imgPath = "{{ asset('assets/img/icon/dummy.png') }}";
          }
          $("img#previewpricelist").attr('src', imgPath);
          $("input[name='edt_photo']").attr('value', data.photo);
          $("form#form_edit").attr('action', url+'/'+id+'/update')
          $('#edit_pricelist').modal('show');
      });
  }
</script>
@endsection