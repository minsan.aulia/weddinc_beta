@extends('layouts.owner_layout')

@section('title', 'Dashboard Owner')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">STATUS RATING</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_discussion.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body text-center">
                      <p>Rating Total Anda</p>
                      <span class="fa fa-star text-warning"></span>
                      <span class="fa fa-star text-warning"></span>
                      <span class="fa fa-star text-warning"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      (3.0/5 dari 140.131 ulasan)
                      <p><a href="{{ route('review.owner') }}">Lihat semua ulasan</a></p>
                      <p class="mt-4">Top 10 Kompetitor Anda</p>
                      <table class="table">
                        <thead>
                          <tr>
                            <td>No</td>
                            <td>Nama Vendor</td>
                            <td>Rating</td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>Butioko</td>
                            <td>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              (5.0/5 dari 1.435.232 ulasan)
                            </td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>Satioko</td>
                            <td>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star"></span>
                              (4.7/5 dari 1.340.451 ulasan)
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection