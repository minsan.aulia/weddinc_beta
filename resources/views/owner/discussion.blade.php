@extends('layouts.owner_layout')

@section('title', 'Dashboard Owner')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">SEMUA DISKUSI</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_discussion.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                <div class="container">
                  <div class="messaging">
                        <div class="inbox_msg">
                          <div class="inbox_people">
                            <div class="headind_srch">
                              <div class="recent_heading">
                                <h4>Diskusi</h4>
                              </div>
                              <div class="srch_bar">
                                <div class="stylish-input-group">
                                  <input type="text" class="search-bar"  placeholder="Cari" >
                                  <span class="input-group-addon">
                                  <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                  </span> </div>
                              </div>
                            </div>
                            <div class="inbox_chat">
                              <div class="chat_list active_chat">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>Nama User <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>Nama User <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>Nama User <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>Nama User <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>Nama User <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>Nama User <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>Nama User <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="mesgs">
                            <div class="msg_history">
                              <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg">
                                    <p>Test which is a new approach to have all
                                      solutions</p>
                                    <span class="time_date"> 11:01 AM    |    June 9</span></div>
                                </div>
                              </div>
                              <div class="outgoing_msg">
                                <div class="sent_msg">
                                  <p>Test which is a new approach to have all
                                    solutions</p>
                                  <span class="time_date"> 11:01 AM    |    June 9</span> </div>
                              </div>
                              <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg">
                                    <p>Test, which is a new approach to have</p>
                                    <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
                                </div>
                              </div>
                              <div class="outgoing_msg">
                                <div class="sent_msg">
                                  <p>Apollo University, Delhi, India Test</p>
                                  <span class="time_date"> 11:01 AM    |    Today</span> </div>
                              </div>
                              <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg">
                                    <p>We work directly with our designers and suppliers,
                                      and sell direct to you, which means quality, exclusive
                                      products, at a price anyone can afford.</p>
                                    <span class="time_date"> 11:01 AM    |    Today</span></div>
                                </div>
                              </div>
                            </div>
                            <div class="type_msg">
                              <div class="input_msg_write row">
                                <div class="col-2">
                                  <div class="dropup">
                                    <button class="msg_send_btn" type="button" data-toggle="modal" data-target="#add_order"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                  </div>
                                </div>
                                <div class="col-8">
                                  <input type="text" class="write_msg" placeholder="Tulis pesan" />
                                </div>
                                <div class="col-1">
                                  <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
    <div class="container">
      <form action="#" method="POST" class="needs-validation" novalidate>
        <div class="modal fade" id="add_order">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header text-center display-4">
                Buat Order
              </div>
              <div class="modal-body">
                <div class="col-md-12">
                  <select class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih paket" data-hide-disabled="true" required>
                    <option value="1">Paket pertama - Rp8.000.000</option>
                    <option value="2">Paket kedua - Rp13.000.000</option>
                    <option value="1">Paket ketiga - Rp15.000.000</option>
                  </select>
                  <div class="valid-feedback"><i><small>valid</small></i></div>
                  <div class="invalid-feedback"><i><small>masukkan nama paket</small></i></div>
                </div>
                <div class="row">
                  <div class="col-md-5">
                    <div class="row">
                      <div class="col-6 text-right p-2">
                        Termin Pembayaran : 
                      </div>
                      <div class="col-6 p-2">
                        <select class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih termin" data-hide-disabled="true" required>
                          <option value="1">1 kali bayar</option>
                          <option value="2">2 kali bayar</option>
                        </select>
                      </div>
                      <div class="col-6 text-right p-2">
                        Harga paket : 
                      </div>
                      <div class="col-6 p-2">
                        <p class="font-weight-bold">Rp23.000.000</p>
                      </div>
                      <div class="col-6 text-right p-2">
                        Biaya tambahan : 
                      </div>
                      <div class="col-6 p-2">
                        <input type="text" class="money form-control" name="">
                      </div>
                      <div class="col-6 text-right p-2">
                        Keterangan : 
                      </div>
                      <div class="col-6 p-2">
                        <textarea class="form-control" name=""></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="row">
                      <div class="col-6 text-right p-2">
                        Nama klien yang memesan : 
                      </div>
                      <div class="col-6 p-2">
                        <img src="https://www.jamf.com/jamf-nation/img/default-avatars/generic-user-purple.png" style="width: 30px; height: auto;"> Anastasia Herawati
                      </div>
                      <div class="col-6 text-right p-2">
                        Tanggal pernikahan : 
                      </div>
                      <div class="col-6 p-2">
                        <input type="date" class="form-control" name="" required>
                      </div>
                      <div class="col-6 text-right p-2">
                        Tanggal pengantaran : 
                      </div>
                      <div class="col-6 p-2">
                        <input type="date" class="form-control" name="">
                        <div class="custom-control custom-checkbox mb-3 mt-1">
                          <input type="checkbox" class="custom-control-input" id="customCheck1">
                          <label class="custom-control-label" for="customCheck1"><small>Sama dengan tanggal pernikahan</small></label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-sm">Buat order</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection