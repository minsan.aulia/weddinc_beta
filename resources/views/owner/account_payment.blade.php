@extends('layouts.owner_layout')

@section('title', 'Dashboard Payment')

@section('navigation')
    @parent
@endsection

@section('content')
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="row col-md-12 justify-content-center mb-4">
          <div class="col-md-3">
          <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('vendor.owner') }}" class="text-darktosca"><i class="fa fa-home"></i> Profil Vendor</a></li>
                <li class="list-group-item"><a href="{{ route('profile.owner') }}" class="text-darktosca"><i class="fa fa-user"></i> Profil Pemilik</a></li>
                <li class="list-group-item"><a href="{{ route('pembayaran.owner') }}" class="text-darktosca"><i class="fa fa-dollar-sign"></i> Pembayaran</a></li>
                <li class="list-group-item">Profile Strength</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-7">
            <div class="row">
              <div class="col-md-12 mb-4">
                <div class="card text-center">
                <form action="{{ route('editPayment.owner') }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="card-header bg-tosca text-white">
                    <div class="row">
                      <div class="col-6 text-left font-weight-bold">DETAIL PEMBAYARAN</div>
                      <div class="col-6 text-right"><i class="fa fa-dollar-sign"></i></div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <td class="font-weight-bold">Nama bank</td>
                            <td>
                              <select class="selectpicker show-tick form-control" name="name_bank" data-width="100%" data-container="body" data-live-search="true" title="Pilih bank" data-hide-disabled="true">
                                  @foreach($bank as $value)
                                      @if($data->name_bank == $value->id)
                                          <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                      @else
                                          <option value="{{ $value->id }}">{{ $value->name }}</option>
                                      @endif
                                  @endforeach
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-weight-bold">Nomor rekening</td>
                            <td><input type="text" name="no_rek" value="{{ $data->no_rek }}" class="form-control"></td>
                          </tr>
                          <tr>
                            <td class="font-weight-bold">Scan buku tabungan</td>
                            <td>
                              <img id="previewsavings" src="{!! ($photo = $data->photo_rek) === NULL ? asset('assets/img/icon/card/card_savings.png') : asset('uploads/photoRekening/'.$photo) !!}" width="100%" height="200px"/><br/>
                              <input type="file" name="photo_rek" id="imageRekening" style="display: none;"/>
                              <input type="hidden" style="display: none" name="remove_rek" id="remove_rek">
                              <a class="btn btn-success btn-sm mt-2" href="javascript:changeSavings()">Ganti</a>
                              <a class="btn btn-danger btn-sm mt-2" href="javascript:removeSavings()">Hapus</a>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-weight-bold">Scan surat kuasa</td>
                            <td>
                              <img id="previewpower" src="{!! ($photo = $data->photo_suratKuasa) === NULL ? asset('assets/img/icon/card/card_power.png') : asset('uploads/photoSuratKuasa/'.$photo) !!}" width="100%" height="200px"/><br/>
                              <input type="file" name="photo_suratKuasa" id="imagepower" style="display: none;"/>
                              <input type="hidden" style="display: none" name="remove_kuasa" id="remove_kuasa">
                              <a class="btn btn-success btn-sm mt-2" href="javascript:changePower()">Ganti</a>
                              <a class="btn btn-danger btn-sm mt-2" href="javascript:removePower()">Hapus</a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 mb-4">
                <div class="card text-center">
                  <input type="submit" class="btn btn-tosca" value="Simpan">
                </div>
              </div>
            </form>
            </div>
          </div>
      </div>
  </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->
<script>
    function changeSavings() {
        $('#imageRekening').click();
    }
    $('#imageRekening').change(function () {
        var imgPath = this.value;
        var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
            readURLRekening(this);
        else
            alert("Please select image file (jpg, jpeg, png).")
    });
    function readURLRekening(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                $('#previewsavings').attr('src', e.target.result);
//              $("#remove").val(0);
            };
        }
    }
    function removeSavings() {
        $('#previewsavings').attr('src', '../assets/img/icon/card/card_savings.png');
         $("#remove_rek").val(1);
    }

    function changePower() {
        $('#imagepower').click();
    }
    $('#imagepower').change(function () {
        var imgPath = this.value;
        var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
            readURLPower(this);
        else
            alert("Please select image file (jpg, jpeg, png).")
    });
    function readURLPower(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                $('#previewpower').attr('src', e.target.result);
//              $("#remove").val(0);
            };
        }
    }
    function removePower() {
        $('#previewpower').attr('src', '../assets/img/icon/card/card_power.png');
         $("#remove_kuasa").val(1);
    }
</script>
@endsection
