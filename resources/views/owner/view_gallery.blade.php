@extends('layouts.owner_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
<section>
      <div class="container container-fluid mb-4">
        <div class="row">
          <div class="col-md-12">
            <div class="image-section">
              <img src="{{ asset('assets/img/icon/dummy_cover.png') }}">
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-3">
                <div class="row">
                  <div class="col-md-12 ml-3">
                    <div class="row mb-3">
                      <div class="col-md-12 user-image text-center">
                        <img src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle">
                      </div>
                      <div class="col-md-12 text-center mt-3 p-3 font-weight-bold border border-bottom-0">
                        Nama Vendor
                      </div>
                      <div class="col-md-12 text-center p-3 border border-bottom-0 border-top-0">
                        Jl. Pemuda Pancasila, Jakarta Selatan, DKI Jakarta
                      </div>
                      <div class="col-md-12 text-center p-3 border border-bottom-0 border-top-0">
                        Premium Vendor
                      </div>
                      <div class="col-md-12 text-center p-3 border border-top-0">
                        <div class="row">
                          <div class="col-md-6">165K Views</div>
                          <div class="col-md-6">75 Love</div>
                        </div>
                      </div>
                    </div>
                    <div class="row mb-3">
                      <div class="col-md-12 text-center mt-3 p-3 font-weight-bold border border-bottom-0">
                        <a href="account_discussion.html"><button class="btn btn-tosca">Diskusi dengan kami</button></a>
                      </div>
                      <div class="col-md-12 text-center p-3 border border-top-0">
                        <div class="row">
                          <div class="col-md-12">Hubungi Kami di</div>
                          <div class="col-md-12 text-center mt-3">
                            <div class="row">
                              <div class="col-2">
                                <img src="{{ asset('assets/img/icon/social/facebook.png') }}" style="width: 25px; height: 25px;">
                              </div>
                              <div class="col-2">
                                <img src="{{ asset('assets/img/icon/social/twitter.png') }}" style="width: 25px; height: 25px;">
                              </div>
                              <div class="col-2">
                                <img src="{{ asset('assets/img/icon/social/instagram.png') }}" style="width: 25px; height: 25px;">
                              </div>
                              <div class="col-2">
                                <img src="{{ asset('assets/img/icon/social/whatsapp.png') }}" style="width: 25px; height: 25px;">
                              </div>
                              <div class="col-2">
                                <img src="{{ asset('assets/img/icon/social/phone.png') }}" style="width: 25px; height: 25px;">
                              </div>
                              <div class="col-2">
                                <img src="{{ asset('assets/img/icon/social/email.png') }}" style="width: 25px; height: 25px;">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-9">
                <div class="col-md-12 mr-3 mt-3">
                  <div class="col-md-12 profile-header">
                    <div class="row">
                      <div class="col-md-8 col-sm-6 col-xs-6 profile-header-section1 pull-left">
                        <h1 class="font-weight-bold">Nama Vendor</h1>
                        <h6 class="font-weight-bold">Kategori Vendor</h6>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-6 profile-header-section1 text-right pull-rigth">
                        Rating Vendor
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <section class="gallery-block grid-gallery">
                      <div class="container">
                          <div class="heading">
                              <h2>JIN & JUN WEDDING</h2>
                          </div>
                          <div class="row">
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                              <div class="col-md-6 col-lg-4 item">
                                  <a class="lightbox" href="{{ asset('assets/img/icon/dummy.png') }}">
                                      <img class="img-fluid image scale-on-hover" src="{{ asset('assets/img/icon/dummy.png') }}">
                                  </a>
                              </div>
                          </div>
                      </div>
                  </section>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection