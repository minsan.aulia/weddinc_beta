@extends('layouts.owner_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
<section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">NOTIFIKASI</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="card col-md-12 text-left p-2 mb-2 bg-tosca">
                          <div class="row">
                            <div class="col-md-12"><p><small><i class="fas fa-clock"></i> 13 Oktober 2019</small></p></div>
                            <div class="col-md-12 font-weight-bold">Hore! ini notifikasi pertama kamu!</div>
                            <div class="col-md-12">ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya..</div>
                            <div class="col-md-12 text-right font-italic"><a href="{{ route('notification_detail.owner') }}" class="text-white"><p><small>lihat selengkapnya..</small></p></a></div>
                          </div>
                        </div>
                        <div class="card col-md-12 text-left p-2 mb-2">
                          <div class="row">
                            <div class="col-md-12"><p><small><i class="fas fa-clock"></i> 13 Oktober 2019</small></p></div>
                            <div class="col-md-12 font-weight-bold">Hore! ini notifikasi pertama kamu!</div>
                            <div class="col-md-12">ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya..</div>
                            <div class="col-md-12 text-right font-italic"><a href="{{ route('notification_detail.owner') }}" class="text-darktosca"><p><small>lihat selengkapnya..</small></p></a></div>
                          </div>
                        </div>
                        <div class="card col-md-12 text-left p-2 mb-2">
                          <div class="row">
                            <div class="col-md-12"><p><small><i class="fas fa-clock"></i> 13 Oktober 2019</small></p></div>
                            <div class="col-md-12 font-weight-bold">Hore! ini notifikasi pertama kamu!</div>
                            <div class="col-md-12">ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya..</div>
                            <div class="col-md-12 text-right font-italic"><a href="{{ route('notification_detail.owner') }}" class="text-darktosca"><p><small>lihat selengkapnya..</small></p></a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection