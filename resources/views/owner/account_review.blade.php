@extends('layouts.owner_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
<section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">SEMUA ULASAN</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_discussion.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body text-center">
                      <table class="table" id="dataTables" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Dari</th>
                            <th>Rating</th>
                            <th>Ulasan</th>
                            <th>Respon</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-center">1</td>
                            <td class="text-center">
                              <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="width: 30px; height: auto;"> Anastasia Herawati
                            </td>
                            <td class="text-center">
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                            </td>
                            <td class="text-center">Pelayanan sangat baik, suka banget deh di sini..</td>
                            <td class="text-center">Hi.. terima kasih telah memeasn jasa kami, semoga Anda senang.</td>
                          </tr>
                          <tr>
                            <td class="text-center">2</td>
                            <td class="text-center">
                              <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="width: 30px; height: auto;"> Anastasia Herawati
                            </td>
                            <td class="text-center">
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star text-warning"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                            </td>
                            <td class="text-center">Pelayanan sangat baik, suka banget deh di sini..</td>
                            <td class="text-center"><button type="button" class="btn btn-sm btn-tosca"  data-toggle="modal" data-target="#add_respons">Balas</button></td>s
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
    <div class="container">
      <form action="#" method="POST" class="needs-validation" novalidate>
        <div class="modal fade" id="add_respons">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header text-center display-4">
                Balas review
              </div>
              <div class="modal-body">
                <div class="col-md-12 mb-4">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>Balasan</td>
                        <td class="input-group">
                          <textarea class="form-control" type="text" name="" required></textarea>
                          <div class="valid-feedback"><i><small>valid</small></i></div>
                          <div class="invalid-feedback"><i><small>masukkan balasan</small></i></div>
                        </td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-sm">Balas</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection