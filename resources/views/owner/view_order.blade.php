@extends('layouts.owner_layout')

@section('title', 'Dashboard Owner')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">Invoice : 149797979</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-4">
                          <p class="card-text">MAKE UP PLATINUM</p>
                          <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                          <p class="card-text mt-2">
                            <img src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle" style="width: 25px; height: 25px;">
                            <small class="text-muted">Widya Widyastuti</small>
                            <p class="card-text"><small class="text-muted">Tanggal : 9 Oktober 2019</small></p>
                            <p class="card-text"><small class="text-muted">Pukul : 11:15:17 PM</small></p>
                          </p>
                        </div>
                        <div class="col-md-8 text-right">
                          <p class="card-text"><small class="text-muted">Metode pembayaran :</small></p>
                          <p class="card-text">VIRTUAL ACCOUNT BCA</p>
                          <p class="card-text"><small class="text-muted">Kode promo :</small></p>
                          <p class="card-text">-</p>
                          <p class="card-text"><small class="text-muted">Biaya tambahan :</small></p>
                          <p class="card-text">Rp0,-</p>
                          <p class="card-text"><small class="text-muted">Status disetujui :</small></p>
                          <p class="card-text">
                            <button type="submit" class="btn btn-success btn-sm">Disetujui</button>
                            <button type="submit" class="btn btn-danger btn-sm">Belum disetujui</button>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="card-header bg-tosca text-white border text-left">HARGA PAKET</div>
                    <div class="card-body border boder-bottom-0 border-left-0 border-right-0">
                      <div class="row">
                        <div class="col-md-12">
                          <table class="table borderless">
                            <thead>
                              <tr>
                                <td class="text-right p-2">HARGA PAKET</td>
                                <td class="text-left p-2 font-weight-bold">Rp80.000.000</td>
                              </tr>
                              <tr>
                                <td class="text-right p-2">
                                  <p>BIAYA TAMBAHAN</p>
                                  <p><small>(test make up, ongkos kirim, dll.)</small></p>
                                </td>
                                <td class="text-left p-2 font-weight-bold">Rp850.000</td>
                              </tr>
                              <tr>
                                <td class="text-right p-2">KODE PROMO</td>
                                <td class="text-left p-2 font-weight-bold text-danger">(NIKAHYUK) -Rp45.000.000</td>
                              </tr>
                              <tr>
                                <td class="text-right p-2">TOTAL</td>
                                <td class="text-left p-2 font-weight-bold text-success">Rp50.000.000</td>
                              </tr>
                            </thead>
                          </table>
                        </div>
                      </div>
                      </div>
                    <div class="card-header bg-tosca text-white border text-left">TERMIN PEMBAYARAN</div>
                    <div class="card-body border boder-bottom-0 border-left-0 border-right-0">
                      <div class="row">
                        <div class="col-md-12">
                          <table class="table borderless">
                            <thead>
                              <tr>
                                <td class="text-left p-2">Termin</td>
                                <td class="text-left p-2">Batas Waktu</td>
                                <td class="text-left p-2">Jumlah</td>
                                <td class="text-left p-2">Metode</td>
                                <td class="text-left p-2">Status</td>
                              </tr>
                              <tr>
                                <td class="text-left p-2">Pembayaran ke-1</td>
                                <td class="text-left p-2">26 Oktober 2019</td>
                                <td class="text-left p-2">Rp25.000.000</td>
                                <td class="text-left p-2">Virtual Account BCA</td>
                                <td class="text-left p-2">Selesai</td>
                              </tr>
                              <tr>
                                <td class="text-left p-2">Pembayaran ke-2</td>
                                <td class="text-left p-2">15 Desember 2019</td>
                                <td class="text-left p-2">Rp25.000.000</td>
                                <td class="text-left p-2">Transfer Bank BNI</td>
                                <td class="text-left p-2">Pending</td>
                              </tr>
                            </thead>
                          </table>
                        </div>
                      </div>
                      </div>
                    <div class="card-header bg-tosca text-white border text-left">REVIEW</div>
                    <div class="card-body border boder-bottom-0 border-left-0 border-right-0">
                      <div class="row">
                            <div class="col-md-12 card m-2">
                              <div class="row m-1">
                                <div class="col-md-3">
                                  <div class="col-md-12 text-center">
                                    <img src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle" width="50" height="50">
                                  </div>
                                  <div class="col-md-12 text-center">
                                    <p><small>Evelyn Christiana</small></p>
                                  </div>
                                </div>
                                <div class="col-md-9 text-left">
                                  <div class="row">
                                    <div class="col-md-12">Rating</div>
                                    <div class="col-md-12">Lorem ipsum dolor sit amet.</div>
                                    <div class="col-md-12 display-10">Date</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 m-2">
                              <div class="col-md-9 card p-3 float-right">
                                <div class="row justify-content-end">
                                  <div class="col-md-12">
                                    <div class="row">
                                      <div class="col-md-12 text-left">
                                        <div class="row">
                                          <div class="col-md-4">
                                            <div class="col-md-12 text-center">
                                              <img src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle" width="50" height="50">
                                            </div>
                                            <div class="col-md-12 text-center">
                                              <p><small>Nama vendor</small></p>
                                            </div>
                                          </div>
                                          <div class="col-md-8">Hi.. terima kasih telah menggunakan layanan kami.</div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 m-2">
                              <div class="col-md-9 card p-3 float-right">
                                <form action="#" method="POST" class="needs-validation" novalidate>
                                  <div class="row text-left">
                                    <div class="col-md-12 p-2">Balas Ulasan</div>
                                    <div class="col-md-12 p-2">
                                      <textarea class="form-control" placeholder="balas ulasan"required></textarea>
                                      <div class="valid-feedback"><i><small>valid</small></i></div>
                                      <div class="invalid-feedback"><i><small>masukkan balasan</small></i></div>
                                    </div>
                                    <div class="col-md-12 p-2"><button type="submit" class="btn btn-tosca btn-sm">Balas</button></div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                      </div>
                    <div class="card-footer text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection