@extends('layouts.owner_layout')

@section('title', 'Gallery')

@section('navigation')
    @parent
@endsection

@section('content')
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="row col-md-12 justify-content-center mb-4">
          <div class="col-md-3">
            <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-7">
            <div class="row">
              <div class="col-md-12 mb-4">
                <div class="card text-center">
                  <div class="card-header bg-tosca text-white">
                    <div class="row">
                      <div class="col-6 text-left">DAFTAR GALERI</div>
                      <div class="col-6 text-right"><i class="fa fa-plus text-white" data-toggle="modal" data-target="#add_gallery" style="cursor: pointer;"></i></div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="card-group">
                      @if(count($galleries)==0)
                        <div class="col-12 mt-4 text-center">
                          <h4 class="text-secondary">No Data Found</h4>
                        </div>
                      @endif
                      @foreach($galleries as $gallery)
                      <div class="col-sm-6 col-xl-4 mb-3">
                        <div class="card h-100">
                          <div class="card-body">
                            <h5 class="card-title">{{ $gallery->name }}</h5>
                            <p class="card-text">
                              <img src="{{ ($gallery->profileVendor->photo)? asset('uploads/vendors/'.$gallery->profileVendor->photo) : asset('assets/img/icon/dummy.png') }}" class="rounded-circle" style="width: 25px; height: 25px;">
                              <small class="text-muted">{{ $gallery->vendor->name }}</small>
                            </p>
                            <img src="{{ ($gallery->photo_1)? asset('uploads/galleries/'.$gallery->photo_1) : asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                            <p class="card-text"><small class="text-muted">{{ str_limit($gallery->description, 50, ' . . .') }}</small></p>
                          </div>
                          <div class="card-footer bg-tosca text-white text-muted"><a class="text-white" href="{{ route('gallery.owner.detail', $gallery->id) }}">Lihat detail</a></div>
                        </div>
                      </div>
                      @endforeach

                    </div>
                  </div>
                  <div class="card-footer bg-tosca text-white text-muted"></div>
                </div>
              </div>
            </div>
          </div>
      </div>
  </section>

  <div class="container">
      <div class="modal fade" id="add_gallery">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <form action="{{ route('gallery.owner.store') }}" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="modal-header text-center display-4">
                Tambah galeri
              </div>
              <div class="modal-body">
                <div class="col-md-12 mb-4">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>Nama galeri</td>
                        <td>
                          <input class="form-control @error('gallery_name') is-invalid @enderror" type="text" name="gallery_name" value="{{ old('gallery_name') }}">
                          @error('gallery_name')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </td>
                      </tr>
                      <tr>
                        <td>Keterangan</td>
                        <td>
                          <textarea class="form-control @error('description') is-invalid @enderror" type="text" name="description">{{ old('description') }}</textarea>
                          @error('description')
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </td>
                      </tr>
                    </thead>
                  </table>
                </div>
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-body">
                      <div class="card-group">
                        <div class="card">
                          <div class="card-body">
                            <img id="previewgallery1" src="{{ asset('assets/img/icon/dummy.png') }}" width="100%" height="180px"/><br/>
                            <input type="file" class="form-control @error('photo_1') is-invalid @enderror" name="photo_1" id="imagegallery1" style="display: none;"/>
                            <input type="hidden" style="display: none" value="0" name="removephoto_1" id="removegallery1">
                            @error('photo_1')
                              <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <a class="btn btn-success btn-sm mt-2" href="javascript:changeGallery1()">Ganti</a> 
                            <a class="btn btn-danger btn-sm mt-2" href="javascript:removeGallery1()">Hapus</a>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-body">
                            <img id="previewgallery2" src="{{ asset('assets/img/icon/dummy.png') }}" width="100%" height="180px"/><br/>
                            <input type="file" class="form-control @error('photo_2') is-invalid @enderror" name="photo_2" id="imagegallery2" style="display: none;"/>
                            <input type="hidden" style="display: none" value="0" name="removephoto_2" id="removegallery2">
                            @error('photo_2')
                              <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <a class="btn btn-success btn-sm mt-2" href="javascript:changeGallery2()">Ganti</a> 
                            <a class="btn btn-danger btn-sm mt-2" href="javascript:removeGallery2()">Hapus</a>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-body">
                            <img id="previewgallery3" src="{{ asset('assets/img/icon/dummy.png') }}" width="100%" height="180px"/><br/>
                            <input type="file" class="form-control @error('photo_3') is-invalid @enderror" name="photo_3" id="imagegallery3" style="display: none;"/>
                            <input type="hidden" style="display: none" value="0" name="removephoto_3" id="removegallery3">
                            @error('photo_3')
                              <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <a class="btn btn-success btn-sm mt-2" href="javascript:changeGallery3()">Ganti</a> 
                            <a class="btn btn-danger btn-sm mt-2" href="javascript:removeGallery3()">Hapus</a>
                          </div>
                        </div>
                      </div>
                      <div class="card-group">
                        <div class="card">
                          <div class="card-body">
                            <img id="previewgallery4" src="{{ asset('assets/img/icon/dummy.png') }}" width="100%" height="180px"/><br/>
                            <input type="file" class="form-control @error('photo_4') is-invalid @enderror" name="photo_4" id="imagegallery4" style="display: none;"/>
                            <input type="hidden" style="display: none" value="0" name="removephoto_4" id="removegallery4">
                            @error('photo_4')
                              <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <a class="btn btn-success btn-sm mt-2" href="javascript:changeGallery4()">Ganti</a> 
                            <a class="btn btn-danger btn-sm mt-2" href="javascript:removeGallery4()">Hapus</a>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-body">
                            <img id="previewgallery5" src="{{ asset('assets/img/icon/dummy.png') }}" width="100%" height="180px"/><br/>
                            <input type="file" class="form-control @error('photo_5') is-invalid @enderror" name="photo_5" id="imagegallery5" style="display: none;"/>
                            <input type="hidden" style="display: none" value="0" name="removephoto_5" id="removegallery5">
                            @error('photo_5')
                              <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <a class="btn btn-success btn-sm mt-2" href="javascript:changeGallery5()">Ganti</a> 
                            <a class="btn btn-danger btn-sm mt-2" href="javascript:removeGallery5()">Hapus</a>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-body">
                            <img id="previewgallery6" src="{{ asset('assets/img/icon/dummy.png') }}" width="100%" height="180px"/><br/>
                            <input type="file" class="form-control @error('photo_6') is-invalid @enderror" name="photo_6" id="imagegallery6" style="display: none;"/>
                            <input type="hidden" style="display: none" value="0" name="removephoto_6" id="removegallery6">
                            @error('photo_6')
                              <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <a class="btn btn-success btn-sm mt-2" href="javascript:changeGallery6()">Ganti</a> 
                            <a class="btn btn-danger btn-sm mt-2" href="javascript:removeGallery6()">Hapus</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal fade" id="delete_gallery">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header text-center display-4">
              Hapus galeri
            </div>
            <div class="modal-body">
              <div class="row">
                Yakin menghapus paket ini?
                <table class="table table-bordered table-hover mt-3">
                  <thead>
                    <tr>
                      <td>Nama galeri</td>
                      <td>Galeri ini</td>
                    </tr>
                    <tr>
                      <td>Keterangan</td>
                      <td>keterangan dari galeri</td>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-sm">Hapus</button>
              <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->
  @if(session('addGalleryFailed'))
    <script>
      $('#add_gallery').modal('show');
    </script>
  @endif
@endsection