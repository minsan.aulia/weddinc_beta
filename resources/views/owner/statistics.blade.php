@extends('layouts.owner_layout')

@section('title', 'Dashboard Owner')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">STATISTIK PENGUNJUNG PROFIL</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <p class="display-5">154.956</p>
                      <p>pengunjung</p>
                      <div id="statvisitor" style="height: 500px; min-width: 100%"></div>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted text-left"><small class="font-italic text-white">menampilkan statistik jumlah pengunjung halaman profil vendor.</small></div>
                  </div>
                </div>
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">STATISTIK PENGUNJUNG DAFTAR PAKET</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <p class="display-5">4.121</p>
                      <p>pengunjung</p>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted text-left"><small class="font-italic text-white">menampilkan statistik jumlah pengunjung halaman paket yang ditawarkan.</small></div>
                  </div>
                </div>
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">STATISTIK DISKUSI</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <p class="display-5">15</p>
                      <p>diskusi</p>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted text-left"><small class="font-italic text-white">menampilkan statistik jumlah diskusi dengan pengguna.</small></div>
                  </div>
                </div>
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">STATISTIK TRANSAKSI</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <p class="display-5">1.398</p>
                      <p>transaksi</p>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted text-left"><small class="font-italic text-white">menampilkan statistik jumlah transaksi.</small></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection
    
@section('js') <!-- Make your custom JavaScript -->
    
@endsection