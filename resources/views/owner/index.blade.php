@extends('layouts.owner_layout')

@section('title', 'Dashboard Owner')

@section('navigation')
    @parent
@endsection

@section('content')
<section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('index.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/wp.jpg') }}" style="width: 25px; height: 25px;"> Daftar Order</a></li>
                <li class="list-group-item"><a href="{{ route('daftarharga.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/package.jpg') }}" style="width: 25px; height: 25px;"> Daftar Paket</a></li>
                <li class="list-group-item"><a href="{{ route('gallery.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/gallery.jpg') }}" style="width: 25px; height: 25px;"> Galeri</a></li>
                <li class="list-group-item"><a href="{{ route('statistics.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/statistics.jpg') }}" style="width: 25px; height: 25px;"> Statistik</a></li>
                <li class="list-group-item"><a href="{{ route('discussion.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/discussion.jpg') }}" style="width: 25px; height: 25px;"> Diskusi</a></li>
                <li class="list-group-item"><a href="{{ route('rating.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/feature/starrate.jpg') }}" style="width: 25px; height: 25px;"> Rating</a></li>
                <li class="list-group-item"><a href="{{ route('notification.owner') }}" class="text-darktosca"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"> Notifikasi</a></li>
              </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header bg-tosca text-white">
                      <div class="row">
                        <div class="col-6 text-left">ORDER AKTIF</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="card-group">
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">Invoice : 149797979</h5>
                            <p class="card-text"><small class="text-muted">Tanggal : 9 Oktober 2019</small></p>
                            <p class="card-text">
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" style="width: 25px; height: 25px;">
                              <small class="text-muted">Widya Widyastuti</small>
                            </p>
                            <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                            <p class="card-text"><small class="text-muted">MAKE UP PLATINUM</small></p>
                            <button type="button" class="btn btn-danger btn-sm btn-block">Belum Disetujui</button>
                          </div>
                          <div class="card-footer bg-tosca text-white text-muted"><a class="text-white" data-toggle="modal" data-target="#edit_order" style="cursor: pointer;">Edit</a> | <a class="text-white" data-toggle="modal" data-target="#delete_order" style="cursor: pointer;">Hapus</a> | <a class="text-white" href="{{ route('order.owner') }}">Lihat detail</a></div>
                        </div>
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">Invoice : 149797979</h5>
                            <p class="card-text"><small class="text-muted">Tanggal : 9 Oktober 2019</small></p>
                            <p class="card-text">
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" style="width: 25px; height: 25px;">
                              <small class="text-muted">Widya Widyastuti</small>
                            </p>
                            <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                            <p class="card-text"><small class="text-muted">MAKE UP PLATINUM</small></p>
                            <button type="button" class="btn btn-primary btn-sm btn-block">Selesai</button>
                          </div>
                          <div class="card-footer bg-tosca text-white text-muted"><a class="text-white" href="{{ route('order.owner') }}">Lihat detail</a></div>
                        </div>
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">Invoice : 149797979</h5>
                            <p class="card-text"><small class="text-muted">Tanggal : 9 Oktober 2019</small></p>
                            <p class="card-text">
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" style="width: 25px; height: 25px;">
                              <small class="text-muted">Widya Widyastuti</small>
                            </p>
                            <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                            <p class="card-text"><small class="text-muted">MAKE UP PLATINUM</small></p>
                            <button type="button" class="btn btn-success btn-sm btn-block">Sudah Disetujui</button>
                          </div>
                          <div class="card-footer bg-tosca text-white text-muted"><a class="text-white" href="{{ route('order.owner') }}">Lihat detail</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer bg-tosca text-white text-muted"><a class="text-white" href="{{ route('view_order_all.owner') }}">Lihat selengkapnya</a></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
    <div class="container">
      <form action="#" method="POST" class="needs-validation" novalidate>
        <div class="modal fade" id="edit_order">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header text-center display-4">
                Order Paket
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-5">
                    <div class="row">
                      <div class="col-5"><img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;"></div>
                      <div class="col-7">Trinity Make Up Utama</div>
                      <div class="col-md-12 text-center font-weight-bold m-3">PREMIUM VITUKO LOTI</div>
                      <div class="col-md-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat ornare porta. Sed sit amet ipsum sed massa fringilla congue. Donec a nibh dui. Duis ac ex metus. Etiam rutrum convallis lorem, eu dictum tellus faucibus non. Donec sagittis erat lacus, vel fermentum dolor pellentesque vel.</div>
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="row">
                      <div class="col-6 text-right p-2">
                        Nama klien yang memesan : 
                      </div>
                      <div class="col-6 p-2">
                        <img src="https://www.jamf.com/jamf-nation/img/default-avatars/generic-user-purple.png" style="width: 30px; height: auto;"> Anastasia Herawati
                      </div>
                      <div class="col-6 text-right p-2">
                        Tanggal pernikahan : 
                      </div>
                      <div class="col-6 p-2">
                        <input type="date" class="form-control" name="" required>
                      </div>
                      <div class="col-6 text-right p-2">
                        Tanggal pengantaran : 
                      </div>
                      <div class="col-6 p-2">
                        <input type="date" class="form-control" name="">
                        <div class="custom-control custom-checkbox mb-3 mt-1">
                          <input type="checkbox" class="custom-control-input" id="customCheck1">
                          <label class="custom-control-label" for="customCheck1"><small>Sama dengan tanggal pernikahan</small></label>
                        </div>
                      </div>
                      <div class="col-6 text-right p-2">
                        Termin Pembayaran : 
                      </div>
                      <div class="col-6 p-2">
                        <select class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih termin" data-hide-disabled="true" required>
                          <option value="1">1 kali bayar</option>
                          <option value="2">2 kali bayar</option>
                        </select>
                      </div>
                      <div class="col-6 text-right p-2">
                        Harga paket : 
                      </div>
                      <div class="col-6 p-2">
                        <p class="font-weight-bold">Rp23.000.000</p>
                      </div>
                      <div class="col-6 text-right p-2">
                        Biaya tambahan : 
                      </div>
                      <div class="col-6 p-2">
                        <input type="number" class="form-control" name="">
                      </div>
                      <div class="col-6 text-right p-2">
                        Keterangan : 
                      </div>
                      <div class="col-6 p-2">
                        <textarea class="form-control" name=""></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </div>
      </form>
        <div class="modal fade" id="delete_order">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header text-center display-4">
                Hapus oder
              </div>
              <div class="modal-body">
                <div class="row">
                  Yakin menghapus order ini?
                  <table class="table table-bordered table-hover mt-3">
                    <thead>
                      <tr>
                        <td>Invoice</td>
                        <td>INV12192182</td>
                      </tr>
                      <tr>
                        <td>Klien</td>
                        <td>Widya Setyawati</td>
                      </tr>
                      <tr>
                        <td>Tanggal order</td>
                        <td>19 Oktober 2019</td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm">Hapus</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </div>
      </div>   
@endsection

@section('footer')

@parent 

@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection
