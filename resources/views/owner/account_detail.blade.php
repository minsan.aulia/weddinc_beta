@extends('layouts.owner_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
  <section>
    <div class="container container-fluid mb-4">
      <div class="row">
        <div class="col-md-12">
          <div class="image-section">
            <img src="{{ ($cover = $profileVendor->cover) === NULL ? asset('assets/img/icon/dummy_cover.png') : asset('uploads/photoCover/'.$cover) }}">
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-3">
              <div class="row">
                <div class="col-md-12 ml-3">
                  <div class="row mb-3">
                    <div class="col-md-12 user-image text-center">
                      <img src="{{ ($photo = $profileVendor->photo) === NULL ? asset('assets/img/icon/dummy_user.png') : asset('uploads/photoProfile/'.$photo) }}" class="rounded-circle">
                    </div>
                    <div class="col-md-12 text-center mt-3 p-3 font-weight-bold border border-bottom-0">
                      {{ $vendor->name }}
                    </div>
                    <div class="col-md-12 text-center p-3 border border-bottom-0 border-top-0">
                      {{ $profileVendor->alamat }}, {{ $city->name }}
                    </div>
                    <div class="col-md-12 text-center p-3 border border-bottom-0 border-top-0">
                      {{ $vendor->type }} Vendor
                    </div>
                    <div class="col-md-12 text-center p-3 border border-top-0">
                      <div class="row">
                        <div class="col-md-6">165K Views</div>
                        <div class="col-md-6">75 Love</div>
                      </div>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-md-12 text-center mt-3 p-3 font-weight-bold border border-bottom-0">
                      <button class="btn btn-tosca">Diskusi dengan kami</button>
                    </div>
                    <div class="col-md-12 text-center p-3 border border-top-0">
                      <div class="row">
                        <div class="col-md-12">Hubungi Kami di</div>
                        <div class="col-md-12 text-center mt-3">
                          <div class="row">
                            <div class="col-2">
                              <img src="{{ asset('assets/img/icon/social/facebook.png') }}" style="width: 25px; height: 25px;">
                            </div>
                            <div class="col-2">
                              <img src="{{ asset('assets/img/icon/social/twitter.png') }}" style="width: 25px; height: 25px;">
                            </div>
                            <div class="col-2">
                              <img src="{{ asset('assets/img/icon/social/instagram.png') }}" style="width: 25px; height: 25px;">
                            </div>
                            <div class="col-2">
                              <img src="{{ asset('assets/img/icon/social/whatsapp.png') }}" style="width: 25px; height: 25px;">
                            </div>
                            <div class="col-2">
                              <img src="{{ asset('assets/img/icon/social/phone.png') }}" style="width: 25px; height: 25px;">
                            </div>
                            <div class="col-2">
                              <img src="{{ asset('assets/img/icon/social/email.png') }}" style="width: 25px; height: 25px;">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-9">
              <div class="col-md-12 mr-3 mt-3">
                <div class="col-md-12 profile-header">
                  <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-6 profile-header-section1 pull-left">
                      <h1 class="font-weight-bold">{{ $vendor->name }}</h1>
                      <h6 class="font-weight-bold">{{ $category->name }}</h6>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6 profile-header-section1 text-right pull-rigth">
                      Rating Vendor
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" href="#package" role="tab" data-toggle="tab">Paket</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#gallery" role="tab" data-toggle="tab">Galeri</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#review" role="tab" data-toggle="tab">Review</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#about" role="tab" data-toggle="tab">Tentang</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade show active" id="package">
                        <div class="card-group mt-3 mb-3">
                          <div class="card">
                            <div class="card-body">
                              <h5 class="card-title">PAKET SILVER</h5>
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                              <p class="card-text"><small class="text-muted">Lorem ipsum dolor sit amet.</small></p>
                            </div>
                            <div class="card-footer text-muted"><a href="{{ route('discussion.user') }}" class="text-darktosca" style="cursor: pointer;">Pesan</a></div>
                          </div>
                          <div class="card">
                            <div class="card-body">
                              <h5 class="card-title">PAKET GOLD</h5>
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                              <p class="card-text"><small class="text-muted">Lorem ipsum dolor sit amet.</small></p>
                            </div>
                            <div class="card-footer text-muted"><a href="{{ route('discussion.user') }}" class="text-darktosca" style="cursor: pointer;">Pesan</a></div>
                          </div>
                          <div class="card">
                            <div class="card-body">
                              <h5 class="card-title">PAKET PLATINUM</h5>
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                              <p class="card-text"><small class="text-muted">Lorem ipsum dolor sit amet.</small></p>
                            </div>
                            <div class="card-footer text-muted"><a href="{{ route('discussion.user') }}" class="text-darktosca" style="cursor: pointer;">Pesan</a></div>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="gallery">
                        <div class="card-group mt-3 mb-3">
                          <div class="card">
                            <div class="card-body">
                              <h5 class="card-title">JIN & JUN WEDDING</h5>
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                              <p class="card-text"><small class="text-muted">Lorem ipsum dolor sit amet.</small></p>
                            </div>
                            <div class="card-footer text-muted"><a href="{{ route('view_gallery.owner') }}" class="text-darktosca" style="cursor: pointer;">Lihat</a></div>
                          </div>
                          <div class="card">
                            <div class="card-body">
                              <h5 class="card-title">SURYA & WIDYA WEDDING</h5>
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                              <p class="card-text"><small class="text-muted">Lorem ipsum dolor sit amet.</small></p>
                            </div>
                            <div class="card-footer text-muted"><a href="{{ route('view_gallery.owner') }}" class="text-darktosca" style="cursor: pointer;">Lihat</a></div>
                          </div>
                          <div class="card">
                            <div class="card-body">
                              <h5 class="card-title">BUDI & ANI WEDDING</h5>
                              <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                              <p class="card-text"><small class="text-muted">Lorem ipsum dolor sit amet.</small></p>
                            </div>
                            <div class="card-footer text-muted"><a href="{{ route('view_gallery.owner') }}" class="text-darktosca" style="cursor: pointer;">Lihat</a></div>
                          </div>
                        </div>
                      </div>
                        <div role="tabpanel" class="tab-pane fade" id="review">
                          <div class="row">
                            <div class="col-md-12 card m-2">
                              <div class="row m-1">
                                <div class="col-md-3">
                                  <div class="col-md-12 text-center">
                                    <img src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle" width="50" height="50">
                                  </div>
                                  <div class="col-md-12 text-center">
                                    Evelyn Christiana
                                  </div>
                                </div>
                                <div class="col-md-9">
                                  <div class="row">
                                    <div class="col-md-12">Rating</div>
                                    <div class="col-md-12">Lorem ipsum dolor sit amet.</div>
                                    <div class="col-md-12 display-10">Date</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 m-2">
                              <div class="col-md-9 card p-3 float-right">
                                <div class="row justify-content-end">
                                  <div class="col-md-12">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="row">
                                          <div class="col-md-3"><img src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle" width="50" height="50"></div>
                                          <div class="col-md-9">Hi.. terima kasih telah menggunakan layanan kami.</div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      <div role="tabpanel" class="tab-pane fade" id="about">
                        <div class="col-md-12 card m-3">
                          <div class="row">
                            <div class="col-md-12 display-8 mb-2">About</div>
                            <div class="col-md-12">{!! $profileVendor->tentang !!}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection
