@extends('layouts.owner_layout')

@section('title', 'Account Detail')

@section('navigation')
    @parent
@endsection

@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
      <li data-target="#demo" data-slide-to="0" class="active"></li>
      <li data-target="#demo" data-slide-to="1"></li>
      <li data-target="#demo" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{ asset('assets/img/highlights/dashboard/1.png') }}" alt="Perjuangan" width="100%">
          <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Perjuangan</h3>
            <p>Hal paling membahagiakan di dunia adalah saat bisa bersama orang yang kita cintai.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/2.png') }}" alt="Masa Depan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Masa Depan</h3>
            <p>Pernikahan adalah awal menuju kehidupan yang bebas dan penuh rintangan.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/3.png') }}" alt="Kebersamaan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Kebersamaan</h3>
            <p>Saat bersama orang yang kita percaya, hidup akan menjadi lebih mudah.</p>
          </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>   
    <section class="site-section">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 text-center">
            <p class="display-8 font-weight-bold">Weddinc adalah sebuah perusahaan teknologi yang bergerak di bidang pernikahan.</p>
          </div>
          <div class="col-md-12 text-center">
            <p class="display-9 font-weight-bold text-darktosca">Yang bertujuan untuk para calon pengantin bisa mengurus pernikahannya dengan mudah, aman, dan nyaman.</p>
          </div>
          <div class="col-md-12 text-center p-5 mt-5 border border-bottom-0 border-right-0 border-left-0">
            <p class="display-7 font-weight-bold text-tosca">KEBAHAGIAAN DIMULAI DARI</p>
          </div>
          <div class="row col-md-12 text-center" style="height: 400px;">
            <div class="col-md-6 pl-0 pr-0" style="background: url({{ asset('assets/img/others/about/3.png') }}) no-repeat; background-size: cover;">
            </div>
            <div class="col-md-6 display-5 pl-0 pr-0">
              <div class="container h-100">
                <div class="row align-items-center h-100">
                    <div class="col-10 mx-auto">
                        <div class="h-100 justify-content-center">
                            <div class="mb-4">Jabodetabek...</div>
                            <div class="mt-4 display-8">...dan menyusul kota-kota lainnya</div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
          </div>
          <div class="col-md-12 p-5 mt-5 text-center mt-5 border border-bottom-0 border-right-0 border-left-0">
            <p class="display-7 font-weight-bold text-tosca font-italic">DEAR USER...</p>
          </div>
          <div class="row col-md-12 text-white">
            <div class="col-md-6" style="background: url({{ asset('assets/img/others/about/2.png') }}) no-repeat; background-size: cover;">
            </div>
            <div class="col-md-6 p-4" style="background: url({{ asset('assets/img/others/about/5.png') }}) no-repeat; background-size: cover;">
              @foreach ($about_user as $about_user)
                {!! $about_user->content !!}
              @endforeach
            </div>
          </div>
          <div class="col-md-12 p-5 mt-5 text-center mt-5 border border-bottom-0 border-right-0 border-left-0">
            <p class="display-7 font-weight-bold text-tosca font-italic">DEAR VENDOR...</p>
          </div>
          <div class="row col-md-12 text-white">
            <div class="col-md-6 p-4" style="background: url({{ asset('assets/img/others/about/4.png') }}) no-repeat; background-size: cover;">
              @foreach ($about_vendor as $about_vendor)
                {!! $about_vendor->content !!}
              @endforeach
            </div>
            <div class="col-md-6" style="background: url({{ asset('assets/img/others/about/1.png') }}) no-repeat; background-size: cover;">>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection