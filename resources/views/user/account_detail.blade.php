@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container container-fluid mb-4">
        <div class="row">
          <div class="col-md-12">
            <div class="image-section">
              <img src="{{ asset('assets/img/icon/dummy_cover.png') }}">
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-3">
                <div class="row">
                  <div class="col-md-12 ml-3">
                    <div class="row mb-3">
                      <div class="col-md-12 user-image text-center">
                        <img src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle">
                      </div>
                      <div class="col-md-12 text-center mt-3 p-3 font-weight-bold border border-bottom-0">
                        Nama User
                      </div>
                      <div class="col-md-12 text-center p-3 border border-bottom-0 border-top-0">
                        <a href="{{ route('discussion.owner') }}"><button class="btn btn-dustypink">Diskusi dengan pengguna</button></a>
                      </div>
                      <div class="col-md-12 text-center p-3 border border-bottom-0 border-top-0">
                        Alamat email
                      </div>
                      <div class="col-md-12 text-center p-3 border border-top-0">
                        Nomor telepon
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-9">
                <div class="col-md-12 mr-3 mt-3">
                  <div class="col-md-12 profile-header">
                    <div class="row">
                      <div class="col-md-8 col-sm-6 col-xs-6 profile-header-section1 pull-left">
                        <h1 class="font-weight-bold">Nama Pengguna</h1>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 mt-3">
                    <div class="row">
                      <div class="col-md-12 border">
                        <div class="col-md-12">
                          <h4>Semua ulasan</h4>
                        </div>
                          <div class="col-md-12 card m-3">
                            <div class="row m-1">
                              <div class="col-md-3">
                                <div class="col-md-12 text-center">
                                  <img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" width="50" height="50">
                                </div>
                                <div class="col-md-12 text-center">
                                  Make Up Tanah Abang
                                </div>
                              </div>
                              <div class="col-md-9">
                                <div class="row">
                                  <div class="col-md-12">Rating</div>
                                  <div class="col-md-12">Lorem ipsum dolor sit amet.</div>
                                  <div class="col-md-12 display-10">Date</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 card m-3">
                            <div class="row m-1">
                              <div class="col-md-3">
                                <div class="col-md-12 text-center">
                                  <img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" width="50" height="50">
                                </div>
                                <div class="col-md-12 text-center">
                                  Tuina Catering
                                </div>
                              </div>
                              <div class="col-md-9">
                                <div class="row">
                                  <div class="col-md-12">Rating</div>
                                  <div class="col-md-12">Lorem ipsum dolor sit amet.</div>
                                  <div class="col-md-12 display-10">Date</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 card m-3">
                            <div class="row m-1">
                              <div class="col-md-3">
                                <div class="col-md-12 text-center">
                                  <img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" width="50" height="50">
                                </div>
                                <div class="col-md-12 text-center">
                                  Vetero Hotel
                                </div>
                              </div>
                              <div class="col-md-9">
                                <div class="row">
                                  <div class="col-md-12">Rating</div>
                                  <div class="col-md-12">Lorem ipsum dolor sit amet.</div>
                                  <div class="col-md-12 display-10">Date</div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection