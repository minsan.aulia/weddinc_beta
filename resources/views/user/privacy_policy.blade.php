@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
      <li data-target="#demo" data-slide-to="0" class="active"></li>
      <li data-target="#demo" data-slide-to="1"></li>
      <li data-target="#demo" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{ asset('assets/img/highlights/dashboard/1.png') }}" alt="Perjuangan" width="100%">
          <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Perjuangan</h3>
            <p>Hal paling membahagiakan di dunia adalah saat bisa bersama orang yang kita cintai.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/2.png') }}" alt="Masa Depan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Masa Depan</h3>
            <p>Pernikahan adalah awal menuju kehidupan yang bebas dan penuh rintangan.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/3.png') }}" alt="Kebersamaan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Kebersamaan</h3>
            <p>Saat bersama orang yang kita percaya, hidup akan menjadi lebih mudah.</p>
          </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
    <section class="site-section">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 offset-md-1">
            <div style="text-align: center;">
              <b>KEBIJAKAN PRIVASI USER DAN VENDOR&nbsp;</b></div>
              <br />
              Adanya Kebijakan Privasi ini adalah komitmen nyata dari Weddinc untuk menghargai dan melindungi setiap data atau informasi pribadi dari Klien atau Vendor yang selanjutnya disebut “Pengguna” situs www.weddinc.id, situs-situs turunannya, serta aplikasi gawai Weddinc (selanjutnya disebut sebagai "Situs").<br />
              <br />
              Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari situs Weddinc sebagaimana tercantum dalam "Syarat &amp; Ketentuan" dan informasi lain yang tercantum di Situs) menetapkan dasar atas perolehan, pengumpulan, pengolahan, penganalisisan, penampilan, pembukaan, dan/atau segala bentuk pengelolaan yang terkait dengan data atau informasi yang Pengguna berikan kepada Weddinc atau yang Weddinc kumpulkan dari Pengguna, termasuk data pribadi Pengguna, baik pada saat Pengguna melakukan pendaftaran di Situs, mengakses Situs, maupun mempergunakan layanan-layanan pada Situs (selanjutnya disebut sebagai "data").<br />
              <br />
              Dengan mengakses dan/atau mempergunakan layanan Weddinc, Pengguna menyatakan bahwa setiap data Pengguna merupakan data yang benar dan sah, serta Pengguna memberikan persetujuan kepada Weddinc untuk memperoleh, mengumpulkan, menyimpan, mengelola dan mempergunakan data tersebut sebagaimana tercantum dalam Kebijakan Privasi maupun Syarat dan Ketentuan Weddinc.<br />
              <ul>
              <li><b>Perolehan dan Pengumpulan Data Pengguna</b></li>
              <li><b>Penggunaan Data</b></li>
              <li><b>Pengungkapan Data Pribadi Pengguna</b></li>
              <li><b>Cookies</b></li>
              <li><b>Pilihan Pengguna dan Transparansi</b></li>
              <li><b>Penyimpanan dan Penghapusan Informasi</b></li>
              <li><b>Pembaruan Kebijakan Privasi</b></li>
              </ul>
              <br />
              <b>1. PEROLEHAN DAN PENGUMPULAN DATA PENGGUNA</b><br />
              Weddinc mengumpulkan data Pengguna dengan tujuan untuk memproses transaksi Pengguna, mengelola dan memperlancar proses penggunaan Situs, serta tujuan-tujuan lainnya selama diizinkan oleh peraturan perundang-undangan yang berlaku. Adapun data Pengguna yang dikumpulkan adalah sebagai berikut:<br />
              <div>
              <ul style="list-style-type: none;">
              <li>A. Data yang diserahkan secara mandiri oleh Pengguna, termasuk namun tidak terbatas pada data yang diserahkan pada saat Pengguna:
                <ul style="list-style-type: none;">
                  <li>i. Membuat atau memperbarui akun Weddinc, termasuk diantaranya nama pengguna (username), alamat email, nomor telepon, password, alamat, foto, dan lain-lain;</li>
                  <li>ii. Menghubungi Weddinc, termasuk melalui layanan pengguna;</li>
                  <li>iii. Mengisi survei yang dikirimkan oleh Weddinc atau atas nama Weddinc;</li>
                  <li>iv. Melakukan interaksi dengan Pengguna (klien-vendor atau vendor-klien) lainnya melalui fitur diskusi, ulasan, rating, dan sebagainya;</li>
                  <li>v. Mempergunakan layanan-layanan pada Situs, termasuk data transaksi yang detil, diantaranya jenis, jumlah dan/atau keterangan dari produk/jasa/paket dipesan, alamat pengiriman, metode pembayaran yang digunakan, jumlah transaksi, tanggal dan waktu transaksi, serta detil transaksi lainnya;</li>
                  <li>vi. Mengisi data-data pembayaran pada saat Pengguna melakukan aktivitas transaksi pembayaran melalui Situs, termasuk namun tidak terbatas pada data rekening bank, kartu kredit, virtual account, instant payment, internet banking, gerai ritel; dan/atau</li>
                  <li>vii. Menggunakan fitur yang membutuhkan izin akses terhadap perangkat Pengguna.</li>
                </ul>
              </li>
              <li>B. Data yang terekam pada saat Pengguna mempergunakan Situs, termasuk namun tidak terbatas pada:
                <ul style="list-style-type: none;">
                  <li>i. Data lokasi riil atau perkiraannya seperti alamat IP, lokasi Wi-Fi, geo-location, dan sebagainya;</li>
                  <li>ii.Data berupa waktu dari setiap aktivitas Pengguna, termasuk aktivitas pendaftaran, login, transaksi, dan lain sebagainya;</li>
                  <li>iii.Data penggunaan atau preferensi Pengguna, diantaranya interaksi Pengguna dalam menggunakan Situs, pilihan yang disimpan, serta pengaturan yang dipilih. Data tersebut diperoleh menggunakan cookies, pixel tags, dan teknologi serupa yang menciptakan dan mempertahankan pengenal unik;</li>
                  <li>iv. Data perangkat, diantaranya jenis perangkat yang digunakan untuk mengakses Situs, termasuk model perangkat keras, sistem operasi dan versinya, perangkat lunak, nama file dan versinya, pilihan bahasa, pengenal perangkat unik, pengenal iklan, nomor seri, informasi gerakan perangkat, dan/atau informasi jaringan seluler;</li>
                  <li>v. Data catatan (log), diantaranya catatan pada server yang menerima data seperti alamat IP perangkat, tanggal dan waktu akses, fitur aplikasi atau laman yang dilihat, proses kerja aplikasi dan aktivitas sistem lainnya, jenis peramban, dan/atau situs atau layanan pihak ketiga yang Anda gunakan sebelum berinteraksi dengan Situs.</li>
                </ul>
              </li>
              <li>C. Data yang diperoleh dari sumber lain, termasuk:
                <ul style="list-style-type: none;">
                  <li>i. Mitra Weddinc yang turut membantu Weddinc dalam mengembangkan dan menyajikan layanan-layanan dalam Situs kepada Pengguna, antara lain mitra penyedia layanan pembayaran, infrastruktur situs, dan mitra-mitra lainnya.</li>
                  <li>ii. Mitra Weddinc tempat Pengguna membuat atau mengakses akun Weddinc, seperti layanan media sosial, atau situs/aplikasi yang menggunakan API Weddinc atau yang digunakan Weddinc;</li>
                  <li>iii. Penyedia layanan finansial, termasuk namun tidak terbatas pada lembaga atau biro pemeringkat kredit atau Lembaga Pengelola Informasi Perkreditan (LPIP);</li>
                  <li>iv. Penyedia layanan pemasaran;</li>
                  <li>v. Sumber yang tersedia secara umum.</li>
                </ul>
              </li>
              </ul>
              </div>
              <div>
              <br />
              Weddinc dapat menggabungkan data yang diperoleh dari sumber tersebut dengan data lain yang dimilikinya.<br />
              <br />
              <b>2. PENGGUNAAN DATA</b><br />
              Weddinc dapat menggunakan keseluruhan atau sebagian data yang diperoleh dan dikumpulkan dari Pengguna sebagaimana disebutkan dalam bagian sebelumnya untuk hal-hal sebagai berikut:<br />
              <ul style="list-style-type: none;">
              <li>A. Memproses segala bentuk permintaan, aktivitas maupun transaksi yang dilakukan oleh Pengguna melalui Situs, termasuk untuk keperluan pengiriman atau pengantaran produk/jasa/paket kepada Pengguna.</li>
              <li>B. Penyediaan fitur-fitur untuk memberikan, mewujudkan, memelihara dan memperbaiki produk dan layanan kami, termasuk namun tidak terbatas pada:
                <ul style="list-style-type: none;">
                  <li>i. Menawarkan, memperoleh, menyediakan, atau memfasilitasi layanan marketplace, pinjaman, maupun produk-produk lainnya melalui Situs;</li>
                  <li>ii. Memungkinkan fitur untuk mempribadikan akun Weddinc Pengguna, seperti Wishlist dan Vendor Favorit; dan/atau</li>
                  <li>iii. Melakukan kegiatan internal yang diperlukan untuk menyediakan layanan pada situs/aplikasi Weddinc, seperti pemecahan masalah software, bug, permasalahan operasional, melakukan analisis data, pengujian, dan penelitian, dan untuk memantau dan menganalisis kecenderungan penggunaan dan aktivitas.</li>
                </ul>
              </li>
              <li>C. Membantu Pengguna pada saat berkomunikasi dengan Layanan Pelanggan Weddinc, diantaranya untuk:
                <ul style="list-style-type: none;">
                  <li>i. Memeriksa dan mengatasi permasalahan Pengguna;</li>
                  <li>ii. Mengarahkan pertanyaan Pengguna kepada petugas Layanan Pelanggan yang tepat untuk mengatasi permasalahan; dan</li>
                  <li>iii. Mengawasi dan memperbaiki tanggapan Layanan Pelanggan Weddinc.</li>
                </ul>
              </li>
              <li>D. Menghubungi Pengguna melalui email, surat, telepon, fax, dan lain-lain, termasuk namun tidak terbatas, untuk membantu dan/atau menyelesaikan proses transaksi maupun proses penyelesaian kendala.</li>
              <li>E. Menggunakan informasi yang diperoleh dari Pengguna untuk tujuan penelitian, analisis, pengembangan dan pengujian produk guna meningkatkan keamanan dan keamanan layanan-layanan pada Situs, serta mengembangkan fitur dan produk baru.</li>
              <li>F. Menginformasikan kepada Pengguna terkait produk, layanan, promosi, studi, survei, berita, perkembangan terbaru, acara dan lain-lain, baik melalui Situs maupun melalui media lainnya. Weddinc juga dapat menggunakan informasi tersebut untuk mempromosikan dan memproses kontes dan undian, memberikan hadiah, serta menyajikan iklan dan konten yang relevan tentang layanan Weddinc dan mitranya.</li>
              <li>G. Melakukan monitoring ataupun investigasi terhadap transaksi-transaksi mencurigakan atau transaksi yang terindikasi mengandung unsur kecurangan atau pelanggaran terhadap Syarat dan Ketentuan atau ketentuan hukum yang berlaku, serta melakukan tindakan-tindakan yang diperlukan sebagai tindak lanjut dari hasil monitoring atau investigasi transaksi tersebut.</li>
              <li>H. Dalam keadaan tertentu, Weddinc mungkin perlu untuk menggunakan ataupun mengungkapkan data Pengguna untuk tujuan penegakan hukum atau untuk pemenuhan persyaratan hukum dan peraturan yang berlaku, termasuk dalam hal terjadinya sengketa atau proses hukum antara Pengguna dan Weddinc.</li>
              </ul>
              <br />
              <b>3. PENGUNGKAPAN DATA PRIBADI PENGGUNA</b><br />
              Weddinc menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan data pribadi Anda kepada pihak ketiga lain, tanpa terdapat izin dari Anda, kecuali dalam hal-hal sebagai berikut:<br />
              <ul style="list-style-type: none;">
              <li>A. Dibutuhkan adanya pengungkapan data Pengguna kepada vendor dan atau mitra atau pihak ketiga lain yang membantu Weddinc dalam menyajikan layanan pada Situs dan memproses segala bentuk aktivitas Pengguna dalam Situs, termasuk memproses transaksi, verifikasi pembayaran, pengiriman atau pengantaran produk/jasa/paket, dan lain-lain.</li>
              <li>B. Weddinc dapat menyediakan informasi yang relevan kepada mitra Weddinc sesuai dengan persetujuan Pengguna untuk menggunakan layanan mitra, termasuk diantaranya aplikasi atau situs lain yang telah saling mengintegrasikan API atau layanannya, atau mitra yang mana Weddinc telah bekerjasama untuk mengadakan promosi, kontes, atau layanan yang dikhususkan.</li>
              <li>C. Dibutuhkan adanya komunikasi antara mitra Weddinc (seperti penyedia pembayaran dan lain-lain) dengan Pengguna dalam hal penyelesaian kendala maupun hal-hal lainnya.</li>
              <li>D. Weddinc dapat menyediakan informasi yang relevan kepada konsultan, mitra pemasaran, firma riset, atau penyedia layanan sejenis.</li>
              <li>E. Pengguna menghubungi Weddinc melalui media publik seperti blog, media sosial, dan fitur tertentu pada Situs, komunikasi antara Pengguna dan Weddinc mungkin dapat dilihat secara publik.</li>
              <li>F. Weddinc dapat membagikan informasi Pengguna kepada anak perusahaan dan afiliasinya untuk membantu memberikan layanan atau melakukan pengolahan data untuk dan atas nama Weddinc.</li>
              <li>G. Weddinc mengungkapkan data Pengguna dalam upaya mematuhi kewajiban hukum dan/atau adanya permintaan yang sah dari aparat penegak hukum.</li>
              </ul>
              <br />
              <b>4. COOKIES</b><br />
              <ul style="list-style-type: none;">
              <li>A. Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat Pengguna yang menjalankan fungsi dalam menyimpan preferensi maupun konfigurasi Pengguna selama mengunjungi suatu situs.</li>
              <li>B. Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses data lain yang Pengguna miliki di perangkat komputer Pengguna, selain dari yang telah Pengguna setujui untuk disampaikan.</li>
              <li>C. Walaupun secara otomatis perangkat komputer Pengguna akan menerima cookies, Pengguna dapat menentukan pilihan untuk melakukan modifikasi melalui pengaturan browser Pengguna yaitu dengan memilih untuk menolak cookies (pilihan ini dapat membatasi layanan optimal pada saat melakukan akses ke Situs).</li>
              <li>D. Weddinc menggunakan fitur Google Analytics Demographics and Interest. Data yang kami peroleh dari fitur tersebut, seperti umur, jenis kelamin, minat Pengguna dan lain-lain, akan kami gunakan untuk pengembangan Situs dan konten Weddinc.</li>
              <li>E. Weddinc dapat menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka meningkatkan layanan dan konten Weddinc, termasuk diantaranya ialah penyesuaian dan penyajian iklan kepada setiap Pengguna berdasarkan minat atau riwayat kunjungan. Jika Anda tidak ingin iklan ditampilkan berdasarkan penyesuaian tersebut, maka Anda dapat mengaturnya melalui browser.</li>
              </ul>
              <br /></div>
              <div>
              <b>5. PILIHAN PENGGUNA DAN TRANSPARANSI</b><br />
              <ul style="list-style-type: none;">
              <li>A. Perangkat seluler pada umumnya (iOS, Android, dan sebagainya) memiliki pengaturan sehingga aplikasi Weddinc tidak dapat mengakses data tertentu tanpa persetujuan dari Pengguna. Perangkat iOS akan memberikan pemberitahuan kepada Pengguna saat aplikasi Weddinc pertama kali meminta akses terhadap data tersebut, sedangkan perangkat Android akan memberikan pemberitahuan kepada Pengguna saat aplikasi Weddinc pertama kali dimuat. Dengan menggunakan aplikasi dan memberikan akses terhadap aplikasi, Pengguna dianggap memberikan persetujuannya terhadap Kebijakan Privasi.</li>
              <li>B. Setelah transaksi jual beli melalui situs Weddinc berhasil, Klien memiliki kesempatan untuk memberikan penilaian dan ulasan terhadap vendor dan Weddinc. Informasi ini mungkin dapat dilihat secara publik.</li>
              <li>C. Pengguna dapat mengakses dan mengubah informasi berupa alamat email, nomor telepon, tanggal lahir, jenis kelamin, daftar alamat, metode pembayaran, dan rekening bank melalui fitur Pengaturan pada Situs.</li>
              <li>D. Sejauh diizinkan oleh ketentuan yang berlaku, Pengguna dapat menghubungi Weddinc untuk melakukan penarikan persetujuan terhadap perolehan, pengumpulan, penyimpanan, pengelolaan dan penggunaan data Pengguna. Apabila terjadi demikian maka Pengguna memahami konsekuensi bahwa Pengguna tidak dapat menggunakan layanan Situs maupun layanan Weddinc lainnya.</li>
              </ul>
              <br />
              <b>6. PENYIMPANAN DAN PENGHAPUSAN INFORMASI</b><br />
              Weddinc akan menyimpan informasi selama akun Pengguna tetap aktif dan dapat melakukan penghapusan sesuai dengan ketentuan peraturan hukum yang berlaku.<br />
              <br />
              <b>7. PEMBARUAN KEBIJAKAN PRIVASI</b><br />
              Weddinc dapat sewaktu-waktu melakukan perubahan atau pembaruan terhadap Kebijakan Privasi ini. Weddinc menyarankan agar Pengguna membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Situs maupun layanan Weddinc lainnya, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi.</div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection