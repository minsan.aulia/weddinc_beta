@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Rincian Akun</a></li>
                  <li class="list-group-item"><a href="{{ route('notification.user') }}" class="text-darkpink"><i class="fa fa-bell"></i> Notifikasi</a></li>
                  <li class="list-group-item"><a href="{{ route('wishlist.user') }}" class="text-darkpink"><i class="fa fa-heart"></i> Wishlist</a></li>
                  <li class="list-group-item"><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                  <li class="list-group-item"><a href="{{ route('discussion.user') }}" class="text-darkpink"><i class="fa fa-comment"></i> Diskusi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-6 text-left">KERANJANG</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <table class="table">
                          <thead>
                            <tr>
                              <td>1</td>
                              <td>
                                <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 150px; height: auto;">
                              </td>
                              <td><p>Make Up Casual</p><p><small>Valeria Make Up</small></p></td>
                              <td>Make Up</td>
                              <td>Rp1.500.000,-</td>
                              <td><button type="button" class="btn btn-success btn-sm btn-block">Disetujui</button></td>
                              <td>
                                <a href="{{ route('cart_view.user') }}"><button type="button" class="btn btn-dustypink btn-sm border" >Lihat</button></a>
                              </td>
                            </tr>
                            <tr>
                              <td>2</td>
                              <td>
                                <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 150px; height: auto;">
                              </td>
                              <td><p>Eloka Paket 100</p><p><small>Ayam Geprek Menteng</small></p></td>
                              <td>Catering</td>
                              <td>Rp11.000.000,-</td>
                              <td><button type="button" class="btn btn-primary btn-sm btn-block">Selesai</button></td>
                              <td>
                                <a href="{{ route('cart_view.user') }}"><button type="button" class="btn btn-dustypink btn-sm border" >Lihat</button></a>
                              </td>
                            </tr>
                            <tr>
                              <td>3</td>
                              <td>
                                <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 150px; height: auto;">
                              </td>
                              <td><p>Paket Hemat 500</p><p><small>Soto Babat Bumiayu</small></p></td>
                              <td>Catering</td>
                              <td>Rp5.000.000,-</td>
                              <td><button type="button" class="btn btn-danger btn-sm btn-block">Belum Disetujui</button></td>
                              <td>
                                <a href="{{ route('cart_view.user') }}"><button type="button" class="btn btn-dustypink btn-sm border" >Lihat</button></a>
                              </td>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                    <div class="card-footer text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection