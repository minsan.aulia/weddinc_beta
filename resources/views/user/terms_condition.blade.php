@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
      <li data-target="#demo" data-slide-to="0" class="active"></li>
      <li data-target="#demo" data-slide-to="1"></li>
      <li data-target="#demo" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{ asset('assets/img/highlights/dashboard/1.png') }}" alt="Perjuangan" width="100%">
          <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Perjuangan</h3>
            <p>Hal paling membahagiakan di dunia adalah saat bisa bersama orang yang kita cintai.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/2.png') }}" alt="Masa Depan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Masa Depan</h3>
            <p>Pernikahan adalah awal menuju kehidupan yang bebas dan penuh rintangan.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/3.png') }}" alt="Kebersamaan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Kebersamaan</h3>
            <p>Saat bersama orang yang kita percaya, hidup akan menjadi lebih mudah.</p>
          </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
    <section class="site-section">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 offset-md-1">
            <div style="text-align: center;">
                <b>SYARAT DAN KETENTUAN PENGGUNA WEDDINC</b></div>
                <br />
                Halo terima kasih telah bergabung dengan Weddinc<br />
                <br />
                Mari Wujudkan pernikahan impian anda.<br />
                <br />
                Berikut adalah syarat dan kententuan calon pengantin atau klien yang selanjutnya disebut “Pengguna” platform Weddinc (PT Wahana Sidharta Madjadikara) berisikan semua peraturan dan ketentuan yang secara otomatis mengikat ketika anda melakukan kunjungan ke situs web dan atau aplikasi Weddinc.<br />
                <br />
                Silahkan membaca dengan seksama seluruh konten Syarat dan Ketentuan untuk dapat memahami batasan hak dan kewajiban anda sebagai pengguna atau pengunjung Platform Weddinc.<br />
                Bilamana tidak setuju dengan konten syarat dan ketentuan ini, anda dapat berhenti mengakses Platform Weddinc.<br />
                <ul>
                <li><b>DEFINISI</b></li>
                <li><b>AKUN, PASSWORD DAN KEAMANAN</b></li>
                <li><b>TRANSAKSI</b></li>
                <li><b>HARGA</b></li>
                <li><b>PROMO</b></li>
                <li><b>PENGIRIMAN ATAU TRANSPORTASI</b></li>
                <li><b>KETENTUAN PEMBATALAN</b></li>
                <li><b>KETENTUAN LAIN</b></li>
                <li><b>PENOLAKAN JAMINAN DAN BATASAN TANGGUNG JAWAB</b></li>
                <li><b>PELEPASAN</b></li>
                <li><b>GANTI RUGI</b></li>
                <li><b>PILIHAN HUKUM</b></li>
                <li><b>PEMBAHARUAN</b></li>
                </ul>
                <br />
                <b>1. DEFINISI</b><br />
                <ul style="list-style-type: none;">
                    <li>a) PT Wahana Sidharta Madjadikara adalah suatu perseroan terbatas yang menjalankan kegiatan usaha jasa web portal www.weddinc.id, yakni situs pencarian toko dan Barang yang dijual oleh Vendor terdaftar. Selanjutnya disebut Weddinc.</li>
                    <li>b) Situs Weddinc adalah www.weddinc.id.</li>
                    <li>c) Syarat &amp; ketentuan adalah perjanjian antara Pengguna dan Weddinc yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab pengguna dan Weddinc, serta tata cara penggunaan sistem layanan Weddinc.</li>
                    <li>d) Pengunjung adalah setiap orang yang mengakses situs tanpa melakukan registrasi dan atau login.</li>
                    <li>e) Pengguna adalah pihak yang menggunakan dan melakukan transaksi layanan Weddinc maupun pihak lain yang sekedar berkunjung ke Situs Weddinc.</li>
                    <li>f) Klien adalah Pengguna terdaftar yang melakukan permintaan atas produk/jasa/paket yang ditawarkan oleh vendor di Situs Weddinc.</li>
                </ul>
                <br />
                <b>2. AKUN</b><br />
                <ul style="list-style-type: none;">
                    <li>a) Pengguna dengan ini menyatakan bahwa pengguna adalah orang yang cakap dan mampu untuk mengikatkan dirinya dalam sebuah perjanjian yang sah menurut hukum.</li>
                    <li>b) Weddinc tidak memungut biaya pendaftaran kepada Pengguna.</li>
                    <li>c) Pengguna yang telah mendaftar berhak bertindak sebagai klien atas vendor yang teregistrasi pada platform Weddinc.</li>
                    <li>d) Weddinc tanpa pemberitahuan terlebih dahulu kepada Pengguna, memiliki kewenangan untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran Syarat &amp; ketentuan dan/atau hukum yang berlaku, yakni tindakan berupa suspensi akun, dan/atau penghapusan akun pengguna.</li>
                    <li>e) Refund adalah berasal dari pengembalian dana transaksi produk/jasa/paket di Situs Weddinc.</li>
                    <li>f) Weddinc memiliki kewenangan untuk melakukan hold / penahanan Refund Pengguna apabila ditemukan / diduga adanya tindak kecurangan dalam bertransaksi dan/atau pelanggaran terhadap syarat dan ketentuan Weddinc.</li>
                    <li>g) Pengguna bertanggung jawab secara pribadi untuk menjaga kerahasiaan akun dan password untuk semua aktivitas yang terjadi dalam akun Pengguna.</li>
                    <li>h) Weddinc tidak akan meminta termasuk namun tidak terbatas username, password maupun kode SMS verifikasi atau kode one time password (OTP) milik akun Pengguna untuk alasan apapun, oleh karena itu Weddinc menghimbau Pengguna agar tidak memberikan data tersebut maupun data penting lainnya kepada pihak yang mengatasnamakan Weddinc atau pihak lain yang tidak dapat dijamin keamanannya.</li>
                    <li>i) Pengguna setuju untuk memastikan bahwa Pengguna keluar dari akun di akhir setiap sesi dan memberitahu Weddinc jika ada penggunaan tanpa izin atas sandi atau akun Pengguna.</li>
                    <li>j) Pengguna dengan ini menyatakan bahwa Weddinc tidak bertanggung jawab atas kerugian ataupun kendala yang timbul atas penyalahgunaan akun Pengguna yang diakibatkan oleh kelalaian Pengguna, termasuk namun tidak terbatas pada meminjamkan atau memberikan akses akun kepada pihak lain, mengakses link atau tautan yang diberikan oleh pihak lain, memberikan atau memperlihatkan kode verifikasi (OTP), password atau email kepada pihak lain, maupun kelalaian Pengguna lainnya yang mengakibatkan kerugian ataupun kendala pada akun Pengguna.</li>
                    <li>k) Pengguna memahami dan menyetujui bahwa untuk mempergunakan fasilitas keamanan OTP maka penyedia jasa telekomunikasi terkait dapat sewaktu-waktu mengenakan biaya kepada Pengguna.</li>
                </ul>
                <br />
                <b>3. TRANSAKSI</b><br />
                <ul style="list-style-type: none;">
                    <li>a) Demi keamanan Pengguna diwajibkan bertransaksi melalui prosedur transaksi yang telah ditetapkan oleh Weddinc. Pengguna melakukan pembayaran dengan menggunakan metode pembayaran yang sebelumnya telah dipilih oleh Pengguna, dan kemudian Weddinc akan meneruskan dana ke pihak Vendor apabila tahapan transaksi pada sistem Weddinc telah selesai.</li>
                    <li>b) Saat melakukan transaksi produk/jasa/paket, Pengguna menyetujui bahwa:
                        <ul style="list-style-type: none;">
                            <li>i.Pengguna bertanggung jawab untuk membaca, memahami, dan menyetujui informasi/deskripsi keseluruhan produk/jasa/paket sebelum membuat tawaran atau komitmen untuk bertransaksi.</li>
                            <li>ii. Pengguna masuk ke dalam kontrak yang mengikat secara hukum untuk membeli produk/jasa/paket ketika Pengguna bertransaksi.</li>
                            <li>iii. Weddinc tidak mengalihkan kepemilikan secara hukum atas barang-barang dari Vendor kepada Pengguna.</li>
                        </ul>
                    </li>
                    <li>c) Pengguna memahami dan menyetujui bahwa ketersediaan produk/jasa/paket merupakan tanggung jawab vendor yang menawarkan hal tersebut. Terkait ketersediaan produk/jasa/paket dapat berubah sewaktu-waktu, sehingga dalam keadaan tidak tersedia, maka vendor akan menolak pesanan, dan pembayaran atas produk/jasa/paket yang bersangkutan dikembalikan kepada Pengguna.</li>
                    <li>d) Pengguna memahami sepenuhnya dan menyetujui bahwa segala transaksi yang dilakukan antara Pengguna dan vendor di luar sistem pembayaran Weddinc dan/atau tanpa sepengetahuan Weddinc (melalui fasilitas/jaringan pribadi, pengiriman pesan, pengaturan transaksi khusus diluar situs Weddinc atau upaya lainnya) adalah merupakan tanggung jawab pribadi dari Pengguna.</li>
                    <li>e) Pembayaran oleh Pengguna wajib dilakukan segera dengan rincian sebagai berikut:
                        <ul style="list-style-type: none;">
                            <li>i. Jika durasi pemesanan produk/jasa/paket dengan tanggal pemenuhan yang diminta oleh pengguna 14 hari atau kurang. Maka termin pembayaran yang berlaku hanya satu kali dan selambat-lambatnya dilakukan segera dalam waktu 1x24 jam setelah check-out.</li>
                            <li>ii. Jika jarak pemesanan produk/jasa/paket dengan tanggal pemenuhan yang diminta oleh pengguna 14 hari atau lebih dan transaksi di atas 3,5 juta Rupiah (Tiga juta lima ratus ribu rupiah). Maka akan ada pilihan termin pembayaran sebanyak 2(dua) kali dengan pembagian  nominal 50:50. Dengan rincian:
                                <ul style="list-style-type: none;">
                                    <li>a. Termin 1: 50% (dari total harga) dilakukan segera selambat-lambatnya 1x24 jam setelah pesanan yang diajukan oleh vendor disetujui.</li>
                                    <li>b. Termin 2: 50% (sisa pembayaran) dilakukan segera selambat-lambatnya sebelum H-14 dengan tanggal pengantaran yang diminta.</li>
                                </ul>
                            </li>
                            <li>iii. Jika jarak pemesanan produk/jasa/paket dengan tanggal pengantaran yang diminta oleh pengguna 14 hari atau lebih dan transaksi di bawah 3,5 juta Rupiah (Tiga juta lima ratus ribu rupiah). Maka hanya akan ada satu termin pembayaran yang dilakukan segera selambat-lambatnya 1x24 jam setelah pesanan yang diajukan vendor disetujui.</li>
                        </ul>
                    </li>
                    <li>f) Konfirmasi pembayaran dilakukan dengan sistem yang berlaku dan dapat berubah sewaktu-waktu dan dapat berubah sesuai dengan kebijakan perusahaan.</li>
                    <li>g) Jika dalam waktu yang sudah ditentukan oleh sistem terkait tenggat waktu pembayaran telah terlewati, maka pesanan otomatis akan dibatalkan oleh sistem. </li>
                    <li>h) Pengguna menyetujui untuk tidak memberitahukan atau menyerahkan bukti pembayaran dan/atau data pembayaran kepada pihak lain selain Weddinc. Dalam hal terjadi kerugian akibat pemberitahuan atau penyerahan bukti pembayaran dan/atau data pembayaran oleh Pengguna kepada pihak lain, maka hal tersebut akan menjadi tanggung jawab Pengguna.</li>
                    <li>i) Pengguna wajib melakukan konfirmasi pesanan selesai, setelah vendor selesai memenuhi pesanan. Weddinc memberikan batas waktu 2 (dua) hari untuk Pengguna melakukan konfirmasi. Jika dalam batas waktu tersebut tidak ada konfirmasi atau klaim dari pihak Pengguna, maka dengan demikian Pengguna menyatakan menyetujui dilakukannya konfirmasi pesanan selesai secara otomatis oleh sistem Weddinc.</li>
                    <li>j) Setelah adanya konfirmasi pesanan selesai atau konfirmasi pesanan selesai otomatis, maka dana pihak Pengguna yang telah dikirimkan ke Rekening resmi Weddinc akan dilanjut kirimkan ke pihak Vendor (transaksi dianggap selesai).</li>
                    <li>k) Pengguna memahami dan menyetujui bahwa setiap klaim yang dilayangkan setelah adanya konfirmasi / konfirmasi otomatis penerimaan Barang adalah bukan menjadi tanggung jawab Weddinc. Kerugian yang timbul setelah adanya konfirmasi/konfirmasi otomatis pesanan selesai menjadi tanggung jawab Pengguna secara pribadi.</li>
                    <li>l) Pengguna memahami dan menyetujui bahwa masalah keterlambatan proses pembayaran dan biaya tambahan yang disebabkan oleh perbedaan bank yang Pengguna pergunakan dengan bank Rekening resmi Weddinc adalah tanggung jawab Pengguna secara pribadi.</li>
                    <li>m) Pengembalian dana dari Weddinc kepada Pengguna hanya dapat dilakukan jika dalam keadaan-keadaan tertentu berikut ini:
                        <ul style="list-style-type: none;">
                            <li>i. Masalah pengiriman Barang telah teridentifikasi secara jelas dari vendor yang mengakibatkan pesanan produk/jasa/paket tidak sampai,</li>
                            <li>ii. Vendor tidak bisa menyanggupi order karena kehabisan stok, perubahan ongkos kirim, maupun penyebab lainnya.</li>
                            <li>iii. Vendor sudah menyanggupi pesanan, tetapi setelah batas waktu yang ditentukan ternyata Vendor tidak memenuhi pesanan hingga batas waktu yang telah ditentukan.</li>
                            <li>iv. Penyelesaian permasalahan melalui Pusat Bantuan berupa keputusan untuk pengembalian dana kepada Pengguna atau hasil keputusan dari pihak Weddinc.</li>
                        </ul>
                    </li>
                    <li>n) Apabila terjadi proses pengembalian dana, maka pengembalian akan dilakukan melalui transfer bank milik Pengguna yang akan bertambah sesuai dengan jumlah pengembalian dana.</li>
                    <li>o) Weddinc berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum terselesaikan akibat tidak adanya kesepakatan penyelesaian, baik antara Vendor dan Pengguna, dengan melihat bukti-bukti yang ada. Keputusan Weddinc adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Vendor dan Pengguna untuk mematuhinya.</li>
                    <li>p) Apabila Pengguna memilih menggunakan metode pembayaran transfer bank, maka total pembayaran akan ditambahkan kode unik untuk mempermudah proses verifikasi.</li>
                    <li>q) Pengguna wajib melakukan pembayaran dengan nominal yang sesuai dengan jumlah tagihan beserta kode unik (apabila ada) yang tertera pada halaman pembayaran. Weddinc tidak bertanggungjawab atas kerugian yang dialami Pengguna apabila melakukan pembayaran yang tidak sesuai dengan jumlah tagihan yang tertera pada halaman pembayaran.</li>
                    <li>r) Pengguna memahami sepenuhnya dan menyetujui bahwa invoice yang diterbitkan adalah atas nama Vendor.</li>
                </ul>
                <br />
                <b>4. HARGA</b><br />
                <ul style="list-style-type: none;">
                    <li>a) Harga Barang yang terdapat dalam situs Weddinc adalah harga yang ditetapkan oleh Vendor. </li>
                    <li>b) Pembeli memahami dan menyetujui bahwa kesalahan keterangan harga dan informasi lainnya yang disebabkan tidak terbaharuinya halaman situs Weddinc dikarenakan browser/ISP yang dipakai Pembeli adalah tanggung jawab Pembeli.</li>
                    <li>c) Pengguna memahami dan menyetujui bahwa setiap masalah dan/atau perselisihan yang terjadi akibat ketidaksepahaman antara Vendor dan pengguna tentang harga bukanlah merupakan tanggung jawab Weddinc.</li>
                    <li>d) Dengan melakukan pemesanan melalui Weddinc, Pengguna menyetujui untuk membayar total biaya yang harus dibayarkan sebagaimana tertera dalam halaman pembayaran, yang terdiri dari harga produk/jasa/paket, biaya tambahan(jika ada), dan biaya-biaya lain yang mungkin timbul dan akan diuraikan secara tegas dalam halaman pembayaran. Pengguna setuju untuk melakukan pembayaran melalui metode pembayaran yang telah dipilih sebelumnya oleh Pengguna.</li>
                    <li>e) Jika ada perubahan terhadap rincian produk/jasa/paket vendor setelah pengguna menyetujui dan melakukan pembayaran maka rincian produk/jasa/paket yang berlaku adalah yang sebelumnya dan atau tertera di invoice.</li>
                    <li>f) Situs Weddinc hanya melayani transaksi dalam mata uang Rupiah.</li>
                </ul>
                <br />
                <b>5. PROMO</b><br />
                <ul style="list-style-type: none;">
                    <li>a) Weddinc sewaktu-waktu dapat mengadakan kegiatan promosi (selanjutnya disebut sebagai “Promo/Deals”) dengan Syarat dan Ketentuan yang mungkin berbeda pada masing-masing kegiatan Promo. Pengguna dihimbau untuk membaca dengan seksama Syarat dan Ketentuan Promo tersebut.</li>
                    <li>b) Pengguna hanya boleh menggunakan 1 (satu) akun Weddinc untuk mengikuti setiap promo Weddinc. Jika ditemukan pembuatan lebih dari 1 (satu) akun oleh 1 (satu) Pengguna yang sama dan/atau nomor handphone yang sama dan/atau ID pelanggan yang sama dan/atau identitas pembayaran yang sama dan/atau riwayat transaksi yang sama, maka Pengguna tidak berhak mendapatkan manfaat dari promo Weddinc.</li>
                </ul>
                <br />
                <b>6. PENGIRIMAN ATAU TRANSPORTASI</b><br />
                <ul style="list-style-type: none;">
                    <li>a) Weddinc tidak memfasilitasi transportasi atau pengiriman produk/jasa/paket yang ditawarkan oleh vendor kepada pengguna.</li>
                    <li>Semua bentuk transportasi atau pengiriman produk/jasa/paket kepada pengguna dilakukan sepenuhnya atas hasil diskusi vendor dan pengguna.</li>
                    <li>b) Bila terjadi biaya tambahan termasuk namun tidak terbatas seperti ongkos transport vendor (di luar harga produk/jasa/paket utama), dapat menggunakan fitur biaya tambahan yang terdapat pada kolom diskusi yang nantinya akan disetujui oleh pengguna.</li>
                    <li>c) Pengguna memahami dan menyetujui bahwa setiap permasalahan yang terjadi pada saat proses pengiriman produk/jasa/paket ke pengguna oleh vendor dan atau penyedia jasa layanan pengiriman Barang adalah bukan merupakan tanggung jawab Weddinc.</li>
                    <li>d) Dalam hal diperlukan untuk dilakukan proses pengembalian produk/paket, maka Pengguna, baik vendor maupun pengguna, diwajibkan untuk melakukan pengiriman barang langsung ke masing-masing pengguna maupun vendor. Weddinc tidak menerima pengembalian atau pengiriman produk/jasa/paket atas transaksi yang dilakukan oleh vendor dan atau pengguna dalam kondisi apapun.</li>
                </ul>
                <br />
                <div>
                <b>7. KETENTUAN PEMBATALAN</b><br />
                <ul style="list-style-type: none;">
                    <li>a) Jika dalam waktu lebih dari 14 hari dari tanggal pemenuhan pesanan dan pengguna mengajukan pembatalan pesanan. Maka tidak akan dikenakan biaya pembatalan. Dan biaya yang telah dibayarkan oleh pengguna akan dikembalikan secara utuh melalui bank transfer.</li>
                    <li>b) Jika dalam waktu 14 hari atau kurang dari tanggal pemenuhan pesanan pengguna mengajukan pembatalan pesanan. Maka akan dikenakan biaya pembatalan sebesar 15% dari total harga pesanan.</li>
                    <li>c) Jika dalam waktu 14 hari atau kurang dari tanggal pemenuhan sejak pengguna membayarkan DP dan pengguna mengajukan pembatalan maka akan dikenakan biaya sebesar pembatalan sebesar 25% dari DP yang telah dibayarkan.</li>
                    <li>d) Jika dalam waktu 14 hari atau kurang dari tanggal pemenuhan pesanan dan telah membayarkan semua termin pembayarannya lalu pengguna mengajukan pembatalan pesanan. Maka akan dikenakan biaya pembatalan sebesar 15 % dari total biaya yang telah dibayarkan.</li>
                    <li>d) Jika dalam pengembalian biaya melalui bank transfer dikenakan biaya administrasi bank termasuk namun tidak terbatas seperti biaya charge antar bank. Maka biayanya akan ditanggung oleh pengguna dengan melakukan pengurangan sesuai nominal charge yang tertera.</li>
                </ul>
                <br />
                <b>8. KETENTUAN LAIN</b><br />
                Segala hal yang belum dan/atau tidak diatur dalam syarat dan ketentuan khusus dalam fitur tersebut maka akan sepenuhnya merujuk pada syarat dan ketentuan Weddinc secara umum.<br />
                <br />
                <b>9. PENOLAKAN JAMINAN DAN BATASAN TANGGUNG JAWAB</b><br />
                Weddinc adalah portal web Wedding Planner &amp; Vendor Commerce, yang menyediakan layanan kepada calon pengantin untuk dapat merencanakan pernikahan dengan mudah dan dapat langsung mencari vendor yang sesuai. Demikian pun pada vendor agar dapat dengan mudah menawarkan layanannya kepada calon pengantin. Dengan demikian transaksi yang terjadi pun adalah transaksi antara para pengguna platform Weddinc (klien-vendor), sehingga para pengguna memahami bahwa batasan tanggung jawab Weddinc secara proporsional adalah sebagai penyedia jasa portal web.<br />
                <br />
                Weddinc selalu berupaya untuk menjaga Layanan Weddinc aman, nyaman, dan berfungsi dengan baik, tapi Weddinc tidak dapat menjamin operasi terus-menerus atau akses ke Layanan Weddinc dapat selalu sempurna. Informasi dan data dalam situs Weddinc memiliki kemungkinan tidak terjadi secara real time.<br />
                <br />
                Pengguna setuju bahwa Anda memanfaatkan Layanan Weddinc atas risiko Pengguna sendiri, dan Layanan Weddinc diberikan kepada Anda pada "SEBAGAIMANA ADANYA" dan "SEBAGAIMANA TERSEDIA".<br />
                <br />
                Sejauh diizinkan oleh hukum yang berlaku, Weddinc (termasuk Induk Perusahaan, direktur, dan karyawan) adalah tidak bertanggung jawab, dan Anda setuju untuk tidak menuntut Weddinc bertanggung jawab, atas segala kerusakan atau kerugian (termasuk namun tidak terbatas pada hilangnya uang, reputasi, keuntungan, atau kerugian tak berwujud lainnya) yang diakibatkan secara langsung atau tidak langsung dari :<br />
                <ul style="list-style-type: none;">
                    <li>i. Penggunaan atau ketidakmampuan Pengguna dalam menggunakan Layanan Weddinc</li>
                    <li>ii. Keterlambatan atau gangguan dalam Layanan Weddinc.</li>
                    <li>iii. Kelalaian dan kerugian yang ditimbulkan oleh masing-masing Pengguna.</li>
                    <li>iv. Kualitas produk/jasa/paket.</li>
                    <li>v. Pengiriman atau pengantaran produk/paket.</li>
                    <li>vi. Pelanggaran Hak atas Kekayaan Intelektual.</li>
                    <li>vii. Perselisihan antar pengguna.</li>
                    <li>viii. Pencemaran nama baik pihak lain.</li>
                    <li>ix.Setiap penyalahgunaan produk/jasa/paket yang sudah dibeli pihak klien.</li>
                    <li>x. Kerugian akibat pembayaran tidak resmi kepada pihak lain selain ke Rekening Resmi Weddinc, yang dengan cara apapun mengatas-namakan Weddinc ataupun kelalaian penulisan rekening dan/atau informasi lainnya dan/atau kelalaian pihak bank.</li>
                    <li>xi. Virus atau perangkat lunak berbahaya lainnya (bot, script, automation tool selain fitur Power Merchant, hacking tool) yang diperoleh dengan mengakses, atau menghubungkan ke layanan Weddinc.</li>
                    <li>xii. Gangguan, bug, kesalahan atau ketidakakuratan apapun dalam Layanan Weddinc.</li>
                    <li>xiii. Kerusakan pada perangkat keras Anda dari penggunaan setiap Layanan Weddinc.</li>
                    <li>xiv. Isi, tindakan, atau tidak adanya tindakan dari pihak ketiga, termasuk terkait dengan Produk yang ada dalam situs Weddinc yang diduga palsu.</li>
                    <li>xv.Tindak penegakan yang diambil sehubungan dengan akun Pengguna.</li>
                    <li>xvi. Adanya tindakan peretasan yang dilakukan oleh pihak ketiga kepada akun pengguna.</li>
                </ul>
                <br />
                <b>10. PELEPASAN</b><br />
                Jika Anda memiliki perselisihan dengan satu atau lebih pengguna, Anda melepaskan Weddinc (termasuk Induk Perusahaan, Direktur, dan karyawan) dari klaim dan tuntutan atas kerusakan dan kerugian (aktual dan tersirat) dari setiap jenis dan sifatnya, yang dikenal dan tidak dikenal, yang timbul dari atau dengan cara apapun berhubungan dengan sengketa tersebut, termasuk namun tidak terbatas pada kerugian yang timbul dari pembelian Barang yang telah dilarang pada. Dengan demikian maka Pengguna dengan sengaja melepaskan segala perlindungan hukum (yang terdapat dalam undang-undang atau peraturan hukum yang lain) yang akan membatasi cakupan ketentuan pelepasan ini.<br />
                <br />
                <b>11. GANTI RUGI</b><br />
                Pengguna akan melepaskan Weddinc dari tuntutan ganti rugi dan menjaga Weddinc (termasuk Induk Perusahaan, direktur, dan karyawan) dari setiap klaim atau tuntutan, termasuk biaya hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul dalam hal Anda melanggar Perjanjian ini, penggunaan Layanan Weddinc yang tidak semestinya dan/ atau pelanggaran Anda terhadap hukum atau hak-hak pihak ketiga.<br />
                <br />
                <b>12. PILIHAN HUKUM</b><br />
                Perjanjian ini akan diatur oleh dan ditafsirkan sesuai dengan hukum Republik Indonesia, tanpa memperhatikan pertentangan aturan hukum. Anda setuju bahwa tindakan hukum apapun atau sengketa yang mungkin timbul dari, berhubungan dengan, atau berada dalam cara apapun berhubungan dengan situs dan/atau Perjanjian ini akan diselesaikan secara eksklusif dalam yurisdiksi pengadilan Republik Indonesia.<br />
                <br />
                <b>13. PEMBAHARUAN</b><br />
                Syarat &amp; ketentuan mungkin diubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. Weddinc menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat &amp; ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Weddinc, maka pengguna dianggap menyetujui perubahan-perubahan dalam Syarat &amp; Ketentuan.</div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection