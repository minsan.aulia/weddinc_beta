@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Rincian Akun</a></li>
                  <li class="list-group-item"><a href="{{ route('notification.user') }}" class="text-darkpink"><i class="fa fa-bell"></i> Notifikasi</a></li>
                  <li class="list-group-item"><a href="{{ route('wishlist.user') }}" class="text-darkpink"><i class="fa fa-heart"></i> Wishlist</a></li>
                  <li class="list-group-item"><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                  <li class="list-group-item"><a href="{{ route('discussion.user') }}" class="text-darkpink"><i class="fa fa-comment"></i> Diskusi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                <div class="container">
                  <div class="messaging">
                        <div class="inbox_msg">
                          <div class="inbox_people">
                            <div class="headind_srch">
                              <div class="recent_heading">
                                <h4>Diskusi</h4>
                              </div>
                              <div class="srch_bar">
                                <div class="stylish-input-group">
                                  <input type="text" class="search-bar"  placeholder="Cari" >
                                  <span class="input-group-addon">
                                  <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                  </span> </div>
                              </div>
                            </div>
                            <div class="inbox_chat">
                              <div class="chat_list active_chat">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>nama vendor <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>nama vendor <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>nama vendor <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>nama vendor <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>nama vendor <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>nama vendor <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="chat_list">
                                <div class="chat_people">
                                  <div class="chat_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                  <div class="chat_ib">
                                    <h5>nama vendor <span class="chat_date">Dec 25</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                      astrology under one roof.</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="mesgs">
                            <div class="msg_history">
                              <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg">
                                    <p>Test which is a new approach to have all
                                      solutions</p>
                                    <span class="time_date"> 11:01 AM    |    June 9</span></div>
                                </div>
                              </div>
                              <div class="outgoing_msg">
                                <div class="sent_msg">
                                  <p>Test which is a new approach to have all
                                    solutions</p>
                                  <span class="time_date"> 11:01 AM    |    June 9</span> </div>
                              </div>
                              <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg">
                                    <p>Test, which is a new approach to have</p>
                                    <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
                                </div>
                              </div>
                              <div class="outgoing_msg">
                                <div class="sent_msg">
                                  <p>Apollo University, Delhi, India Test</p>
                                  <span class="time_date"> 11:01 AM    |    Today</span> </div>
                              </div>
                              <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="max-width: 100%;"> </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg">
                                    <p>We work directly with our designers and suppliers,
                                      and sell direct to you, which means quality, exclusive
                                      products, at a price anyone can afford.</p>
                                    <span class="time_date"> 11:01 AM    |    Today</span></div>
                                </div>
                              </div>
                              <div class="outgoing_msg">
                                <div class="sent_msg">
                                  <p>Apollo University, Delhi, India Test</p>
                                  <span class="time_date"> 11:01 AM    |    Today</span> </div>
                              </div>
                            </div>
                            <div class="type_msg">
                              <div class="input_msg_write row">
                                <input type="text" class="write_msg" placeholder="Tulis pesan" />
                                <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection