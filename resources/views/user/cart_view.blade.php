@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Rincian Akun</a></li>
                  <li class="list-group-item"><a href="{{ route('notification.user') }}" class="text-darkpink"><i class="fa fa-bell"></i> Notifikasi</a></li>
                  <li class="list-group-item"><a href="{{ route('wishlist.user') }}" class="text-darkpink"><i class="fa fa-heart"></i> Wishlist</a></li>
                  <li class="list-group-item"><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                  <li class="list-group-item"><a href="{{ route('discussion.user') }}" class="text-darkpink"><i class="fa fa-comment"></i> Diskusi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-6 text-left">Invoice : 149797979</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-4">
                          <p class="card-text">MAKE UP PLATINUM</p>
                          <img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;">
                          <p class="card-text">Kirim ke :</p>
                          <p><small class="text-muted">Jl. Drupada No.45, Jagakarsa, Jakarta Selatan</small></p>
                        </div>
                        <div class="col-md-8 text-right">
                          <p class="card-text"><small class="text-muted">Vendor : </small><img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" style="width: 25px; height: 25px;"> Trinita Make Up</p>
                          <p class="card-text"><small class="text-muted">Tanggal Order : </small>19 Oktober 2019</p>
                          <p class="card-text"><small class="text-muted">Tanggal Order Disetujui : </small>24 Oktober 2019</p>
                          <p class="card-text"><small class="text-muted">Tanggal Nikah : </small>22 Desember 2019</p>
                          <p class="card-text"><small class="text-muted">Tanggal Pengantaran : </small>20 Desember 2019</p>
                          <p class="card-text"><small class="text-muted">Status :</small></p>
                          <p class="card-text">
                            <button type="submit" class="btn btn-success btn-sm">Setujui</button>
                            <button type="submit" class="btn btn-warning btn-sm">Konfirmasi</button>
                            <button type="submit" class="btn btn-primary btn-sm">Selesai</button>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="card-header border text-left">HARGA PAKET</div>
                    <div class="card-body border boder-bottom-0 border-left-0 border-right-0">
                      <div class="row">
                        <div class="col-md-12">
                          <table class="table borderless">
                            <thead>
                              <tr>
                                <td class="text-right p-2">HARGA PAKET</td>
                                <td class="text-left p-2 font-weight-bold">Rp80.000.000</td>
                              </tr>
                              <tr>
                                <td class="text-right p-2">
                                  <p>BIAYA TAMBAHAN</p>
                                  <p><small>(test make up, ongkos kirim, dll.)</small></p>
                                </td>
                                <td class="text-left p-2 font-weight-bold">Rp850.000</td>
                              </tr>
                              <tr>
                                <td class="text-right p-2">KODE PROMO</td>
                                <td class="text-left p-2 font-weight-bold text-danger">(NIKAHYUK) -Rp45.000.000</td>
                              </tr>
                              <tr>
                                <td class="text-right p-2">TOTAL</td>
                                <td class="text-left p-2 font-weight-bold text-success">Rp50.000.000</td>
                              </tr>
                            </thead>
                          </table>
                        </div>
                      </div>
                      </div>
                    <div class="card-header border text-left">TERMIN PEMBAYARAN</div>
                    <div class="card-body border boder-bottom-0 border-left-0 border-right-0">
                      <div class="row">
                        <div class="col-md-12">
                          <table class="table borderless">
                            <thead>
                              <tr>
                                <td class="text-left p-2">Termin</td>
                                <td class="text-left p-2">Batas Waktu</td>
                                <td class="text-left p-2">Jumlah</td>
                                <td class="text-left p-2">Metode</td>
                                <td class="text-left p-2">Status</td>
                              </tr>
                              <tr>
                                <td class="text-left p-2">Pembayaran ke-1</td>
                                <td class="text-left p-2">26 Oktober 2019</td>
                                <td class="text-left p-2">Rp25.000.000</td>
                                <td class="text-left p-2">Virtual Account BCA</td>
                                <td class="text-left p-2">Selesai</td>
                              </tr>
                              <tr>
                                <td class="text-left p-2">Pembayaran ke-2</td>
                                <td class="text-left p-2">15 Desember 2019</td>
                                <td class="text-left p-2">Rp25.000.000</td>
                                <td class="text-left p-2">Transfer Bank BNI</td>
                                <td class="text-left p-2">Pending</td>
                              </tr>
                            </thead>
                          </table>
                        </div>
                      </div>
                      </div>
                    <div class="card-header border text-left">REVIEW</div>
                    <div class="card-body border boder-bottom-0 border-left-0 border-right-0">
                      <div class="row">
                            <div class="col-md-12 card m-2">
                              <div class="row m-1">
                                <div class="col-md-3">
                                  <div class="col-md-12 text-center">
                                    <img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" width="50" height="50">
                                  </div>
                                  <div class="col-md-12 text-center">
                                    <p><small>Evelyn Christiana</small></p>
                                  </div>
                                </div>
                                <div class="col-md-9 text-left">
                                  <div class="row">
                                    <div class="col-md-12">Rating</div>
                                    <div class="col-md-12">Lorem ipsum dolor sit amet.</div>
                                    <div class="col-md-12 display-10">Date</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 card m-2">
                              <form action="#" method="POST" class="needs-validation" novalidate>
                                  <div class="row text-left">
                                    <div class="col-md-12 p-2">Beri Rating</div>
                                    <div class="col-md-12">
                                      <div class="container">
                                        <div class="starrating risingstar d-flex justify-content-center flex-row-reverse">
                                          <input type="radio" id="star5" name="rate" value="5" /><label for="star5" title="5 star">5</label>
                                          <input type="radio" id="star4" name="rate" value="4" /><label for="star4" title="4 star">4</label>
                                          <input type="radio" id="star3" name="rate" value="3" /><label for="star3" title="3 star">3</label>
                                          <input type="radio" id="star2" name="rate" value="2" /><label for="star2" title="2 star">2</label>
                                          <input type="radio" id="star1" name="rate" value="1" /><label for="star1" title="1 star">1</label>
                                        </div>
                                      </div>  
                                    </div>
                                    <div class="col-md-12 p-2">
                                      <textarea class="form-control" placeholder="balas ulasan"required></textarea>
                                      <div class="valid-feedback"><i><small>valid</small></i></div>
                                      <div class="invalid-feedback"><i><small>masukkan balasan</small></i></div>
                                    </div>
                                    <div class="col-md-12 p-2"><button type="submit" class="btn btn-dustypink btn-sm">Balas</button></div>
                                  </div>
                                </form>
                            </div>
                            <div class="col-md-12 m-2">
                              <div class="col-md-9 card p-3 float-right">
                                <div class="row justify-content-end">
                                  <div class="col-md-12">
                                    <div class="row">
                                      <div class="col-md-12 text-left">
                                        <div class="row">
                                          <div class="col-md-4">
                                            <div class="col-md-12 text-center">
                                              <img src="{{ asset('assets/img/icon/dummy.png') }}" class="rounded-circle" width="50" height="50">
                                            </div>
                                            <div class="col-md-12 text-center">
                                              <p><small>Nama vendor</small></p>
                                            </div>
                                          </div>
                                          <div class="col-md-8">Hi.. terima kasih telah menggunakan layanan kami.</div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 m-2">
                              <div class="col-md-9 card p-3 float-right">
                                <form action="#" method="POST" class="needs-validation" novalidate>
                                  <div class="row text-left">
                                    <div class="col-md-12 p-2">Balas Ulasan</div>
                                    <div class="col-md-12 p-2">
                                      <textarea class="form-control" placeholder="balas ulasan"required></textarea>
                                      <div class="valid-feedback"><i><small>valid</small></i></div>
                                      <div class="invalid-feedback"><i><small>masukkan balasan</small></i></div>
                                    </div>
                                    <div class="col-md-12 p-2"><button type="submit" class="btn btn-dustypink btn-sm">Balas</button></div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection