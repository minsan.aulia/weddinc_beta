@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Rincian Akun</a></li>
                  <li class="list-group-item"><a href="{{ route('notification.user') }}" class="text-darkpink"><i class="fa fa-bell"></i> Notifikasi</a></li>
                  <li class="list-group-item"><a href="{{ route('wishlist.user') }}" class="text-darkpink"><i class="fa fa-heart"></i> Wishlist</a></li>
                  <li class="list-group-item"><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                  <li class="list-group-item"><a href="{{ route('discussion.user') }}" class="text-darkpink"><i class="fa fa-comment"></i> Diskusi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <form action="#" method="POST" class="needs-validation" novalidate>
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-6 text-left">EDIT AKUN</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12 mt-4">
                           <div class="row">
                            <div class="col-md-4 text-right">Nama</div>
                            <div class="col-md-5">
                              <input type="text" name="" class="form-control" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nama</small></i></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12 mt-2">
                          <div class="row">
                            <div class="col-md-4 text-right">Email</div>
                            <div class="col-md-5">
                              <input type="email" name="" class="form-control" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan email</small></i></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12 mt-2">
                          <div class="row">
                            <div class="col-md-4 text-right">Jenis kelamin</div>
                            <div class="col-md-5">
                              <select class="custom-select" required>
                                <option value="0">-Pilih-</option>
                                <option value="1">Laki-laki</option>
                                <option value="2">Perempuan</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan jenis kelamin</small></i></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12 mt-2">
                          <div class="row">
                            <div class="col-md-4 text-right">Telepon</div>
                            <div class="col-md-2">
                              <select class="custom-select" required>
                                <option value="+62">+62</option>
                              </select>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan kode telepon</small></i></div>
                            </div>
                            <div class="col-md-3">
                              <input type="text" class="form-control" required>
                              <div class="valid-feedback"><i><small>valid</small></i></div>
                              <div class="invalid-feedback"><i><small>masukkan nomor telepon</small></i></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12 text-center mt-2 mb-5">
                          <a href="{{ route('account.user') }}"><button type="button" class="btn btn-secondary col-md-2">Kembali</button></a>
                          <button type="submit" class="btn btn-dustypink col-md-2">Simpan</button>
                        </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted"></div>
                  </div>
                </div>
              </div>
              </form>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection