@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Rincian Akun</a></li>
                  <li class="list-group-item"><a href="{{ route('notification.user') }}" class="text-darkpink"><i class="fa fa-bell"></i> Notifikasi</a></li>
                  <li class="list-group-item"><a href="{{ route('wishlist.user') }}" class="text-darkpink"><i class="fa fa-heart"></i> Wishlist</a></li>
                  <li class="list-group-item"><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                  <li class="list-group-item"><a href="{{ route('discussion.user') }}" class="text-darkpink"><i class="fa fa-comment"></i> Diskusi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-6 text-left">DETAIL NOTIFIKASI</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="card col-md-12 text-left p-2 mb-2">
                          <div class="row">
                            <div class="col-md-12"><p><small><i class="fas fa-clock"></i> 13 Oktober 2019</small></p></div>
                            <div class="col-md-12 font-weight-bold mb-2">Hore! ini notifikasi pertama kamu!</div>
                            <div class="col-md-12">ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya. ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya. ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya. ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya. ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya. ini isi dari notifikasi yang singkat dan bisa dilihat lebih lengkp melalui tombol selengkapnya. </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection