@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="https://blog.weddinc.id" class="text-darkpink"><i class="fa fa-blog"></i> Berita & Event</a></li>
                  <li class="list-group-item"><a href="{{ route('vendors.user') }}" class="text-darkpink"><i class="fa fa-home"></i> Vendor</a></li>
                  <li class="list-group-item"><a href="{{ route('venues.user') }}" class="text-darkpink"><i class="fa fa-map-marker"></i> Venue</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-6 text-left">CARI VENUE</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        Coming soon...
                      </div>
                    </div>
                    <div class="card-footer text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection