@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Rincian Akun</a></li>
                  <li class="list-group-item"><a href="{{ route('notification.user') }}" class="text-darkpink"><i class="fa fa-bell"></i> Notifikasi</a></li>
                  <li class="list-group-item"><a href="{{ route('wishlist.user') }}" class="text-darkpink"><i class="fa fa-heart"></i> Wishlist</a></li>
                  <li class="list-group-item"><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                  <li class="list-group-item"><a href="{{ route('discussion.user') }}" class="text-darkpink"><i class="fa fa-comment"></i> Diskusi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-6 text-left">RINCIAN AKUN</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/feature/wp_order.jpg') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row mt-4">
                        <div class="col-md-4">
                        <?php if(!empty(auth()->user()->photo)){ ?>
                          <div class="col-md-12 text-center">
                             <img src="{{ auth()->user()->photo }}" class="rounded-circle" width="250" height="250">
                          </div>
                        <?php } else{ ?>
                          <div class="col-md-12 text-center">
                             <img src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle" width="250" height="250">
                          </div>
                        <?php } ?>
                          <!-- <div class="col-md-8 offset-md-2 mt-2">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="customFile">
                              <label class="custom-file-label" for="customFile">Ganti</label>
                            </div>
                          </div> -->
                        </div>
                        <div class="col-md-6 offset-md-2">
                          <div class="col-md-12 text-right"><a href="{{ route('profile') }}" class="text-darkpink"><i class="fa fa-pencil text-darkpink" aria-hidden="true"></i> Edit</div></a>
                          <table class="table table-hover">
                            <tr>
                              <td class="text-right p-3" width="30%">Nama :</td>
                              <td colspan="2" class="p-3">{{ auth()->user()->name }}</td>
                            </tr>
                            <tr>
                              <td class="text-right p-3" width="30%">Email :</td>
                              <td colspan="2" class="p-3">{{ auth()->user()->email }}</td>
                            </tr>
                            <tr>
                              <td class="text-right p-3" width="30%">Jenis kelamin :</td>
                              <td colspan="2" class="p-3">
                                <select class="custom-select" disabled>
                                  <option value="0">-Pilih-</option>
                                  <option value="1">Laki-laki</option>
                                  <option value="2">Perempuan</option>
                                </select>
                              </td>
                            </tr>
                            <tr>
                              <td class="text-right p-3" width="30%">Telepon :</td>
                              <td class="p-3">
                                <select class="custom-select" disabled>
                                  <option value="+62">+62</option>
                                </select>
                              </td>
                              <td class="p-3">
                                <input type="text" class="form-control" disabled>
                              </td>
                            </tr>
                            <tr>
                              <td class="text-right p-3" width="30%">Kata Sandi :</td>
                              <td colspan="2" class="p-3"><a href="{{ route('reset_password.user') }}"><button type="button" class="btn btn-secondary">Ubah Kata sandi</button></a></td>
                            </tr>
                            <!-- <tr>
                              <td class="text-right" width="30%"></td>
                              <td colspan="2">Sambungkan ke Facebook<br><button type="button" class="btn btn-fb">Terhubung ke Facebook</button></td>
                            </tr> -->
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection