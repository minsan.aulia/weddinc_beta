@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')

    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Rincian Akun</a></li>
                  <li class="list-group-item"><a href="{{ route('notification.user') }}" class="text-darkpink"><i class="fa fa-bell"></i> Notifikasi</a></li>
                  <li class="list-group-item"><a href="{{ route('wishlist.user') }}" class="text-darkpink"><i class="fa fa-heart"></i> Wishlist</a></li>
                  <li class="list-group-item"><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                  <li class="list-group-item"><a href="{{ route('discussion.user') }}" class="text-darkpink"><i class="fa fa-comment"></i> Diskusi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-6 text-left">WISHLIST</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">

                      @if (count($wishlists)==0)
                        <h5 class="text-secondary">There is nothing on the wishlist</h5>
                      @endif

                      <div class="row">
                        <table class="table">
                          <thead>
                            @foreach ($wishlists as $wishlist)

                            <?php
                              $name = $wishlist->package->name;
                              $desc = $wishlist->package->description;
                              $vendor_name = $wishlist->package->vendor->name;
                              $vendor_category = $wishlist->package->vendor->category;
                              $price = "Rp " . number_format($wishlist->package->price, 2);
                            ?>
                            
                            <input type="hidden" id="pkgName_{{$wishlist->id}}" value="{{$name}}">
                            <input type="hidden" id="pkgDesc_{{$wishlist->id}}" value="{{$desc}}">
                            <input type="hidden" id="pkgPrice_{{$wishlist->id}}" value="{{$price}}">

                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>
                                <img src="{{ ($wishlist->package->photo)? asset('uploads/packages/'.$wishlist->package->photo) : asset('assets/img/icon/dummy.png') }}" style="width: 150px; height: auto;">
                              </td>
                              <td><p>{{ $name }}</p><p><small>{{ $vendor_name }}</small></p></td>
                              <td>{{ $vendor_category }}</td>
                              <td>{{ $price }}</td>
                              <td>
                                <a href="{{ route('discussion.user') }}"><button type="button" class="btn btn-dustypink btn-sm border">Chat Vendor</button></a>
                                <a href="javascript:;" id="delete_wishlist" data-toggle="modal" onclick="deleteData({{$wishlist->id}})" class="btn btn-danger btn-sm border" data-target="#delete_cart"><i class="fa fa-trash" aria-hidden="true"></i></a>
                              </td>
                            </tr>
                            @endforeach
                          </thead>
                        </table>
                      </div>
                    </div>
                    <div class="card-footer text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </section>
      <div class="container">
        <div class="modal fade" id="delete_cart">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <form action="" id="deleteForm">
                <div class="modal-header text-center display-4">
                  Hapus dari wishlist
                </div>
                <div class="modal-body">
                  <div class="row">
                    Yakin menghapus paket ini?
                    <table class="table table-bordered table-hover mt-3">
                      <thead>
                        <tr>
                          <td>Nama paket</td>
                          <td id="pkg_name">Paket ini</td>
                        </tr>
                        <tr>
                          <td>Keterangan</td>
                          <td id="pkg_desc">keterangan dari paket</td>
                        </tr>
                        <tr>
                          <td>Harga</td>
                          <td id="pkg_price">harga paketnya</td>
                        </tr>
                      </thead>
                    </table>
                    
                      {{ csrf_field() }}
                    
                  </div>
                </div>
                <div class="modal-footer">
                  <a href="">
                    <button type="submit" onclick="formSubmit()" class="btn btn-danger btn-sm">Hapus</button>
                  </a>
                  <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
                </div>
              </form>

            </div>
          </div>
        </div>
        <form action="#" method="POST" class="needs-validation" novalidate>
        <div class="modal fade" id="buy_package">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header text-center display-4">
                Order PAKET
              </div>
              
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-5">
                    <div class="row">
                      <div class="col-5"><img src="{{ asset('assets/img/icon/dummy.png') }}" style="width: 100%; height: auto;"></div>
                      <div class="col-7">Trinity Make Up Utama</div>
                      <div class="col-md-12 text-center font-weight-bold m-3">PREMIUM VITUKO LOTI</div>
                      <div class="col-md-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat ornare porta. Sed sit amet ipsum sed massa fringilla congue. Donec a nibh dui. Duis ac ex metus. Etiam rutrum convallis lorem, eu dictum tellus faucibus non. Donec sagittis erat lacus, vel fermentum dolor pellentesque vel.</div>
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="row">
                      <div class="col-6 text-right p-2">
                        Nama klien yang memesan : 
                      </div>
                      <div class="col-6 p-2">
                        <img src="{{ asset('assets/img/icon/dummy_user.png') }}" style="width: 30px; height: auto;"> Anastasia Herawati
                      </div>
                      <div class="col-6 text-right p-2">
                        Tanggal pernikahan : 
                      </div>
                      <div class="col-6 p-2">
                        <input type="date" class="form-control" name="" required>
                      </div>
                      <div class="col-6 text-right p-2">
                        Tanggal pengantaran : 
                      </div>
                      <div class="col-6 p-2">
                        <input type="date" class="form-control" name="">
                        <div class="custom-control custom-checkbox mb-3 mt-1">
                          <input type="checkbox" class="custom-control-input" id="customCheck1">
                          <label class="custom-control-label" for="customCheck1"><small>Sama dengan tanggal pernikahan</small></label>
                        </div>
                      </div>
                      <div class="col-6 text-right p-2">
                        Termin Pembayaran : 
                      </div>
                      <div class="col-6 p-2">
                        <select class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih termin" data-hide-disabled="true" required>
                          <option value="1">1 kali bayar</option>
                          <option value="2">2 kali bayar</option>
                        </select>
                      </div>
                      <div class="col-6 text-right p-2">
                        Harga paket : 
                      </div>
                      <div class="col-6 p-2">
                        <p class="font-weight-bold">Rp23.000.000</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success btn-sm">Pilih pembayaran</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </div>
        </form>
      </div>

<script>

function deleteData(id)
{
    var id = id;
    var url = '{{ route("wishlist.destroy", ":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    var input_name = $('#pkgName_'+id).val();
    var input_desc = $('#pkgDesc_'+id).val();
    var input_price = $('#pkgPrice_'+id).val();
    $('#pkg_name').text(input_name);
    $('#pkg_desc').text(input_desc);
    $('#pkg_price').text(input_price);
}

function formSubmit()
{
    $("#deleteForm").submit();
}
</script>

@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection
