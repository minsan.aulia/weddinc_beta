@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Rincian Akun</a></li>
                  <li class="list-group-item"><a href="{{ route('notification.user') }}" class="text-darkpink"><i class="fa fa-bell"></i> Notifikasi</a></li>
                  <li class="list-group-item"><a href="{{ route('wishlist.user') }}" class="text-darkpink"><i class="fa fa-heart"></i> Wishlist</a></li>
                  <li class="list-group-item"><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                  <li class="list-group-item"><a href="{{ route('discussion.user') }}" class="text-darkpink"><i class="fa fa-comment"></i> Diskusi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-7">
              <form action="#" method="POST" class="needs-validation" novalidate>
              <div class="row">
                <div class="col-md-12 mt-4">
                   <div class="row">
                    <div class="col-md-4 text-right">Password lama</div>
                    <div class="col-md-4">
                      <input type="text" name="" class="form-control" required>
                        <div class="valid-feedback"><i><small>valid</small></i></div>
                        <div class="invalid-feedback"><i><small>masukkan password lama</small></i></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 mt-2">
                  <div class="row">
                    <div class="col-md-4 text-right">Password baru</div>
                    <div class="col-md-4">
                      <input type="text" name="" class="form-control" required>
                        <div class="valid-feedback"><i><small>valid</small></i></div>
                        <div class="invalid-feedback"><i><small>masukkan password baru</small></i></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 mt-2">
                  <div class="row">
                    <div class="col-md-4 text-right">Ulangi password baru</div>
                    <div class="col-md-4">
                      <input type="text" name="" class="form-control" required>
                        <div class="valid-feedback"><i><small>valid</small></i></div>
                        <div class="invalid-feedback"><i><small>ulangi password baru</small></i></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 text-center mt-2 mb-5">
                  <a href="{{ route('account.user') }}"><button type="button" class="btn btn-secondary col-md-2">Kembali</button></a>
                  <button type="submit" class="btn btn-dustypink col-md-2">Simpan</button>
                </div>
              </div>
              </form>
            </div>
        </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection