@extends('layouts.user_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12 justify-content-center mb-4">
            <div class="col-md-3">
              <div class="border">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="https://blog.weddinc.id" class="text-darkpink"><i class="fa fa-blog"></i> Berita & Event</a></li>
                  <li class="list-group-item"><a href="{{ route('vendors.user') }}" class="text-darkpink"><i class="fa fa-home"></i> Vendor</a></li>
                  <li class="list-group-item"><a href="{{ route('venues.user') }}" class="text-darkpink"><i class="fa fa-map-marker"></i> Venue</a></li>
                </ul>
              </div>
            </div>

            <div class="col-md-7">
              <div class="row">
                <div class="col-md-12 mb-4">
                  <div class="card text-center">
                    <div class="card-header">
                      <div class="row">
                        <div class="col-6 text-left">CARI VENDOR</div>
                        <div class="col-6 text-right"><img src="{{ asset('assets/img/icon/notification.png') }}" style="width: 25px; height: 25px;"></div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12 border border-top-0 border-left-0 border-right-0 sticky-top bg-white p-2">
                          <form action="{{ route('vendors.user') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="row col-md-12">
                              <div class="col-4 text-center">
                                <select name="category" class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih Kategori" data-hide-disabled="true">
                                  @foreach($categories as $category)
                                  <option value="{{ $category->id }}" {{ ($current_category==$category->id) ? 'selected' : '' }}>{{ $category->name }}</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="col-3 text-center">
                                <select name="city" class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih Venue" data-hide-disabled="true">
                                  @foreach($cities as $city)
                                  <option value="{{ $city->id }}" {{ ($current_city==$city->id) ? 'selected' : '' }}>{{ $city->name }}</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="col-4 text-center">
                                <select name="price" class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih Rentang Harga" data-hide-disabled="true">
                                  <option value="<1" {{ ($current_price=="0-1") ? 'selected' : '' }}>< Rp 1.000.000</option>
                                  <option value="1-2" {{ ($current_price=="1-2") ? 'selected' : '' }}>Rp 1.000.000 - Rp 2.000.000</option>
                                  <option value="2-3" {{ ($current_price=="2-3") ? 'selected' : '' }}>Rp 2.000.000 - Rp 3.000.000</option>
                                  <option value="3-4" {{ ($current_price=="3-4") ? 'selected' : '' }}>Rp 3.000.000 - Rp 4.000.000</option>
                                  <option value="4-5" {{ ($current_price=="4-5") ? 'selected' : '' }}>Rp 4.000.000 - Rp 5.000.000</option>
                                  <option value=">5" {{ ($current_price==">5") ? 'selected' : '' }}>> Rp 5.000.000</option>
                                </select>
                              </div>
                              <div class="col-1 text-center">
                                <button type="submit" class="btn btn-dustypink">Go</button>
                              </div>
                            </div>
                          </form>
                        </div>

                        @if(count($packages)==0)
                          <div class="col-12 mt-4 text-center">
                            <h4 class="text-secondary">No Data Found</h4>
                          </div>
                        @endif
                        
                        @foreach ($packages as $package)
                        <div class="col-md-4 p-3">
                          <div class="card h-100">
                            <img class="card-img-top" src="{{ ($package->photo)? asset('uploads/packages/'.$package->photo) : asset('assets/img/icon/dummy.png') }}" alt="Card image">
                            <div class="card-body">
                              <h4 class="card-title">{{ $package->name }}</h4>
                              <p class="card-text">
                                <img src="{{ ($package->profile_vendor->photo)? asset('uploads/photoProfile/'.$package->profile_vendor->photo) : asset('assets/img/icon/dummy.png') }}" style="width: 30px; height: auto;" class="rounded-circle"> {{ $package->vendor->name }}
                              </p>
                            </div>
                            <div class="card-footer text-muted">
                              <div class="row">
                                <div class="col-6"><a href="{{route('wishlist.store', $package->id)}}">
                                  <button type="button" class="btn {{(WishlistHelp::isPackageExists($package->id))? 'btn-danger' : 'btn-warning'}} btn-sm btn-block text-white">Wishlist</button>
                                </a></div>
                                <div class="col-6"><a href="{{ route('discussion.user') }}"><button type="button" class="btn btn-success btn-sm btn-block text-white">Chat</button></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        @endforeach

                      </div>
                    </div>
                    <div class="card-footer text-muted"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection