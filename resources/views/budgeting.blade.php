@extends('layouts.homepage2_layout')

@section('navigation')
    @parent
@endsection

@section('content')
<div class="budgetting">
                        <div class="budgetting-tab" id="budgetting1">
                          <form class="needs-validation" novalidate>
                            <div class="form-row row">
                              <div class="input-group col-md-12 mt-2">
                                <div class="row col-md-12">
                                  <div class="input-group-prepend col-md-5">
                                    <div class="col-md-12 text-right">Jumlah Anggaran</div>
                                  </div>
                                  <div class="col-md-4">
                                    <input type="text" class="money form-control" id="budget" placeholder="Masukkan jumlah anggaran" required>
                                    <div class="valid-feedback"><i><small>valid</small></i></div>
                                    <div class="invalid-feedback"><i><small>masukkan jumlah anggaran</small></i></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="form-row row">
                              <div class="input-group col-md-12 mt-2">
                                <div class="row col-md-12">
                                  <div class="input-group-prepend col-md-5">
                                    <div class="col-md-12 text-right">Jumlah Tamu</div>
                                  </div>
                                  <div class="col-md-4">
                                    <input type="text" class="money form-control" id="guest" placeholder="Masukkan jumlah tamu" required>
                                    <div class="valid-feedback"><i><small>valid</small></i></div>
                                    <div class="invalid-feedback"><i><small>masukkan jumlah tamu</small></i></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="form-row row">
                              <div class="col-md-12 mt-2">
                                <div class="row justify-content-center">
                                  <a href="#budgetting2"><button type="button" class="btn btn-dustypink" onclick="calculate_weddingbudget()">Selanjutnya</button></a>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                        <div class="budgetting-tab" id="budgetting2" style="display: none;">
                          <div class="row">
                            <table class="table table-stripped mt-4">
                              <thead>
                                <tr class="bg-dustypink text-white text-center font-weight-bold">
                                  <td width="30%">Kategori</td>
                                  <td width="70%">Estimasi</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Venue</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Tempat akad/resepsi</td>
                                        <td width="35%"><p id="resepsi">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Acara pernikahan</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Prosesi</td>
                                        <td width="35%"><p id="prosesi">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Catering</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Menu utama</td>
                                        <td width="35%"><p id="menu_utama">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Pondokan</td>
                                        <td width="35%"><p id="pondokan">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Wedding cake</td>
                                        <td width="35%"><p id="wedding_cake">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Dekorasi</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Dekorasi</td>
                                        <td width="35%"><p id="dekorasi">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Mempelai wanita</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Busana</td>
                                        <td width="35%"><p id="busana_wanita">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Rias</td>
                                        <td width="35%"><p id="rias">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Mempelai pria</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Busana</td>
                                        <td width="35%"><p id="busana_pria">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Hiburan</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">MC & Live music</td>
                                        <td width="35%"><p id="mc">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Dokumentasi</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Dokumentasi pre wedding</td>
                                        <td width="35%"><p id="dok_prewed">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Dokumentasi foto</td>
                                        <td width="35%"><p id="dok_foto">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Dokumentasi video</td>
                                        <td width="35%"><p id="dok_video">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Transportasi</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Mobil pengantin</td>
                                        <td width="35%"><p id="mobil">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Lainnya</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Mahar</td>
                                        <td width="35%"><p id="mahar">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Undangan</td>
                                        <td width="35%"><p id="undangan">Rp.0</p><p id="undangan_pcs" class="display-10 font-weight-bold">(Rp.0/undangan)</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%"></td>
                                        <td width="35%"><p id="souvenir">Rp.0</p><p id="souvenir_pcs" class="display-10 font-weight-bold">(Rp.0/undangan)</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Biaya tak terduga</td>
                                        <td width="35%"><p id="biaya_takterduga">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <div class="col-md-12 text-center">
                              <a href="#budgetting1"><button type="button" class="btn btn-dustypink col-md-2">Kembali</button></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection