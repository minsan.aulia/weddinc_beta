@extends('layouts.homepage_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="site-blocks-cover overlay" data-aos="fade" data-stellar-background-ratio="0.5">
      <div id="demo" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="{{ asset('assets/img/highlights/home/1.jpg') }}" alt="Weddinc.id" width="100%">
          </div>
          <!-- <div class="carousel-item">
            <img src="assets/img/highlights/home/2.jpg" alt="Weddinc.id" width="100%">
          </div> -->
          <div class="carousel-item">
            <img src="{{ asset('assets/img/highlights/home/3.jpg') }}" alt="Weddinc.id" width="100%">
          </div>
        </div>
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>  
    </div>
    <section class="section ft-feature-1">
      <div class="container shadow">
        <div class="row align-items-stretch">
          <div class="col-12 card text-dark ft-feature-1-content opacity-5">
            <div class="container">
                <div class="row col-md-8 col-md-offset-4 mb-4">
                    <h2 class="text-dark">Mulai rencana pernikahanmu hari ini!</h2>
                </div>
                <div class="row">
                <div class="col-12 rounded">
                  <div class="tabshome">
                    <input type="radio" name="tabshome" id="tabshome1" checked >
                    <label for="tabshome1">
                      <img src="{{ asset('assets/img/icon/feature/budgeting.png') }}" style="width: 80px; height: 80px;"><span>Kalkulator</span>
                    </label>
                    <input type="radio" name="tabshome" id="tabshome2">
                    <label for="tabshome2">
                      <img src="{{ asset('assets/img/icon/feature/find_vendor.png') }}" style="width: 80px; height: 80px;"><span>Temukan Vendor</span>
                    </label>
                    <input type="radio" name="tabshome" id="tabshome3">
                    <label for="tabshome3">
                      <img src="{{ asset('assets/img/icon/feature/blog.png') }}" style="width: 80px; height: 80px;"><span>Blog</span>
                    </label>
                    <div id="tabshome-content1" class="tabshome-content">
                      <!--<div class="planner1" style="overflow: hidden;">
                        <div class="row">
                          <div class="col-md-8 offset-md-2">
                            <div class="row">
                              <div class="col-12 text-center mt-3 mb-3 display-4 font-italic">Venue</div>
                              <div class="col-12">
                                <div class="form-group row">
                                <div class="col-md-12">
                                  <select class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih venue" data-hide-disabled="true">
                                    <option value="1">Jakarta Pusat</option>
                                    <option value="2">Jakarta Barat</option>
                                  </select>
                                </div>
                                </div>
                              </div>
                              <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="customCheck1">
                                  <label class="custom-control-label" for="customCheck1">Sudah memiliki venue pernikahan sendiri.</label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-8 offset-md-2">
                            <div class="row">
                              <div class="col-12 text-center mt-3 mb-3 display-4 font-italic">Tanggal & Waktu</div>
                              <div class="col-12 form-group">
                                <input type="date" name="date" class="form-control">
                              </div>
                              <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="customCheck2">
                                  <label class="custom-control-label" for="customCheck2">Belum merencanakan.</label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 text-center">
                            <button type="button" class="btn btn-dustypink slide-toggle" id="btn-doboth">Selanjutnya</button>
                          </div>
                        </div>
                      </div>
                      <div class="planner2" style="display: none; overflow: hidden;">
                        <div class="row">
                          <div class="col-md-8 offset-md-2">
                            <p class="font-italic">Bantu kami memberi Anda rekomendasi vendor & konten yang lebih baik dengan memberi tahu kami sedikit tentang Anda dan rencana pernikahan Anda!</p>
                          </div>
                          <div class="col-md-8 offset-md-2">
                            <table class="table">
                              <tr>
                                <td width="50%">Saya adalah </td>
                                <td width="50%">
                                  <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="customRadio" name="example" value="customEx">
                                    <label class="custom-control-label" for="customRadio">Pengantin Wanita</label>
                                  </div>
                                  <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="customRadio2" name="example" value="customEx">
                                    <label class="custom-control-label" for="customRadio2">Pengantin Pria</label>
                                  </div> 
                                </td>
                              </tr>
                              <tr>
                                <td width="100%" colspan="2" class="text-dark"><div class="col-12 text-center mt-3 mb-3 display-4 font-italic">Nama saya</div></td>
                              </tr>
                              <tr>
                                <td width="50%"><input type="text" name="" class="form-control" placeholder="nama depan"></td>
                                <td width="50%"><input type="text" name="" class="form-control" placeholder="nama belakang"></td>
                              </tr>
                              <tr>
                                <td width="100%" colspan="2" class="text-dark"><div class="col-12 text-center mt-3 mb-3 display-4 font-italic">Nama pasangan saya</div></td>
                              </tr>
                              <tr>
                                <td width="50%"><input type="text" name="" class="form-control" placeholder="nama depan"></td>
                                <td width="50%"><input type="text" name="" class="form-control" placeholder="nama belakang"></td>
                              </tr>
                              <tr>
                                <td width="50%" class="text-center">
                                  <div class="row">
                                    <div class="col-md-12 display-8 font-italic mt-4">Tema pernikahan</div>
                                    <div class="col-md-12">
                                      <select name="" class="custom-select">
                                        <option value="">Keluarga pengantin pria</option>
                                        <option value="">Teman pengantin pria</option>
                                        <option value="">Rekan kerja pengantin pria</option> 
                                        <option value="">Keluarga pengantin wanita</option>
                                        <option value="">Teman pengantin wanita</option>
                                        <option value="">Rekan kerja pengantin wanita</option> 
                                      </select>
                                  </div>
                                  </div>
                                </td>
                                <td width="50%" class="text-center">
                                  <div class="row">
                                    <div class="col-md-12 display-8 font-italic mt-4">Tamu undangan</div>
                                    <div class="col-md-12"><input type="number" name="" class="form-control"></div>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </div>
                          <div class="col-md-8 offset-md-2">
                            <p class="font-italic">Jangan khawatir, Anda selalu dapat memperbarui informasi ini!</p>
                          </div>
                          <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-fb col-md-2 slide-toggle">Kembali</button>
                            <a href="coming_soon.html"><button type="button" class="btn btn-dustypink col-md-2">Rencanakan!</button></a>
                          </div>
                        </div>
                      </div>-->
                      <div class="">
                        <div class="" id="">
                          <form action="{{ route('login') }}" method="POST" class="needs-validation" novalidate>
                            <div class="form-row row">
                              <div class="input-group col-md-12 mt-2">
                                <div class="row col-md-12">
                                  <div class="input-group-prepend col-md-5">
                                    <div class="col-md-12 text-right">Jumlah Anggaran</div>
                                  </div>
                                  <div class="col-md-4">
                                    <input type="text" class="money form-control" id="budget" placeholder="Masukkan jumlah anggaran" required>
                                    <div class="valid-feedback"><i><small>valid</small></i></div>
                                    <div class="invalid-feedback"><i><small>masukkan jumlah anggaran</small></i></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="form-row row">
                              <div class="input-group col-md-12 mt-2">
                                <div class="row col-md-12">
                                  <div class="input-group-prepend col-md-5">
                                    <div class="col-md-12 text-right">Jumlah Tamu</div>
                                  </div>
                                  <div class="col-md-4">
                                    <input type="text" class="money form-control" id="guest" placeholder="Masukkan jumlah tamu" required>
                                    <div class="valid-feedback"><i><small>valid</small></i></div>
                                    <div class="invalid-feedback"><i><small>masukkan jumlah tamu</small></i></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                            <div class="form-row row">
                              <div class="col-md-12 mt-2">
                                <div class="row justify-content-center">
                                  <a href="{{ route('login') }}"><button type="button" class="btn btn-dustypink mt-2">Selanjutnya</button></a>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="" id="" style="display: none;">
                          <div class="row">
                            <table class="table table-stripped mt-4">
                              <thead>
                                <tr class="bg-dustypink text-white text-center font-weight-bold">
                                  <td width="30%">Kategori</td>
                                  <td width="70%">Estimasi</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Venue</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Tempat akad/resepsi</td>
                                        <td width="35%"><p id="resepsi">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Acara pernikahan</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Prosesi</td>
                                        <td width="35%"><p id="prosesi">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Catering</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Menu utama</td>
                                        <td width="35%"><p id="menu_utama">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Pondokan</td>
                                        <td width="35%"><p id="pondokan">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Wedding cake</td>
                                        <td width="35%"><p id="wedding_cake">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Dekorasi</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Dekorasi</td>
                                        <td width="35%"><p id="dekorasi">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Mempelai wanita</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Busana</td>
                                        <td width="35%"><p id="busana_wanita">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Rias</td>
                                        <td width="35%"><p id="rias">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Mempelai pria</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Busana</td>
                                        <td width="35%"><p id="busana_pria">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Hiburan</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">MC & Live music</td>
                                        <td width="35%"><p id="mc">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Dokumentasi</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Dokumentasi pre wedding</td>
                                        <td width="35%"><p id="dok_prewed">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Dokumentasi foto</td>
                                        <td width="35%"><p id="dok_foto">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Dokumentasi video</td>
                                        <td width="35%"><p id="dok_video">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Transportasi</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Mobil pengantin</td>
                                        <td width="35%"><p id="mobil">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Lainnya</td>
                                  <td>
                                    <table class="table">
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Mahar</td>
                                        <td width="35%"><p id="mahar">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Undangan</td>
                                        <td width="35%"><p id="undangan">Rp.0</p><p id="undangan_pcs" class="display-10 font-weight-bold">(Rp.0/undangan)</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%"></td>
                                        <td width="35%"><p id="souvenir">Rp.0</p><p id="souvenir_pcs" class="display-10 font-weight-bold">(Rp.0/undangan)</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="5%">&bull;</td>
                                        <td width="40%">Biaya tak terduga</td>
                                        <td width="35%"><p id="biaya_takterduga">Rp.0</p></td>
                                        <td class="text-center">
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-pencil-alt text-secondary" aria-hidden="true"></i></button></a>
                                          <a href="#"><button type="button" class="btn btn-light btn-sm border border-secondary"><i class="fa fa-trash text-secondary" aria-hidden="true"></i></button></a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <div class="col-md-12 text-center">
                              <a href="#budgetting1"><button type="button" class="btn btn-dustypink col-md-2">Kembali</button></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="tabshome-content2" class="tabshome-content">
                      <div class="row">
                        <div class="col-sm">
                          <p class="font-italic display-8">Kategori</p>
                          <select class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih Kategori" data-hide-disabled="true">
                            <option value="1">Katering</option>
                            <option value="2">Make Up</option>
                            <option value="1">Makanan</option>
                            <option value="2">Foto</option>
                          </select>
                        </div>
                        <div class="col-sm">
                          <p class="font-italic display-8">Venue</p>
                          <select class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih Venue" data-hide-disabled="true">
                            <option value="1">Indoor</option>
                            <option value="2">Outdoor</option>
                          </select>
                        </div>
                        <div class="col-sm">
                          <p class="font-italic display-8">Rentang Harga</p>
                          <select class="selectpicker show-tick form-control" data-width="100%" data-container="body" data-live-search="true" title="Pilih Rentang Harga" data-hide-disabled="true">
                            <option value="1">< Rp 50.000.000</option>
                            <option value="2">Rp 50.000.001 - Rp 100.000.000</option>
                          </select>
                        </div>
                        <div class="col-sm text-center">
                          <br>
                          <a href="../user/venues"><button type="button" class="btn btn-dustypink">Go</button></a>
                        </div>
                      </div>
                    </div>
                    <div id="tabshome-content3" class="tabshome-content">
                      <div class="row col-md-12 no-gutter p-0 m-0">
                      <?php
                        function weddinc_strip_all_tags( $string, $remove_breaks = false ) {
                          $string = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', $string );
                          $string = strip_tags( $string );
                          if ( $remove_breaks ) {$string = preg_replace( '/[\r\n\t ]+/', ' ', $string );}
                          return trim( $string );}

                        function weddinc_trim_words( $text, $num_words = 55, $more = null ) {
                          if ( null === $more ) {$more = __( '&hellip;' );}
                          $original_text = $text;
                          $text          = weddinc_strip_all_tags( $text );
                          if (preg_match( '/^utf\-?8$/i', "UTF-8" ) ) {
                            $text = trim( preg_replace( "/[\n\r\t ]+/", ' ', $text ), ' ' );
                            preg_match_all( '/./u', $text, $words_array );
                            $words_array = array_slice( $words_array[0], 0, $num_words + 1 );
                            $sep         = '';}
                          else {
                            $words_array = preg_split( "/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY );
                            $sep         = ' ';}

                          if ( count( $words_array ) > $num_words ) {
                            array_pop( $words_array );
                            $text = implode( $sep, $words_array );
                            $text = $text . $more;}
                          else {
                            $text = implode( $sep, $words_array );}
                          return $text;}

                        $fullHTML     = '';
                        $fullContent  = '';
                        $weddincRss   = new DOMDocument();
                        $pattern    = '/<img\s*(?:class\s*\=\s*[\'\"](.*?)[\'\"].*?\s*|src\s*\=\s*[\'\"](.*?)[\'\"].*?\s*|alt\s*\=\s*[\'\"](.*?)[\'\"].*?\s*|width\s*\=\s*[\'\"](.*?)[\'\"].*?\s*|height\s*\=\s*[\'\"](.*?)[\'\"].*?\s*)+.*?>/si';
                        $weddincFeed  = array();
                        $weddincRss->load('https://blog.weddinc.id/feed/');

                        foreach ($weddincRss->getElementsByTagName('item') as $node) {
                          $item = array ( 
                            'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                            'desc'  => $node->getElementsByTagName('description')->item(0)->nodeValue,
                            'link'  => $node->getElementsByTagName('link')->item(0)->nodeValue,
                            'date'   => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
                            );
                          array_push($weddincFeed, $item);}
                        
                        for($x=0;$x<5;$x++) {
                          $title = str_replace(' & ', ' &amp; ', $weddincFeed[$x]['title']);
                          $link = $weddincFeed[$x]['link'];
                          $post_date = new DateTime($weddincFeed[$x]['date']);
                          $date = date_format($post_date,"D, d M Y");
                          $description = $weddincFeed[$x]['desc'];
                          preg_match_all($pattern, $description, $matches);
                          if(!empty($matches) && isset($matches[2][0])){
                            $description_extract = preg_replace("/<img[^>]+\>/i", "", $description);
                            $description_extract = weddinc_trim_words($description_extract, 100, "...");
                            $fullContent .= '<div class="col-md-12 pb-1 mb-2 border border-top-0 border-left-0 border-right-0"><div class="row">';
                              $fullContent .= '<div class="col-6"><a href="'.$link.'"><img src="'.$matches[2][0].'" class="img-fluid blog-mobile-image"></a></div>';
                              $fullContent .= '<div class="col-6 blog-mobile-title"><p cla><a class="text-dark font-weight-bold" style="text-decoration: none;" href="'.$link.'">'.$title.'</a></p><p class="blog-mobile">'.strip_tags($description_extract).'</p><p class="blog-mobile"><i class="fas fa-star"></i> '.$date.'</p></div>';
                            $fullContent .= '</div></div>';
                          }
                        }
                        $fullHTML .= '';
                          $fullHTML .= $fullContent;
                        $fullHTML .= '';
                          echo $fullHTML;
                      ?>
                      </div>
                      <div class="col-md-12 mt-5 text-center">
                        <a href="https://blog.weddinc.id"><button type="button" class="btn btn-dustypink col-md-2">Baca Selengkapnya</button></a>
                      </div>
                    </div>
                </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="site-section bg-light">
      <div class="container text-center">
        <h2>Apa itu Weddinc?</h2>
        <div class="row">
          <div class="row col-md-4">
            <div class="col-md-6 offset-md-3 mt-2">
              <img src="{{ asset('assets/img/home/wedding.png') }}" class="img-fluid">
            </div>
            <div class="col-md-12 mt-2">
              <h4>Inspirasi & Ide Pernikahan</h4>
              <p>Dapatkan berbagai ide menarik agar pernikahanmu menjadi tidak terlupakan.</p>
            </div>
          </div>
          <div class="row col-md-4">
            <div class="col-md-6 offset-md-3 mt-2">
              <img src="{{ asset('assets/img/home/vendor.png') }}" class="img-fluid">
            </div>
            <div class="col-md-12 mt-2">
              <h4>Banyak Pilihan Vendor</h4>
              <p>Temukan berbagai vendor berpengalaman dan terpercaya, membuatmu tenang menuju Hari spesialmu.</p>
            </div>
          </div>
          <div class="row col-md-4">
            <div class="col-md-6 offset-md-3 mt-2">
              <img src="{{ asset('assets/img/home/security.png') }}" class="img-fluid">
            </div>
            <div class="col-md-12 mt-2">
              <h4>Keamanan</h4>
              <p>Akses pembayaran yang mudah dan terpercaya, serta kenyamanan koordinasi dengan vendor terpilihmu.</p>
            </div>
          </div>
        </div>
      </div>
      <a href="https://api.whatsapp.com/send?phone=6281292549505&text=" class="float text-white" target="_blank">
      <i class="fab fa-whatsapp my-float"></i>
      </a>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection