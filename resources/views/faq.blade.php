@extends('layouts.homepage_layout')

@section('navigation')
    @parent
@endsection

@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
      <li data-target="#demo" data-slide-to="0" class="active"></li>
      <li data-target="#demo" data-slide-to="1"></li>
      <li data-target="#demo" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{ asset('assets/img/highlights/dashboard/1.png') }}" alt="Perjuangan" width="100%">
          <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Perjuangan</h3>
            <p>Hal paling membahagiakan di dunia adalah saat bisa bersama orang yang kita cintai.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/2.png') }}" alt="Masa Depan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Masa Depan</h3>
            <p>Pernikahan adalah awal menuju kehidupan yang bebas dan penuh rintangan.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/3.png') }}" alt="Kebersamaan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Kebersamaan</h3>
            <p>Saat bersama orang yang kita percaya, hidup akan menjadi lebih mudah.</p>
          </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div> 
    <section class="site-section">
      <div class="container-fluid">
        <div class="row">
          <div class="row col-md-12">
            <div class="col-md-12 no-gutter card-body border rounded">
              @foreach($categories as $index_category => $category)
              <div class="panel-group" id="accordion">
                <div class="panel card-header bg-light border">{{ $category->name }}</div>
                <div class="panel panel-default card-body border">
                  @foreach($category->faqs as $index_faq => $faq)
                    <div class="panel-heading border border-top-0 border-left-0 border-right-0">
                      <a class="accordion-toggle text-darkpink" data-toggle="collapse" data-parent="#accordion" href="#faq{{ $index_category }}_{{ $index_faq }}">
                      {{ $faq->ask }}</a>
                    </div>
                    <div id="faq{{ $index_category }}_{{ $index_faq }}" class="panel-collapse collapse mt-2 pl-2">
                      <div class="panel-body border border-right-0 border-top-0 border-bottom-0 p-3">
                      {!! $faq->question !!}
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection