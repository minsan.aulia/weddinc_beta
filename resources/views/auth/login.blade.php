@extends('layouts.homepage2_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <section class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center mb-5"><img src="{{ asset('assets/img/selamat_datang.png') }}" style="height: 40px; width: auto; "></div>
          <div class="row col-md-12 text-center bg-light">
            <div class="col-md-6 offset-md-1 no-gutter">
              <div class="col-12 rounded">
                  <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link" id="nav-signup-tab" data-toggle="tab" href="#nav-login" role="tab" aria-controls="nav-planner" aria-selected="true">Masuk</a>
                      <a class="nav-item nav-link" id="nav-signup-tab" data-toggle="tab" href="#nav-signup" role="tab" aria-controls="nav-vendor" aria-selected="false">Daftar</a>
                    </div>
                  </nav>
                  <div class="tab-content py-3 px-3 px-sm-0 bg-light" id="nav-tabContent">
                    <div class="tab-pane fade show active bg-light" id="nav-login" role="tabpanel" aria-labelledby="nav-signup-tab">
                      <div class="row">
                        <div class="col-md-9 mx-auto">
                          <div class="card card-signin my-5">
                            <div class="card-body">
                              <h5 class="card-title text-center">Masuk</h5>
                              <form action="{{ route('login') }}" method="POST" class="needs-validation" novalidate>
                                @csrf
                                <div class="form-row row">
                                  <div class="input-group col-md-12 mt-3">
                                    <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" placeholder="email" value="{{ old('email') }}" required autofocus>
                                      <div class="valid-feedback"><i><small>valid</small></i></div>
                                      <div class="invalid-feedback"><i><small>masukkan email</small></i></div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-row row">
                                  <div class="input-group col-md-12 mt-3">
                                    <div class="col-md-12">
                                      <input id="password" type="password" class="form-control" name="password" placeholder="password" required>
                                      <div class="valid-feedback"><i><small>valid</small></i></div>
                                      <div class="invalid-feedback"><i><small>masukkan password</small></i></div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-row row">
                                  <div class="input-group col-md-12 mt-2 justify-content-center">
                                    <div class="custom-control custom-checkbox mb-3 mt-1">
                                      <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                      <label class="custom-control-label" for="remember">Ingat kata sandi</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-row row">
                                  <div class="col-md-12 mt-2">
                                    <div class="row justify-content-center">
                                      <button type="submit" class="btn btn-dustypink btn-block mr-3 ml-3 text-uppercase">Masuk</button>
                                    </div>
                                  </div>
                                </div>
                              </form>
                                <hr class="my-4">
                                <div class="card text-left">
                                  <a href="{{ route('login.provider', 'google') }}"><div class="card-body p-2 border border-top-0 border-left-0 border-right-0 text-dark"><img src="{{ asset('assets/img/icon/social/google.png') }}" style="height: 25px;" class="pl-2 pr-5"> masuk dengan google</div></a>
                                  <a href="{{ route('login.provider', 'facebook') }}"><div class="card-body p-2 text-dark"><img src="{{ asset('assets/img/icon/social/facebook.png') }}" style="height: 25px;" class="pl-2 pr-5"> masuk dengan facebook</div></a>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade bg-light" id="nav-signup" role="tabpanel" aria-labelledby="nav-signup-tab">
                      <div class="row">
                        <div class="col-md-9 mx-auto">
                          <div class="card card-signin my-5">
                            <div class="card-body">
                              <h5 class="card-title text-center ">Daftar</h5>
                              <form method="POST" action="{{ route('register') }}" class="needs-validation" novalidate>
                              @csrf
                              <div class="form-row row">
                                <div class="input-group col-md-12 mt-3">
                                  <div class="col-md-12">
                                  <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="nama lengkap" required autofocus>
                                    <div class="valid-feedback"><i><small>valid</small></i></div>
                                    <div class="invalid-feedback"><i><small>masukkan nama lengkap</small></i></div>
                                  </div>
                                </div>
                              </div>
                              <div class="form-row row">
                                <div class="input-group col-md-12 mt-3">
                                  <div class="col-md-12">
                                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email" required>
                                    <div class="valid-feedback"><i><small>valid</small></i></div>
                                    <div class="invalid-feedback"><i><small>masukkan email</small></i></div>
                                  </div>
                                </div>
                              </div>
                              <div class="form-row row">
                                <div class="input-group col-md-12 mt-3">
                                  <div class="col-md-12">
                                  <input id="password" type="password" class="form-control" name="password" placeholder="password" required>
                                    <div class="valid-feedback"><i><small>valid</small></i></div>
                                    <div class="invalid-feedback"><i><small>masukkan password</small></i></div>
                                  </div>
                                </div>
                              </div>
                              <div class="form-row row mb-3">
                                <div class="input-group col-md-12 mt-3">
                                  <div class="col-md-12">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="ulangi password" required>
                                    <div class="valid-feedback"><i><small>valid</small></i></div>
                                    <div class="invalid-feedback"><i><small>masukkan passoword</small></i></div>
                                  </div>
                                </div>
                              </div>                                
                              <button class="btn btn-dustypink btn-block text-uppercase" type="button" data-toggle="modal" data-target="#modalprivacy">Daftar</button>
                                <div class="modal" id="modalprivacy">
                                  <div class="modal-dialog modal-dialog-scrollable text-left">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h1 class="modal-title">Kebijakan Privasi</h1>
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                      </div>
                                      <div class="modal-body">
                                      <div class="col-md-12">
                                        <br>
                                        <b>HARAP DIBACA TERLEBIH DAHULU YA</b>
                                        <br><br>
                                        <b>Terima kasih telah memilih Weddinc!</b>
                                        <p>Di Weddinc, kami ingin memberikan pengalaman sebaik mungkin untuk memastikan anda menikmati layanan kami kapanpun dibutuhkan. Untuk melakukannya, kami akan selalu memperhatikan kemudahaan, keamanan, dan kenyamanan untuk anda dalam mewujudkan pernikahan impian. Meski begitu, privasi dan keamanan data pribadi anda akan selalu dan senantiasa menjadi hal yang sangat penting bagi kami. Jadi, kami ingin secara transparan menjabarkan cara dan alasan kami mengumpulkan, menyimpan, berbagi, dan menggunakan data pribadi Anda- serta menguraikan kendali dan preferensi anda tentang waktu dan cara Anda memilih untuk berbagi data pribadi anda.</p>
                                        <p>Itulah tujuan kami, dan Kebijakan Privasi ini (“Kebijakan”) akan menjelaskan dengan tepat apa yang kami maksud secara lebih terperinci di bawah ini.</p>
                                        <br>
                                        <b>1.Pengumpulan Informasi</b>
                                        <p>Kami mengumpulkan informasi dari anda ketika anda mendaftar pada situs kami, masuk ke akun anda, melakukan transaksi, menggunakan promo, dan/atau ketika anda keluar. Data yang dikumpulkan mencakup nama anda, alamat email, nomor telepon, kebiasaan dalam menggunakan fitur, dan/atau kartu kredit.</p>
                                        <p>Selain itu, kami menerima dan merekam informasi secara otomatis dari gadget, komputer dan peramban anda termasuk alamat IP anda. Perlengakapan perangkat keras dan lunak anda dan halaman yang anda minta.</p>
                                        <br>
                                        <b>2. Penggunaan Informasi</b>
                                        <p>Segala informasi yang kami kumpulkan dari anda dapat digunakan untuk:</p>
                                        <p>
                                          <ul>
                                            <li>Personalisasi pengalaman dan tanggapan anda sesuai kebutuhan pribadi anda</li>
                                            <li>Menyediakan konten iklan yang disesuaikan</li>
                                            <li>Meningkatkan aplikasi dan/atau situs web kami</li>
                                            <li>Meningkatkan layanan pelanggan dan mendukung kebutuhan anda</li>
                                            <li>Menghubungi anda lewat email</li>
                                            <li>Mengatur kontes, promo, atau survei</li>
                                          </ul>  
                                        </p>
                                        <br>
                                        <b>3. Privasi E-Dagang</b>
                                        <p>Kami dalam hal ini adalah Weddinc (PT Wahana Sidharta Madjadikara) adalah pemilik tunggal dari informasi yang dikumpulkan pada situs ini. Informasi pribadi anda yang dapat diidentifikasi tidak akan dijual, dipertukarkan, ditransfer, atau diberikan kepada perusahaan lain dengan alasan apa pun juga, tanpa mendapatkan izin anda selain dari hanya semata-mata untuk memenuhi permohonan dan/atau pemesanan, misalnya untuk proses transaksi/pesanan dan sebagainya.</p>
                                        <b>4. Pengungkapan Pihak Ketiga</b>
                                        <p>Kami tidak menjual, memperdagangkan, atau pun memindahkan keluar dari pihak kami informasi pribadi anda yang dapat diidentifikasi. Kami tidak mengikutkan pihak ketiga tepercaya yang membantu kami dalam mengoperasikan situs web kami atau menyelenggarakan bisnis kami, selama semua pihak setuju untuk menjaga kerahasiaan informasi ini.</p>
                                        <p>Kami meyakini akan pentingnya berbagi informasi dalam rangka menyelidiki, mencegah, atau mengambil tindakan menyangkut aktivitas ilegal, dugaan penipuan, keadaan yang melibatkan ancaman terhadap keamanan fisik seseorang, pelanggaran atas syarat-syarat penggunaan kami, atau hal lain yang diwajibkan oleh hukum.</p>
                                        <p>Namun, informasi bukan privat dapat diberikan kepada pihak lain untuk pemasaran, pariwara, atau penggunaan lain.</p>
                                        <br>
                                        <b>5. Proteksi Informasi</b>
                                        <p>Weddinc menerapkan berbagai langkah keamanan guna menjaga keamanan informasi pribadi anda. Weddinc menggunakan enkripsi teknologi mutakhir untuk memproteksi informasi sensitif yang dikirimkan secara online. Secure Sockets Layer (SSL) menyediakan keamanan bagi setiap pengguna dan kebebasan untuk menelusuri setiap lini website tanpa rasa khawatir mengenai kemungkinan pencurian informasi pribadi pengguna. Database yang digunakan untuk menyimpan informasi pribadi pengguna menggunakan teknik enkripsi RSA dimana terdapat skema padding sehingga hanya pengguna yang  mengetahui passwordnya.</p>
                                        <br>
                                        <b>6. Berhenti Berlangganan</b>
                                        <p>Kami menggunakan alamat email yang anda berikan untuk mengirimi anda informasi dan pembaruan menyangkut transaksi/pesanan anda berita berkala perusahaan, informasi mengenai produk, dll. Bila kapan saja anda ingin berhenti menerima email kami di masa mendatang, kami memasukkan informasi mendetail informasi berhenti berlangganan di bawah tiap-tiap email.</p>
                                        <br>
                                        <b>7. Persetujuan</b>
                                        <p>Dengan menggunakan situs kami, anda setuju dengan kebijakan privasi kami.</p>
                                      </div>
                                      </div>
                                      <div class="modal-footer custom-control custom-checkbox justify-content-between ml-3">
                                        <div class="form-group">
                                          <div class="form-check">
                                            <input type="checkbox" class="custom-control-input" id="customCheck2" required>
                                            <label class="custom-control-label" for="customCheck2">Setuju dengan kebijakan privasi</label>
                                            <div class="invalid-feedback">
                                              Anda harus menyetujui kebijakan privasi.
                                            </div>
                                          </div>
                                        </div>
                                        <input class="btn btn-lg btn-dustypink btn-block text-uppercase" type="submit" value="Daftar"/>
                                      </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                                <hr class="my-4">
                                <div class="card text-left">
                                  <a href=""><div class="card-body p-2 border border-top-0 border-left-0 border-right-0 text-dark"><img src="{{ asset('assets/img/icon/social/google.png') }}" style="height: 25px;" class="pl-2 pr-5"> daftar dengan google</div></a>
                                  <a href=""><div class="card-body p-2 text-dark"><img src="{{ asset('assets/img/icon/social/facebook.png') }}" style="height: 25px;" class="pl-2 pr-5"> daftar dengan facebook</div></a>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 no-gutter sidebar-item">
              <div class="make-me-sticky">
                <div id="demo" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="{{ asset('assets/img/login/login_1.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Tetap tenang dan cukup katakan "Aku bersedia."</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="{{ asset('assets/img/login/login_2.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Buat langkah selanjutnya dalam kehidupan kita.</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="{{ asset('assets/img/login/login_3.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Hidup adalah momen kecil yang berharga.</p>
                      </div>
                    </div>
                  </div>
                </div>  
              </div>
            </div>
          </div>
          <div class="col-md-12 text-center">
            <div class="card-footer">Dipersembahkan oleh Wedd.inc © <?php echo date("Y"); ?></div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection