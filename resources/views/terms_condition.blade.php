@extends('layouts.homepage_layout')

@section('navigation')
    @parent
@endsection

@section('content')
    <div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
      <li data-target="#demo" data-slide-to="0" class="active"></li>
      <li data-target="#demo" data-slide-to="1"></li>
      <li data-target="#demo" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{ asset('assets/img/highlights/dashboard/1.png') }}" alt="Perjuangan" width="100%">
          <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Perjuangan</h3>
            <p>Hal paling membahagiakan di dunia adalah saat bisa bersama orang yang kita cintai.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/2.png') }}" alt="Masa Depan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Masa Depan</h3>
            <p>Pernikahan adalah awal menuju kehidupan yang bebas dan penuh rintangan.</p>
          </div>
      </div>
      <div class="carousel-item">
        <img src="{{ asset('assets/img/highlights/dashboard/3.png') }}" alt="Kebersamaan" width="100%">
        <div class="carousel-caption card-body bg-dark rounded" style="opacity: 0.5">
            <h3>Kebersamaan</h3>
            <p>Saat bersama orang yang kita percaya, hidup akan menjadi lebih mudah.</p>
          </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>  
    <section class="site-section">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 offset-md-1">
            <div style="text-align: center;">
                <b>SYARAT DAN KETENTUAN PENGGUNA WEDDINC</b></div>
                <br />
                @foreach ($termscondition_user as $termscondition_user)
                  {!! $termscondition_user->content !!}
                @endforeach
          </div>
        </div>
      </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->

@endsection