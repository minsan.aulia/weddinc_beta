<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Admin - Weddinc</title>
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-custom.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome/css/all.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/admin_style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/admin_components.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/summernote.css') }}">
</head>

<body>
    @section('navigation')
    <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
            <div class="search-result">
              <div class="search-header">
                Histories
              </div>
            </div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link nav-link-lg message-toggle beep"><i class="far fa-envelope"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-header">Messages
                <div class="float-right">
                  <a href="#">Mark All As Read</a>
                </div>
              </div>
              <div class="dropdown-list-content dropdown-list-message">
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <div class="dropdown-item-avatar">
                    <img alt="image" src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle">
                    <div class="is-online"></div>
                  </div>
                  <div class="dropdown-item-desc">
                    <b>Kusnaedi</b>
                    <p>Hello, Bro!</p>
                    <div class="time">10 Hours Ago</div>
                  </div>
                </a>
              </div>
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </li>
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-header">Notifications
                <div class="float-right">
                  <a href="#">Mark All As Read</a>
                </div>
              </div>
              <div class="dropdown-list-content dropdown-list-icons">
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <div class="dropdown-item-icon bg-primary text-white">
                    <i class="fas fa-code"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Template update is available now!
                    <div class="time text-primary">2 Min Ago</div>
                  </div>
                </a>
              </div>
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </li>
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="{{ asset('assets/img/icon/dummy_user.png') }}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">nama</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="#" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="{{ route('dashboard.admin') }}">WEDDINC</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('dashboard.admin') }}">WD</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Dashboard</li>
              <li class="nav-item dropdown active">
                <a href="{{ route('dashboard.admin') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a></li>
              <li class="menu-header">Master Data</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> <span>Manage Users</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('data_admin.admin') }}">Admin</a></li>
                  <li><a class="nav-link" href="{{ route('data_user.admin') }}">User</a></li>
                  <li><a class="nav-link" href="{{ route('data_vendor.admin') }}">Vendor</a></li>
                </ul>
              </li>
              <li class="menu-header">Document</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-file"></i> <span>For User</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('about_user.admin') }}">About</a></li>
                  <li><a class="nav-link" href="{{ route('termscondition_user.admin') }}">Terms & Condition</a></li>
                  <li><a class="nav-link" href="{{ route('privacypolicy_user.admin') }}">Privacy Policy</a></li>
                  <li><a class="nav-link" href="{{ route('faq_user.admin') }}">FAQ</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-file-alt"></i> <span>For Vendor</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('about_vendor.admin') }}">About</a></li>
                  <li><a class="nav-link" href="{{ route('termscondition_vendor.admin') }}">Terms & Condition</a></li>
                  <li><a class="nav-link" href="{{ route('privacypolicy_vendor.admin') }}">Privacy Policy</a></li>
                  <li><a class="nav-link" href="{{ route('faq_vendor.admin') }}">FAQ</a></li>
                </ul>
              </li>
              <li class="menu-header">Blog</li>
              <li><a class="nav-link" href="https://blog.weddinc.id/wp-admin/"><i class="fab fa-wordpress"></i> <span>WeddBlog</span></a></li>
            </ul>
        </aside>
      </div>
        @show
              
        @yield('content')
                
        @section('footer')
        <footer class="main-footer">
        <div class="footer-left">
          <!-- Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a> -->
        </div>
        <div class="footer-right">
          2.3.0
        </div>
      </footer>
    </div>
  </div>
    @show

  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('assets/js/moment.min.js') }}"></script>
  <script src="{{ asset('assets/js/admin_stisla.js') }}"></script>
  <script src="{{ asset('assets/js/admin_scripts.js') }}"></script>
  <script src="{{ asset('assets/js/admin_custom.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/js/summernote.js') }}"></script>

    @yield('js') <!-- Make your custom JavaScript -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.datatables').DataTable();
    } );
  </script>
  <script>
    $('#summernote').summernote({
    tabsize: 2,
    height: 250
    });
  </script>
    </body>
</html>