<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Wedding Planner & Vendor Commerce</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta content='text/html; charset=UTF-8' http-equiv='Content-Type'/>
        <link href="{{ asset('assets/img/logo/favicon.png') }}" rel='icon' type='image/x-icon'/>
        <link rel="alternate" type="application/atom+xml" title="weddinc - Atom" href="LINK ATOM" />
        <link rel="alternate" type="application/rss+xml" title="Weddinc - RSS" href="LINK RSS" />
        <link rel="service.post" type="application/atom+xml" title="Weddinc - Atom" href="LINK ATOM" />
        <link rel="alternate" type="application/atom+xml" title="Weddinc - Atom" href="LINK ATOM" />
        <link href="{{ asset('assets/img/logo/favicon.png') }}" rel="image_src"/>
        <meta content='Wedding Planner & Vendor Commerce' name='description'/>
        <meta content='https://weddinc.id' property='og:url'/>
        <meta content='Wedding Planner & Vendor Commerce' property='og:title'/>
        <meta content='Wedding Planner & Vendor Commerce' property='og:description'/>
        <meta content="{{ asset('assets/img/logo/favicon.png') }}" property='og:image'/>
        <meta content='wedding planer, wedding organizer, vendor wedding, wo indonesia, wedding venue' name='keywords'/>
        <meta content='article' property='og:type'/>
        <meta content='Wedd.inc' property='og:site_name'/>
        <meta content='BING WEBMASTER' name='msvalidate.01'/>
        <link rel="shortcut icon" type="image/png" href="{{ asset('assets/img/logo/favicon.png') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/quicksand-googleapis.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/flaticon/font/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/icomoon/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome/css/all.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/baguetteBox.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-custom.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}" />
        <script src="{{ asset('assets/js/chartjs/utils.js') }}"></script>
    </head>
  <body>
    @section('navigation')

        @show

        @yield('content')

        @section('footer')

    @show

  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/js/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/js/aos.js') }}"></script>
  <script src="{{ asset('assets/js/typed.js') }}"></script>
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script src="{{ asset('assets/js/baguetteBox.min.js') }}"></script>
  <script src="{{ asset('assets/lib/jquery/jquery.mask.min.js') }}"></script>

    @yield('js') <!-- Make your custom JavaScript -->

    <script type="text/javascript">
    $(document).ready(function() {
      $('#dataTables').DataTable();
    } );
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.money').mask('0.000.000.000', {reverse: true});
      $('.phone').mask('0000−0000−0000');
      $('.year').mask('0000/0000');
    })
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
        console.log("document is ready");
        $( ".cardhover" ).hover(
            function() {
                $(this).addClass('border-danger').css('cursor', 'pointer');
            }, function() {
                $(this).removeClass('border-danger');
            }
            );
    });
  </script>
  <script>
    function calculate_weddingbudget() {
    let budgetinput = document.getElementById("budget").value;
    let budget = budgetinput.split('.').join('');
    let guestinput = document.getElementById("guest").value;
    let guest = guestinput.split('.').join('');
    var resepsi = +budget * 0.15;
    var prosesi = +budget * 0.02;
    var menu_utama = +budget * 0.2;
    var pondokan = +budget * 0.05;
    var wedding_cake = +budget * 0.02;
    var dekorasi = +budget * 0.13;
    var busana_wanita = +budget * 0.05;
    var rias = +budget * 0.03;
    var busana_pria = +budget * 0.03;
    var mc = +budget * 0.08;
    var dok_prewed = +budget * 0.04;
    var dok_foto = +budget * 0.04;
    var dok_video = +budget * 0.02;
    var mobil = +budget * 0.04;
    var mahar = +budget * 0.03;
    var undangan = +budget * 0.01;
    var undangan_pcs = +undangan / guest;
    var souvenir = +budget * 0.03;
    var souvenir_pcs = +souvenir / guest;
    var biaya_takterduga = +budget * 0.04;
    document.getElementById("resepsi").innerHTML = 'Rp.' + resepsi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("prosesi").innerHTML = 'Rp.' + prosesi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("menu_utama").innerHTML = 'Rp.' + menu_utama.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("pondokan").innerHTML = 'Rp.' + pondokan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("wedding_cake").innerHTML = 'Rp.' + wedding_cake.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dekorasi").innerHTML = 'Rp.' + dekorasi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("busana_wanita").innerHTML = 'Rp.' + busana_wanita.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("rias").innerHTML = 'Rp.' + rias.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("busana_pria").innerHTML = 'Rp.' + busana_pria.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mc").innerHTML = 'Rp.' + mc.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_prewed").innerHTML = 'Rp.' + dok_prewed.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_foto").innerHTML = 'Rp.' + dok_foto.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_video").innerHTML = 'Rp.' + dok_video.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mobil").innerHTML = 'Rp.' + mobil.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mahar").innerHTML = 'Rp.' + mahar.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("undangan").innerHTML = 'Rp.' + undangan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("undangan_pcs").innerHTML = '(Rp.' + undangan_pcs.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '/undangan)';
    document.getElementById("souvenir").innerHTML = 'Rp.' + souvenir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("souvenir_pcs").innerHTML = '(Rp.' + souvenir_pcs.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '/undangan)';
    document.getElementById("biaya_takterduga").innerHTML = 'Rp.' + biaya_takterduga.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }
  </script>
  <script type="text/javascript">
    $(function() {
      $('.example1').accordion({ multiOpen: true });
    });
  </script>
  <script type="text/javascript">
    $('.budgetting a').click(function (e) {
    e.preventDefault();
    var href = $(this).attr('href');
      $('.budgetting-tab').fadeOut(1000).promise().done(function(){
        $(href).fadeIn(1000);
      });
  });
  </script>
  <script>
    $(document).ready(function(){
      $(".slide-toggle").click(function(){
        $(".planner1").animate({width: "toggle"}, 500);
        $(".planner2").animate({width: "toggle"}, 500);
      });
    });
  </script>
  <script type="text/javascript" language="javascript">
    function checkformlogin()
    {
        var f = document.forms["formlogin"].elements;
        var cansubmit = true;
        for (var i = 0; i < f.length; i++)
          {if (f[i].value.length == 0) cansubmit = false;}
        if (cansubmit) {document.getElementById('submitlogin').disabled = false;}
        else {document.getElementById('submitlogin').disabled = 'disabled';}
    }
    function checkformregister()
    {
        var f = document.forms["formregister"].elements;
        var cansubmit = true;
        for (var i = 0; i < f.length; i++)
          {if (f[i].value.length == 0) cansubmit = false;}
        if (cansubmit) {document.getElementById('submitregister').disabled = false;}
        else {document.getElementById('submitregister').disabled = 'disabled';}
    }
  </script>
  <script type="text/javascript">
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            else{ window.location.assign("https://weddinc.id/");
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
  </script>
  </script>
