<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Wedding Planner & Vendor Commerce | @yield('title')</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta content='text/html; charset=UTF-8' http-equiv='Content-Type'/>
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{ asset('assets/img/logo/favicon.png') }}">

        <link rel="stylesheet" href="{{ asset('assets/fonts/quicksand-googleapis.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/flaticon/font/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/icomoon/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome/css/all.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/baguetteBox.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-custom.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/summernote.css') }}">
    </head>
    <body>
        @section('navigation')
        <div class="site-wrap">
            <div class="site-mobile-menu">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
            </div>
            <header class="site-navbar py-4 bg-white" role="banner">
            <div class="site-wrap">
            <div class="site-mobile-menu">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
            </div>
            <header class="site-navbar py-2 bg-white pt-0" role="banner">
            <div class="container">
                <div class="row align-items-center">
                <div class="col-11 col-lg-2">
                    <h1 class="mb-0 site-logo"><a href="{{ route('index.owner') }}" class="text-black h2 mb-0"><img src="{{ asset('assets/img/logo/logo.png') }}" alt="" class="img-fluid"></a></h1>
                </div>
                <div class="col-12 col-md-10 d-none d-xl-block">
                    <nav class="site-navigation position-relative text-right" role="navigation">
                    <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block pl-0">
                        <li class="has-children">
                        <a href="#" class="text-dark"><i class="far fa-lg fa-bell"></i></a>
                        <ul class="dropdown">
                            <li><a href="#" class="dropdown-item text-darktosca p-1">Promo menarik agustusan!</a></li>
                            <li><a href="#" class="dropdown-item text-darktosca p-1">Cashback super banyak!</a></li>
                            <li><a href="#" class="dropdown-item text-darktosca p-1">Transaksi pertama bonus hadiah!</a></li>
                            <li><a href="#" class="dropdown-item text-darktosca p-1">Dikson nikah 10% bulan Januari</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('notification.owner') }}" class="dropdown-item text-darktosca p-1">Selengkapnya</a></li>
                        </ul>
                        </li>
                        <!-- <li class="has-children">
                        <a href="#" class="text-darktosca"><img src="{{ asset('assets/img/icon/flag/id.png') }}" style="height: 25px; width: 25px;"> ID</a>
                        <ul class="dropdown">
                            <li><a href="#" class="text-darktosca"><img src="{{ asset('assets/img/icon/flag/id.png') }}" style="height: 25px; width: 25px;"> Indonesian</a></li>
                            <li><a href="#" class="text-darktosca"><img src="{{ asset('assets/img/icon/flag/en.png') }}" style="height: 25px; width: 25px;"> English</a></li>
                        </ul>
                        </li> -->
                        <li class="has-children">
                        <a href="#" class="text-darktosca" value="">{{ session('name') }}</a>
                        <ul class="dropdown">
                            <li><a href="{{ route('vendor.owner') }}" class="text-darktosca"><i class="fa fa-cog"></i> Pengaturan</a></li>
                            <li><a href="{{ route('owner.owner') }}" class="text-darktosca"><i class="fa fa-user"></i> Lihat Profil</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="javascript:void" onclick="$('#logout-form').submit();" class="text-darktosca"><i class="fas fa-sign-out-alt"></i> Keluar</a></li>
                        </ul>
                        </li>
                    </ul>
                    </nav>
                    <form id="logout-form" action="{{ route('logout.owner') }}" method="GET" style="display: none;">
                        @csrf
                    </form>
                </div>
                <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>
                </div>
                </div>
            </div>
            </header>
        @show

        @yield('content')

        @section('footer')
        <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h5>Perusahaan</h5>
                    <ul>
                        <li>- <a href="#">Karir</a></li>
                        <li>- <a href="https://blog.weddinc.id">Blog</a></li>
                        <li>- <a href="{{ route('terms_condition.owner') }}">Syarat dan Ketentuan</a></li>
                        <li>- <a href="{{ route('privacy_policy.owner') }}">Kebijakan Privasi</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Bisnis</h5>
                    <ul>
                        <li>- <a href="#">Pusat Penjual</a></li>
                        <li>- <a href="#">Pusat Pengguna</a></li>
                        <li>- <a href="{{ route('faq.owner') }}">FAQ</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Dukungan</h5>
                    <ul>
                        <li>- <a href="{{ route('about.owner') }}">Tentang Weddinc</a></li>
                        <li>- <a href="#">Kategori</a></li>
                        <li>- <a href="#">Promo</a></li>
                        <li>- <a href="{{ route('contact_us.owner') }}">Hubungi Kami</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 info">
                    <h5>Tentang Kami</h5>
                    <p>Weddinc adalah platform Teknologi Wedding Planner & Commerce yang bertujuan untuk mewujudkan impian pernikahan anda.</p>
                </div>
            </div>
        </div>
        <div class="second-bar">
            <div class="container">
            <div class="row">
                <div class="col-6">
                    <h2 class="logo"><a href="/"><img src="{{ asset('assets/img/logo/logo-alt.png') }}" width="168" height="35"></a></h2>
                </div>
                <div class="col-6">
                    <div class="col-sm-12 my-auto">
                        <ul class="social-media">
                            <li><a href="https://www.instagram.com/weddinc.id" class="p-2"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </footer>
  </div>
  @show


  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/js/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/js/aos.js') }}"></script>
  <script src="{{ asset('assets/js/typed.js') }}"></script>
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script src="{{ asset('assets/js/baguetteBox.min.js') }}"></script>
  <script src="{{ asset('assets/js/highstock.js') }}"></script>
  <script src="{{ asset('assets/js/exporting.js') }}"></script>
  <script src="{{ asset('assets/js/chartjs/Chart.min.js') }}"></script>
  <script src="{{ asset('assets/js/chartjs/utils.js') }}"></script>
  <script src="{{ asset('assets/lib/jquery/jquery.mask.min.js') }}"></script>
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script src="{{ asset('assets/js/summernote.js') }}"></script>

  @yield('js') <!-- Make your custom JavaScript -->

  <script type="text/javascript">
    $(document).ready(function() {
      $('#dataTables').DataTable();
    } );
  </script>
  <script>
    $('#summernote').summernote({
    tabsize: 2,
    height: 250
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.money').mask('0.000.000.000', {reverse: true});
      $('.phone').mask('0000−0000−0000');
      $('.year').mask('0000/0000');
    })
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
        console.log("document is ready");
        $( ".cardhover" ).hover(
            function() {
                $(this).addClass('border-danger').css('cursor', 'pointer');
            }, function() {
                $(this).removeClass('border-danger');
            }
            );
    });
  </script>
  <script>
    function calculate_weddingbudget() {
    let budgetinput = document.getElementById("budget").value;
    let budget = budgetinput.split('.').join('');
    let guestinput = document.getElementById("guest").value;
    let guest = guestinput.split('.').join('');
    var resepsi = +budget * 0.15;
    var prosesi = +budget * 0.02;
    var menu_utama = +budget * 0.2;
    var pondokan = +budget * 0.05;
    var wedding_cake = +budget * 0.02;
    var dekorasi = +budget * 0.13;
    var busana_wanita = +budget * 0.05;
    var rias = +budget * 0.03;
    var busana_pria = +budget * 0.03;
    var mc = +budget * 0.08;
    var dok_prewed = +budget * 0.04;
    var dok_foto = +budget * 0.04;
    var dok_video = +budget * 0.02;
    var mobil = +budget * 0.04;
    var mahar = +budget * 0.03;
    var undangan = +budget * 0.01;
    var undangan_pcs = +undangan / guest;
    var souvenir = +budget * 0.03;
    var souvenir_pcs = +souvenir / guest;
    var biaya_takterduga = +budget * 0.04;
    document.getElementById("resepsi").innerHTML = 'Rp.' + resepsi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("prosesi").innerHTML = 'Rp.' + prosesi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("menu_utama").innerHTML = 'Rp.' + menu_utama.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("pondokan").innerHTML = 'Rp.' + pondokan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("wedding_cake").innerHTML = 'Rp.' + wedding_cake.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dekorasi").innerHTML = 'Rp.' + dekorasi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("busana_wanita").innerHTML = 'Rp.' + busana_wanita.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("rias").innerHTML = 'Rp.' + rias.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("busana_pria").innerHTML = 'Rp.' + busana_pria.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mc").innerHTML = 'Rp.' + mc.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_prewed").innerHTML = 'Rp.' + dok_prewed.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_foto").innerHTML = 'Rp.' + dok_foto.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_video").innerHTML = 'Rp.' + dok_video.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mobil").innerHTML = 'Rp.' + mobil.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mahar").innerHTML = 'Rp.' + mahar.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("undangan").innerHTML = 'Rp.' + undangan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("undangan_pcs").innerHTML = '(Rp.' + undangan_pcs.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '/undangan)';
    document.getElementById("souvenir").innerHTML = 'Rp.' + souvenir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("souvenir_pcs").innerHTML = '(Rp.' + souvenir_pcs.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '/undangan)';
    document.getElementById("biaya_takterduga").innerHTML = 'Rp.' + biaya_takterduga.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }
  </script>
  <script type="text/javascript">
    $(function() {
      $('.example1').accordion({ multiOpen: true });
    });
  </script>
  <script type="text/javascript">
    $('.budgetting a').click(function (e) {
    e.preventDefault();
    var href = $(this).attr('href');
      $('.budgetting-tab').fadeOut(1000).promise().done(function(){
        $(href).fadeIn(1000);
      });
  });
  </script>
  <script>
    $(document).ready(function(){
      $(".slide-toggle").click(function(){
        $(".planner1").animate({width: "toggle"}, 500);
        $(".planner2").animate({width: "toggle"}, 500);
      });
    });
  </script>
  <script>
      function changePricelist() {
          $('#imagepricelist').click();
      }
      $('#imagepricelist').change(function () {
          var imgPath = this.value;
          var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
              readURLPricelist(this);
          else
              alert("Please select image file (jpg, jpeg, png).")
      });
      function readURLPricelist(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.readAsDataURL(input.files[0]);
              reader.onload = function (e) {
                  $('#previewpricelist').attr('src', e.target.result);
                  $("#removeimagepricelist").val(0);
              };
          }
      }
      function removePricelist() {
          $('#previewpricelist').attr('src', '{{ asset('assets/img/icon/dummy.png') }}');
          $("#removeimagepricelist").val(1);
      }

      function changeGallery1() {
          $('#imagegallery1').click();
      }
      $('#imagegallery1').change(function () {
          var imgPath = this.value;
          var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
              readURLGallery1(this);
          else
              alert("Please select image file (jpg, jpeg, png).")
      });
      function readURLGallery1(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.readAsDataURL(input.files[0]);
              reader.onload = function (e) {
                  $('#previewgallery1').attr('src', e.target.result);
                  $("#removegallery1").val(0);
              };
          }
      }
      function removeGallery1() {
          $('#previewgallery1').attr('src', '{{ asset('assets/img/icon/dummy.png') }}');
          $("#removegallery1").val(1);
      }

      function changeGallery2() {
          $('#imagegallery2').click();
      }
      $('#imagegallery2').change(function () {
          var imgPath = this.value;
          var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
              readURLGallery2(this);
          else
              alert("Please select image file (jpg, jpeg, png).")
      });
      function readURLGallery2(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.readAsDataURL(input.files[0]);
              reader.onload = function (e) {
                  $('#previewgallery2').attr('src', e.target.result);
                  $("#removegallery2").val(0);
              };
          }
      }
      function removeGallery2() {
          $('#previewgallery2').attr('src', '{{ asset('assets/img/icon/dummy.png') }}');
          $("#removegallery2").val(1);
      }

      function changeGallery3() {
          $('#imagegallery3').click();
      }
      $('#imagegallery3').change(function () {
          var imgPath = this.value;
          var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
              readURLGallery3(this);
          else
              alert("Please select image file (jpg, jpeg, png).")
      });
      function readURLGallery3(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.readAsDataURL(input.files[0]);
              reader.onload = function (e) {
                  $('#previewgallery3').attr('src', e.target.result);
                  $("#removegallery3").val(0);
              };
          }
      }
      function removeGallery3() {
          $('#previewgallery3').attr('src', '{{ asset('assets/img/icon/dummy.png') }}');
          $("#removegallery3").val(1);
      }

      function changeGallery4() {
          $('#imagegallery4').click();
      }
      $('#imagegallery4').change(function () {
          var imgPath = this.value;
          var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
              readURLGallery4(this);
          else
              alert("Please select image file (jpg, jpeg, png).")
      });
      function readURLGallery4(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.readAsDataURL(input.files[0]);
              reader.onload = function (e) {
                  $('#previewgallery4').attr('src', e.target.result);
                  $("#removegallery4").val(0);
              };
          }
      }
      function removeGallery4() {
          $('#previewgallery4').attr('src', '{{ asset('assets/img/icon/dummy.png') }}');
          $("#removegallery4").val(1);
      }

      function changeGallery5() {
          $('#imagegallery5').click();
      }
      $('#imagegallery5').change(function () {
          var imgPath = this.value;
          var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
              readURLGallery5(this);
          else
              alert("Please select image file (jpg, jpeg, png).")
      });
      function readURLGallery5(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.readAsDataURL(input.files[0]);
              reader.onload = function (e) {
                  $('#previewgallery5').attr('src', e.target.result);
                  $("#removegallery5").val(0);
              };
          }
      }
      function removeGallery5() {
          $('#previewgallery5').attr('src', '{{ asset('assets/img/icon/dummy.png') }}');
          $("#removegallery5").val(1);
      }

      function changeGallery6() {
          $('#imagegallery6').click();
      }
      $('#imagegallery6').change(function () {
          var imgPath = this.value;
          var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
              readURLGallery6(this);
          else
              alert("Please select image file (jpg, jpeg, png).")
      });
      function readURLGallery6(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.readAsDataURL(input.files[0]);
              reader.onload = function (e) {
                  $('#previewgallery6').attr('src', e.target.result);
                  $("#removegallery6").val(0);
              };
          }
      }
      function removeGallery6() {
          $('#previewgallery6').attr('src', '{{ asset('assets/img/icon/dummy.png') }}');
          $("#removegallery6").val(1);
      }
  </script>
  <script type="text/javascript">
    baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
  </script>
  <script type="text/javascript">
    $(function() {
      $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function(data) {
        $('#statvisitor').highcharts('StockChart', {
          rangeSelector: {
            buttons: [
              {type: 'month',count: 1,text: '1m'},
              {type: 'year',count: 1,text: '1y'},
              {type: 'all',text: 'All'}
            ],
          },
          title : {text : ''},
          credits : {enabled : false},
          xAxis: {events:
            {afterSetExtremes: function(e) {
              if (e.trigger == "rangeSelectorButton" && e.rangeSelectorButton.text == "Date"){
                // it is your button that caused this,
                // so setExtrememes to your custom
                // have to do in timeout to let
                // highcharts finish processing events...
                  setTimeout(function(){
                    Highcharts.charts[0].xAxis[0].setExtremes(1198681756385,1368144000000)
                  }, 1);
                }
              },
            }
          },
          series : [{name : 'Pengunjung',data : data,tooltip: {valueDecimals: 0}}]
        });
      });
    });
    $(function() {
      $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function(data) {
        $('#statpackagevisitor').highcharts('StockChart', {
          rangeSelector: {
            buttons: [
              {type: 'month',count: 1,text: '1m'},
              {type: 'year',count: 1,text: '1y'},
              {type: 'all',text: 'All'}
            ],
          },
          title : {text : ''},
          credits : {enabled : false},
          xAxis: {events:
            {afterSetExtremes: function(e) {
              if (e.trigger == "rangeSelectorButton" && e.rangeSelectorButton.text == "Date"){
                // it is your button that caused this,
                // so setExtrememes to your custom
                // have to do in timeout to let
                // highcharts finish processing events...
                  setTimeout(function(){
                    Highcharts.charts[0].xAxis[0].setExtremes(1198681756385,1368144000000)
                  }, 1);
                }
              },
            }
          },
          series : [{name : 'Pengunjung',data : data,tooltip: {valueDecimals: 2}}]
        });
      });
    });$(function() {
      $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function(data) {
        $('#statdiscussion').highcharts('StockChart', {
          rangeSelector: {
            buttons: [
              {type: '',count: 2,text: 'Date'},
              {type: 'hour',count: 1,text: '1h'},
              {type: 'day',count: 1,text: '1d'},
              {type: 'month',count: 1,text: '1m'},
              {type: 'year',count: 1,text: '1y'},
              {type: 'all',text: 'All'}
            ],
          },
          title : {text : ''},
          xAxis: {events:
            {afterSetExtremes: function(e) {
              if (e.trigger == "rangeSelectorButton" && e.rangeSelectorButton.text == "Date"){
                // it is your button that caused this,
                // so setExtrememes to your custom
                // have to do in timeout to let
                // highcharts finish processing events...
                  setTimeout(function(){
                    Highcharts.charts[0].xAxis[0].setExtremes(1198681756385,1368144000000)
                  }, 1);
                }
              },
            }
          },
          series : [{name : 'Diskusi',data : data,tooltip: {valueDecimals: 2}}]
        });
      });
    });$(function() {
      $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function(data) {
        $('#stattransaction').highcharts('StockChart', {
          rangeSelector: {
            buttons: [
              {type: '',count: 2,text: 'Date'},
              {type: 'hour',count: 1,text: '1h'},
              {type: 'day',count: 1,text: '1d'},
              {type: 'month',count: 1,text: '1m'},
              {type: 'year',count: 1,text: '1y'},
              {type: 'all',text: 'All'}
            ],
          },
          title : {text : ''},
          xAxis: {events:
            {afterSetExtremes: function(e) {
              if (e.trigger == "rangeSelectorButton" && e.rangeSelectorButton.text == "Date"){
                // it is your button that caused this,
                // so setExtrememes to your custom
                // have to do in timeout to let
                // highcharts finish processing events...
                  setTimeout(function(){
                    Highcharts.charts[0].xAxis[0].setExtremes(1198681756385,1368144000000)
                  }, 1);
                }
              },
            }
          },
          series : [{name : 'Transaksi',data : data,tooltip: {valueDecimals: 2}}]
        });
      });
    });
  </script>
  <script type="text/javascript">
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';
    function number_format(number, decimals, dec_point, thousands_sep) {
      number = (number + '').replace(',', '').replace(' ', '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {var k = Math.pow(10, prec); return '' + Math.round(n * k) / k;};
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);}
      if ((s[1] || '').length < prec) {s[1] = s[1] || ''; s[1] += new Array(prec - s[1].length + 1).join('0');}
      return s.join(dec);
    }
    var ctx = document.getElementById("visitorstat");
    var visitorstat = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Pengunjung",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: [0, 10000, 5000, 15000, 10000, 20000, 15000, 25000, 20000, 30000, 25000, 40000],
        }],
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {left: 10, right: 25, top: 25, bottom: 0}
        },
        scales: {
          xAxes: [
          {time:
            {unit: 'date'},
            gridLines: {display: false, drawBorder: false},
            ticks: {maxTicksLimit: 7}
          }],
          yAxes: [{
            ticks: {
              maxTicksLimit: 5,
              padding: 10,
              callback: function(value, index, values) {
                return '' + number_format(value);
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2]
            }
          }],
        },
        legend: {
          display: false
        },
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          intersect: false,
          mode: 'index',
          caretPadding: 10,
          callbacks: {
            label: function(tooltipItem, chart) {
              var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
              return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
            }
          }
        }
      }
    });
    var ctx = document.getElementById("packagevisitorstat");
    var packagevisitorstat = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Pengunjung",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: [0, 10000, 5000, 15000, 10000, 20000, 15000, 25000, 20000, 30000, 25000, 40000],
        }],
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {left: 10, right: 25, top: 25, bottom: 0}
        },
        scales: {
          xAxes: [
          {time:
            {unit: 'date'},
            gridLines: {display: false, drawBorder: false},
            ticks: {maxTicksLimit: 7}
          }],
          yAxes: [{
            ticks: {
              maxTicksLimit: 5,
              padding: 10,
              callback: function(value, index, values) {
                return '' + number_format(value);
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2]
            }
          }],
        },
        legend: {
          display: false
        },
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          intersect: false,
          mode: 'index',
          caretPadding: 10,
          callbacks: {
            label: function(tooltipItem, chart) {
              var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
              return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
            }
          }
        }
      }
    });
    var ctx = document.getElementById("discussionstat");
    var discussionstat = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Pengunjung",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: [0, 10000, 5000, 15000, 10000, 20000, 15000, 25000, 20000, 30000, 25000, 40000],
        }],
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {left: 10, right: 25, top: 25, bottom: 0}
        },
        scales: {
          xAxes: [
          {time:
            {unit: 'date'},
            gridLines: {display: false, drawBorder: false},
            ticks: {maxTicksLimit: 7}
          }],
          yAxes: [{
            ticks: {
              maxTicksLimit: 5,
              padding: 10,
              callback: function(value, index, values) {
                return '' + number_format(value);
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2]
            }
          }],
        },
        legend: {
          display: false
        },
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          intersect: false,
          mode: 'index',
          caretPadding: 10,
          callbacks: {
            label: function(tooltipItem, chart) {
              var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
              return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
            }
          }
        }
      }
    });
    var ctx = document.getElementById("transactionstat");
    var transactionstat = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Pengunjung",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: [0, 10000, 5000, 15000, 10000, 20000, 15000, 25000, 20000, 30000, 25000, 40000],
        }],
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {left: 10, right: 25, top: 25, bottom: 0}
        },
        scales: {
          xAxes: [
          {time:
            {unit: 'date'},
            gridLines: {display: false, drawBorder: false},
            ticks: {maxTicksLimit: 7}
          }],
          yAxes: [{
            ticks: {
              maxTicksLimit: 5,
              padding: 10,
              callback: function(value, index, values) {
                return '' + number_format(value);
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2]
            }
          }],
        },
        legend: {
          display: false
        },
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          intersect: false,
          mode: 'index',
          caretPadding: 10,
          callbacks: {
            label: function(tooltipItem, chart) {
              var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
              return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
            }
          }
        }
      }
    });
  </script>
  <script type="text/javascript">
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            else{ window.location.assign("https://weddinc.id/");
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
  </script>
  </body>
</html>
