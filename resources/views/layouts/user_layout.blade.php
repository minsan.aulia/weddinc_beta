<!DOCTYPE html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Weddinc - Wedding Planner & Commerce</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content='text/html; charset=UTF-8' http-equiv='Content-Type'/>
    <link href="{{ asset('assets/img/logo/favicon.png') }}" rel='icon' type='image/x-icon'/>
    <link rel="alternate" type="application/atom+xml" title="weddinc - Atom" href="LINK ATOM" />
    <link rel="alternate" type="application/rss+xml" title="Weddinc - RSS" href="LINK RSS" />
    <link rel="service.post" type="application/atom+xml" title="Weddinc - Atom" href="LINK ATOM" />
    <link rel="alternate" type="application/atom+xml" title="Weddinc - Atom" href="LINK ATOM" />
    <link href="{{ asset('assets/img/logo/favicon.png') }}" rel="image_src"/>
    <meta content='Wedding Planner & Vendor Commerce' name='description'/>
    <meta content='https://weddinc.id' property='og:url'/>
    <meta content='Wedding Planner & Vendor Commerce' property='og:title'/>
    <meta content='Wedding Planner & Vendor Commerce' property='og:description'/>
    <meta content="{{ asset('assets/img/logo/favicon.png') }}" property='og:image'/>
    <meta content='wedding planer, wedding organizer, vendor wedding, wo indonesia, wedding venue' name='keywords'/>
    <meta content='article' property='og:type'/>
    <meta content='Wedd.inc' property='og:site_name'/>
    <meta content='BING WEBMASTER' name='msvalidate.01'/>
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <link rel="shortcut icon" href="{{ asset('assets/img/logo/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/quicksand-googleapis.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/flaticon/font/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/icomoon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-custom.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-S7W604FJ2J"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-S7W604FJ2J');
</script>
  </head>
  <body>
    @section('navigation')
    <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    <header class="site-navbar py-4 bg-white" role="banner">
      <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    <header class="site-navbar py-2 bg-white pt-0" role="banner">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-11 col-lg-2">
            <h1 class="mb-0 site-logo"><a href="{{ route('home') }}" class="text-black h2 mb-0"><img src="{{ asset('assets/img/logo/logo.png') }}" alt="" class="img-fluid"></a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block pl-0">
                <li>
                  <div class="search-header">
                    <form class="form-inline form-sm mt-0">
                      <i class="fas fa-search" aria-hidden="true"></i>
                      <input class="form-control form-control-sm ml-3 w-90 border-top-0 border-left-0 border-right-0" type="text" placeholder="Search"
                        aria-label="Search">
                    </form>
                  </div>
                </li>
                <!--<li><a href="#" class="text-darkpink">Promo</a></li>
                <li><a href="plan.html" class="text-darkpink">Rencanaku</a></li>-->
                <li class="has-children">
                  <a href="#" class="text-dark"><i class="far fa-lg fa-bell"></i></a>
                  <ul class="dropdown">
                    <li><a href="#" class="dropdown-item text-darkpink p-1">Promo menarik agustusan!</a></li>
                    <li><a href="#" class="dropdown-item text-darkpink p-1">Cashback super banyak!</a></li>
                    <li><a href="#" class="dropdown-item text-darkpink p-1">Transaksi pertama bonus hadiah!</a></li>
                    <li><a href="#" class="dropdown-item text-darkpink p-1">Dikson nikah 10% bulan Januari</a></li>
                    <li class="dropdown-divider"></li>
                    <li><a href="{{ route('notification.user') }}" class="dropdown-item text-darkpink p-1">Selengkapnya</a></li>
                  </ul>
                </li>
                <!-- <li class="has-children">
                  <a href="#" class="text-darkpink"><img src="../assets/img/icon/flag/id.png" style="height: 25px; width: 25px;"> ID</a>
                  <ul class="dropdown">
                    <li><a href="{{ route('login') }}" class="text-darkpink"><img src="../assets/img/icon/flag/id.png" style="height: 25px; width: 25px;"> Indonesian</a></li>
                    <li><a href="#" class="text-darkpink"><img src="../assets/img/icon/flag/en.png" style="height: 25px; width: 25px;"> English</a></li>
                  </ul>
                </li> -->
                <li class="has-children">
                  <a href="#" class="text-darkpink">{{ auth()->user()->name }}</a>
                  <ul class="dropdown">
                    <li><a href="{{ route('account.user') }}" class="text-darkpink"><i class="fa fa-cog"></i> Akun Saya</a></li>
                    <li><a href="{{ route('account_detail.user') }}" class="text-darkpink"><i class="fa fa-user"></i> Lihat Profil</a></li>
                    <li><a href="{{ route('cart.user') }}" class="text-darkpink"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                    <li><a class="dropdown-item text-darkpink" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>
                            {{ __('Logout') }}
                    </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    <!-- <li><a href="../home" class="text-darkpink"><i class="fas fa-sign-out-alt"></i> Keluar</a></li> -->
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
          <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>
          </div>
        </div>
      </div>
    </header>
        @show
              
        @yield('content')
                
        @section('footer')
        <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h5>Perusahaan</h5>
                    <ul>
                        <li>- <a href="#">Karir</a></li>
                        <li>- <a href="https://blog.weddinc.id">Blog</a></li>
                        <li>- <a href="{{ route('terms_condition.user') }}">Syarat dan Ketentuan</a></li>
                        <li>- <a href="{{ route('privacy_policy.user') }}">Kebijakan Privasi</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Bisnis</h5>
                    <ul>
                        <li>- <a href="#">Pusat Penjual</a></li>
                        <li>- <a href="#">Pusat Pengguna</a></li>
                        <li>- <a href="{{ route('faq.user') }}">FAQ</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Dukungan</h5>
                    <ul>
                        <li>- <a href="{{ route('about.user') }}">Tentang Weddinc</a></li>
                        <li>- <a href="#">Kategori</a></li>
                        <li>- <a href="#">Promo</a></li>
                        <li>- <a href="{{ route('contact_us.user') }}">Hubungi Kami</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 info">
                    <h5>Tentang Kami</h5>
                    <p>Weddinc adalah platform Teknologi Wedding Planner & Commerce yang bertujuan untuk mewujudkan impian pernikahan anda.</p>
                </div>
            </div>
        </div>
        <div class="second-bar">
            <div class="container">
            <div class="row">
                <div class="col-6">
                    <h2 class="logo"><a href="{{ route('home') }}"><img src="{{ asset('assets/img/logo/logo-alt.png') }}" width="168" height="35"></a></h2>
                </div>
                <div class="col-6">
                    <div class="col-sm-12 my-auto">
                        <ul class="social-media">
                            <li><a href="https://www.instagram.com/weddinc.id" class="p-2"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </footer>
  </div>
    @show

  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/js/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/js/aos.js') }}"></script>
  <script src="{{ asset('assets/js/typed.js') }}"></script>
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script src="{{ asset('assets/js/baguetteBox.min.js') }}"></script>
  <script src="{{ asset('assets/lib/jquery/jquery.mask.min.js') }}"></script>

    @yield('js') <!-- Make your custom JavaScript -->

    <script type="text/javascript">
    $(document).ready(function() {
      $('#dataTables').DataTable();
    } );
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.money').mask('0.000.000.000', {reverse: true});
      $('.phone').mask('0000−0000−0000');
      $('.year').mask('0000/0000');
    })
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
        console.log("document is ready");
        $( ".cardhover" ).hover(
            function() {
                $(this).addClass('border-danger').css('cursor', 'pointer'); 
            }, function() {
                $(this).removeClass('border-danger');
            }
            );
    });
  </script>
  <script>
    function calculate_weddingbudget() {
    let budgetinput = document.getElementById("budget").value;
    let budget = budgetinput.split('.').join('');
    let guestinput = document.getElementById("guest").value;
    let guest = guestinput.split('.').join('');
    var resepsi = +budget * 0.15;
    var prosesi = +budget * 0.02;
    var menu_utama = +budget * 0.2;
    var pondokan = +budget * 0.05;
    var wedding_cake = +budget * 0.02;
    var dekorasi = +budget * 0.13;
    var busana_wanita = +budget * 0.05;
    var rias = +budget * 0.03;
    var busana_pria = +budget * 0.03;
    var mc = +budget * 0.08;
    var dok_prewed = +budget * 0.04;
    var dok_foto = +budget * 0.04;
    var dok_video = +budget * 0.02;
    var mobil = +budget * 0.04;
    var mahar = +budget * 0.03;
    var undangan = +budget * 0.01;
    var undangan_pcs = +undangan / guest;
    var souvenir = +budget * 0.03;
    var souvenir_pcs = +souvenir / guest;
    var biaya_takterduga = +budget * 0.04;
    var total = +budget * 1;
    document.getElementById("resepsi").innerHTML = 'Rp.' + resepsi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("prosesi").innerHTML = 'Rp.' + prosesi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("menu_utama").innerHTML = 'Rp.' + menu_utama.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("pondokan").innerHTML = 'Rp.' + pondokan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("wedding_cake").innerHTML = 'Rp.' + wedding_cake.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dekorasi").innerHTML = 'Rp.' + dekorasi.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("busana_wanita").innerHTML = 'Rp.' + busana_wanita.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("rias").innerHTML = 'Rp.' + rias.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("busana_pria").innerHTML = 'Rp.' + busana_pria.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mc").innerHTML = 'Rp.' + mc.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_prewed").innerHTML = 'Rp.' + dok_prewed.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_foto").innerHTML = 'Rp.' + dok_foto.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("dok_video").innerHTML = 'Rp.' + dok_video.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mobil").innerHTML = 'Rp.' + mobil.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("mahar").innerHTML = 'Rp.' + mahar.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("undangan").innerHTML = 'Rp.' + undangan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("undangan_pcs").innerHTML = '(Rp.' + undangan_pcs.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '/undangan)';
    document.getElementById("souvenir").innerHTML = 'Rp.' + souvenir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("souvenir_pcs").innerHTML = '(Rp.' + souvenir_pcs.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '/undangan)';
    document.getElementById("biaya_takterduga").innerHTML = 'Rp.' + biaya_takterduga.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    document.getElementById("total").innerHTML = 'Rp.' + total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }
  </script>
  <script type="text/javascript">
    $(function() {
      $('.example1').accordion({ multiOpen: true });
    });
  </script>
  <script type="text/javascript">
    $('.budgetting a').click(function (e) {
    e.preventDefault();
    var href = $(this).attr('href');
      $('.budgetting-tab').fadeOut(1000).promise().done(function(){
        $(href).fadeIn(1000);
      });
  });
  </script>
  <script>
    $(document).ready(function(){
      $(".slide-toggle").click(function(){
        $(".planner1").animate({width: "toggle"}, 500);
        $(".planner2").animate({width: "toggle"}, 500);
      });
    });
  </script>
  <script>
  var countDownDate = new Date("Nov 23, 2019 13:00:00").getTime();
  var x = setInterval(function() {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    document.getElementById("days").innerHTML = days;
    document.getElementById("hours").innerHTML = hours;
    document.getElementById("minutes").innerHTML = minutes;
    document.getElementById("seconds").innerHTML = seconds;
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("days").innerHTML = "EXPIRED";
      document.getElementById("hours").innerHTML = "EXPIRED";
      document.getElementById("minutes").innerHTML = "EXPIRED";
      document.getElementById("seconds").innerHTML = "EXPIRED";
    }
  }, 1000);
  </script>
  <script type="text/javascript">
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            else{ window.location.assign("https://weddinc.id/");
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
  </script>