@extends('layouts.homepage2_layout')

@section('navigation')
    @parent
@endsection

@section('content')
<section class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center mb-5"><img src="{{ asset('assets/img/kembangkan_bisnismu.png') }}" style="height: 40px; width: auto; "></div>
          <div class="row col-md-12 bg-light">
            <div class="col-md-6 offset-md-1 no-gutter">
              <div class="col-12 rounded">
                  <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link" id="nav-registration-tab" data-toggle="tab" href="#nav-registration" role="tab" aria-controls="nav-planner" aria-selected="true">Login Vendor</a>
                    </div>
                  </nav>
                    <div class="tab-content py-3 px-3 px-sm-0 bg-light" id="nav-tabContent">
                    <div class="tab-pane fade show active bg-light" id="nav-login" role="tabpanel" aria-labelledby="nav-signup-tab">
                      <div class="row">
                        <div class="col-md-9 mx-auto">
                          <div class="card card-signin my-5">
                            <div class="card-body">
                              <h5 class="card-title text-center">Masuk</h5>
                              <form action="{{ route('login.owner') }}" method="POST" class="needs-validation" novalidate>
                                @csrf
                                <div class="form-row row">
                                  <div class="input-group col-md-12 mt-3">
                                    <div class="col-md-12">
                                      <input type="email" name="email" class="form-control" placeholder="email" required>
                                      <div class="valid-feedback"><i><small>valid</small></i></div>
                                      <div class="invalid-feedback"><i><small>masukkan email</small></i></div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-row row">
                                  <div class="input-group col-md-12 mt-3">
                                    <div class="col-md-12">
                                      <input type="password" name="password" class="form-control" placeholder="password" required>
                                      <div class="valid-feedback"><i><small>valid</small></i></div>
                                      <div class="invalid-feedback"><i><small>masukkan password</small></i></div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-row row">
                                  <div class="input-group col-md-12 mt-2 justify-content-center">
                                    <div class="custom-control custom-checkbox mb-3 mt-1">
                                      <input type="checkbox" class="custom-control-input" id="customCheck1">
                                      <label class="custom-control-label" for="customCheck1">Ingat kata sandi</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-row row">
                                  <div class="col-md-12 mt-2">
                                    <div class="row justify-content-center">
                                      <button type="submit" class="btn btn-tosca btn-block mr-3 ml-3 text-uppercase">Masuk</button>
                                    </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4 no-gutter sidebar-item">
              <div class="make-me-sticky">
                <div id="demo" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="{{ asset('assets/img/login/signup_1.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Kembangkan bisnis Anda menuju dunia luas</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="{{ asset('assets/img/login/signup_2.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Cinta sejati adalah petualangan terbesar.</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="{{ asset('assets/img/login/signup_3.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Jika rencanamu tidak berjalan, ganti sekarang juga.</p>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
          </div>
          <div class="col-md-12 text-center">
          <div class="card-footer">Dipersembahkan oleh Weddinc © <?php echo date("Y"); ?></div>
          </div>
        </div>
    </div>
</section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->
<script type="text/javascript" language="javascript">
    function checkformlogin()
    {
        var f = document.forms["formlogin"].elements;
        var cansubmit = true;
        for (var i = 0; i < f.length; i++)
          {if (f[i].value.length == 0) cansubmit = false;}
        if (cansubmit) {document.getElementById('submitlogin').disabled = false;}
        else {document.getElementById('submitlogin').disabled = 'disabled';}
    }
    function checkformregister()
    {
        var f = document.forms["formregister"].elements;
        var cansubmit = true;
        for (var i = 0; i < f.length; i++)
          {if (f[i].value.length == 0) cansubmit = false;}
        if (cansubmit) {document.getElementById('submitregister').disabled = false;}
        else {document.getElementById('submitregister').disabled = 'disabled';}
    }
</script>
<script type="text/javascript">
    (function() {
        'use strict';
        window.addEventListener('load', function() {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            else{ window.location.assign("https://weddinc.id/");
            }
            form.classList.add('was-validated');
            }, false);
        });
        }, false);
    })();
</script>
@endsection

@section('footer')
  @parent
@endsection