@extends('layouts.homepage2_layout')

@section('navigation')
    @parent
@endsection

@section('content')
<section class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center mb-5"><img src="{{ asset('assets/img/kembangkan_bisnismu.png') }}" style="height: 40px; width: auto; "></div>
          <div class="row col-md-12 bg-light">
            <div class="col-md-6 offset-md-1 no-gutter">
              <div class="col-12 rounded">
                <nav>
                  <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link" id="nav-registration-tab" data-toggle="tab" href="#nav-registration" role="tab" aria-controls="nav-planner" aria-selected="true">Registrasi Vendor</a>
                    </div>
                </nav>
                <form action="{{ route('register.owner') }}" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
                  @csrf
                  <div class="tab-content py-3 px-3 px-sm-0 bg-light" id="nav-tabContent">
                    <div class="tab-pane fade show active bg-light" id="nav-registration" role="tabpanel" aria-labelledby="nav-registration-tab">
                      <div class="accordion" id="myAccordion">
                        <div class="card">
                          <a data-toggle="collapse" data-target="#collapseOne" style="cursor: pointer;">
                            <div class="card-header bg-tosca p-4" id="headingOne">
                              <h6 class="mb-0 text-white">1. Profil Vendor</h6>
                            </div>
                          </a>
                          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#myAccordion">
                            <div class="card-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td>Nama Vendor</td>
                                        <td class="input-group">
                                            <input type="text" class="form-control" name="name_vendor" placeholder="nama vendor" required="">
                                            <div class="valid-feedback"><i><small>valid</small></i></div>
                                            <div class="invalid-feedback"><i><small>masukkan nama vendor</small></i></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kategori Vendor</td>
                                        <td class="input-group">
                                            <div class="dropdown bootstrap-select show-tick form-control" style="width: 100%;">
                                                <select class="selectpicker show-tick form-control" name="category" data-width="100%" data-container="body" data-live-search="true" title="Pilih kategori" data-hide-disabled="true" required="" tabindex="-98">
                                                    @foreach($category as $value)
                                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="valid-feedback"><i><small>valid</small></i></div>
                                            <div class="invalid-feedback"><i><small>pilih kategori vendor</small></i></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Pilih area layanan.</td>
                                    </tr>
                                    <tr><td>
                                        </td><td class="input-group">
                                            <div class="dropdown bootstrap-select show-tick form-control" style="width: 100%;">
                                                <select class="selectpicker show-tick form-control" name="area" data-width="100%" data-container="body" data-live-search="true" title="Pilih area layanan" data-hide-disabled="true" required="" tabindex="-98">
                                                    @foreach($city as $value)
                                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="valid-feedback"><i><small>valid</small></i></div>
                                            <div class="invalid-feedback"><i><small>pilih area layanan</small></i></div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <a data-toggle="collapse" data-target="#collapseTwo" style="cursor: pointer;">
                            <div class="card-header bg-tosca p-4" id="headingTwo">
                              <h6 class="mb-0 text-white">2. Data Personal</h6>
                            </div>
                          </a>
                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#myAccordion">
                            <div class="card-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td>PIC</td>
                                        <td class="input-group">
                                            <input type="text" class="form-control" name="name" placeholder="nama" required="">
                                            <div class="valid-feedback"><i><small>valid</small></i></div>
                                            <div class="invalid-feedback"><i><small>masukkan nama pemilik</small></i></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Telepon</td>
                                        <td class="input-group">
                                            <input type="text" class="form-control" name="no_telp" placeholder="telepon" required="">
                                            <div class="valid-feedback"><i><small>valid</small></i></div>
                                            <div class="invalid-feedback"><i><small>masukkan nomor telepon</small></i></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td class="input-group">
                                            <input type="email" class="form-control" name="email" placeholder="email" required="">
                                            <div class="valid-feedback"><i><small>valid</small></i></div>
                                            <div class="invalid-feedback"><i><small>masukkan email</small></i></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Password</td>
                                        <td class="input-group">
                                            <input type="password" class="form-control" name="password" placeholder="password" required="">
                                            <div class="valid-feedback"><i><small>valid</small></i></div>
                                            <div class="invalid-feedback"><i><small>masukkan password</small></i></div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                          </div>
                        </div>
                        <div class="panel mt-3 mb-3 text-center">
                          <button class="col-md-6 offset-md-3 btn btn-tosca btn-block text-uppercase" type="button" data-toggle="modal" data-target="#modalprivacy">Daftar</button>
                          <p class="mt-3">atau <a href="{{ route('loginPage.owner') }}">sudah memiliki akun</a>?</p>
                        </div>
                        <div class="modal" id="modalprivacy">
                          <div class="modal-dialog modal-dialog-scrollable text-left">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h1 class="modal-title">Kebijakan Privasi</h1>
                                <button type="button" class="close" data-dismiss="modal">×</button>
                              </div>
                              <div class="modal-body">
                                <div class="col-md-12">
                                  @foreach ($privacypolicy_vendor as $privacypolicy_vendor)
                                    {!! $privacypolicy_vendor->content !!}
                                  @endforeach
                                </div>
                                </div>
                                <div class="modal-footer custom-control custom-checkbox justify-content-between ml-3">
                                  <div class="form-group">
                                    <div class="form-check">
                                      <input type="checkbox" class="custom-control-input" id="customCheck2" required>
                                      <label class="custom-control-label" for="customCheck2">Setuju dengan kebijakan privasi</label>
                                      <div class="invalid-feedback">
                                        Anda harus menyetujui kebijakan privasi.
                                      </div>
                                    </div>
                                  </div>
                                  <input class="btn btn-lg btn-tosca btn-block text-uppercase" id="btnRegis" type="submit" value="Daftar" disabled="disabled"/>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-md-4 no-gutter sidebar-item">
              <div class="make-me-sticky">
                <div id="demo" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="{{ asset('assets/img/login/signup_1.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Kembangkan bisnis Anda menuju dunia luas</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="{{ asset('assets/img/login/signup_2.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Cinta sejati adalah petualangan terbesar.</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="{{ asset('assets/img/login/signup_3.jpg') }}" alt="Weddinc" width="100%">
                      <div class="carousel-caption card-body bg-white text-dark rounded" style="opacity: 0.7">
                        <p>Jika rencanamu tidak berjalan, ganti sekarang juga.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 text-center">
          <div class="card-footer">Dipersembahkan oleh Weddinc © <?php echo date("Y"); ?></div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('footer')
  @parent
@endsection

@section('js') <!-- Make your custom JavaScript -->
<script type="text/javascript" language="javascript">
  function checkformlogin()
  {
      var f = document.forms["formlogin"].elements;
      var cansubmit = true;
      for (var i = 0; i < f.length; i++)
        {if (f[i].value.length == 0) cansubmit = false;}
      if (cansubmit) {document.getElementById('submitlogin').disabled = false;}
      else {document.getElementById('submitlogin').disabled = 'disabled';}
  }
  function checkformregister()
  {
      var f = document.forms["formregister"].elements;
      var cansubmit = true;
      for (var i = 0; i < f.length; i++)
        {if (f[i].value.length == 0) cansubmit = false;}
      if (cansubmit) {document.getElementById('submitregister').disabled = false;}
      else {document.getElementById('submitregister').disabled = 'disabled';}
  }
</script>
<script type="text/javascript">
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      var forms = document.getElementsByClassName('needs-validation');
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          else{ window.location.assign("https://weddinc.id/");
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
<script>
function ShowHideDiv() {
  var chkLain = document.getElementById("chkLain");
  var statusRek3 = document.getElementById("statusRek3");
  var statusRek1 = document.getElementById("statusRek1");
  var statusRek2 = document.getElementById("statusRek2");

  if(chkLain.checled){
    console.log('yo');
  }

  statusRek3.style.display = chkLain.checked ? "" : "none";
  statusRek1.style.display = chkLain.checked ? "" : "none";
  statusRek2.style.display = chkLain.checked ? "" : "none";
}

var checker = document.getElementById('customCheck2');
var sendbtn = document.getElementById('btnRegis');

checker.onchange = function(){
  if(this.checked){
      sendbtn.disabled = false;
  } else {
      sendbtn.disabled = true;
  }
}
</script>
@endsection

@section('footer')
  @parent
@endsection
