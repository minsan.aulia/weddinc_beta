<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vendor_id')->unsigned();
            $table->string('wa', 50)->nullable();
            $table->string('telp', 50)->nullable();
            $table->string('fb', 50)->nullable();
            $table->string('twitter', 50)->nullable();
            $table->string('ig', 50)->nullable();
            $table->timestamps();
        });

        Schema::table('contact_vendors', function($table) {
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_vendors');
    }
}
