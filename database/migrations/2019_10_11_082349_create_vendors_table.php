<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('owner_id')->unsigned();
            $table->string('name', 100);
            $table->string('category', 50);
            $table->string('type', 15)->default('FREE');
            $table->string('area', 50);
            $table->boolean('activated')->default(0)->comment('0 = not yet, 1 = yes');
            $table->timestamp('joined_at')->useCurrent();
            $table->timestamps();
        });

        Schema::table('vendors', function($table) {
            $table->foreign('owner_id')->references('id')->on('owners')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
