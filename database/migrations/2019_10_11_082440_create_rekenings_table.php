<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekenings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('owner_id')->unsigned();
            $table->bigInteger('vendor_id')->unsigned();
            $table->string('name_bank', 25)->nullable();
            $table->string('no_rek')->nullable();
            $table->string('name_owner_bank')->nullable();
            $table->string('dp', 3)->nullable();
            $table->string('photo_rek', 50)->nullable();
            $table->boolean('status_rek')->comment('0 = personal, 1 = other')->nullable();
            $table->string('photo_suratKuasa')->nullable();
            $table->timestamps();
        });

        Schema::table('rekenings', function($table) {
            $table->foreign('owner_id')->references('id')->on('owners')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekenings');
    }
}
