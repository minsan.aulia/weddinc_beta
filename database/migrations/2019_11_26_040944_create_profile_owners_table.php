<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_owners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('owner_id')->unsigned();
            $table->string('name', 50);
            $table->string('call_name', 10)->nullable();
            $table->string('no_telp', 14)->nullable();
            $table->string('address')->nullable();
            $table->string('city', 50)->nullable();
            $table->string('type_identity', 15)->nullable();
            $table->string('no_identity', 50)->nullable();
            $table->string('photo_identity')->nullable();
            $table->string('photo_withIdentity')->nullable();
            $table->string('no_npwp', 50)->nullable();
            $table->string('photo_npwp')->nullable();
            $table->timestamps();
        });

        Schema::table('profile_owners', function($table) {
            $table->foreign('owner_id')->references('id')->on('owners')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_owners');
    }
}
