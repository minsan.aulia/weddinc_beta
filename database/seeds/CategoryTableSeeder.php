<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            ['name'=>'Make up & Hairdo'],
            ['name'=>'Documentation'],
            ['name'=>'Wedding Organizer'],
            ['name'=>'MC'],
            ['name'=>'Music'],
            ['name'=>'Jewelry'],
            ['name'=>'Wedding Cake'],
            ['name'=>'Suvenir & Gift'],
        ];

        foreach ($category as $key => $value) {
            Category::create($value);
        }
    }
}
