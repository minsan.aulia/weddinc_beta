<?php

use App\City;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = [
            ['name'=>'Jakarta'],
            ['name'=>'Bogor'],
            ['name'=>'Tangerang'],
            ['name'=>'Bekasi'],
            ['name'=>'Depok'],
        ];

        foreach ($city as $key => $value) {
            City::create($value);
        }
    }
}
