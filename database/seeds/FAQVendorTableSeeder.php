<?php

use App\Faq;
use Illuminate\Database\Seeder;

class FAQVendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faqvendor = [
            ['faq_for' => '1', 'faq_category_id' => '8', 'ask' => 'Bagaimana Cara Mengoptimasi Halaman Profil Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '8', 'ask' => 'Bagaimana Cara Mempublikasikan Halaman Profil Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '8', 'ask' => 'Apa Itu Profile Strength Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '8', 'ask' => 'Apa Itu Klien Potensial?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '8', 'ask' => 'Bagaimana Cara Submit Portfolio ke Halaman Profil Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '9', 'ask' => 'Bagaimana Cara Menjadi Partner Weddinc?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '9', 'ask' => 'Perangkat Apa yang Kompatibel Dengan Aplikasi Weddinc?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '9', 'ask' => 'Bagaimana Cara Masuk ke Weddinc?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '9', 'ask' => 'Apakah Ada Biaya Untuk Menjadi Partner Weddinc?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '9', 'ask' => 'Apa Saja yang Termasuk Dalam Paket Trial Akun Premium?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '9', 'ask' => 'Kapan Paket Trial Akun Premium Berakhir?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '9', 'ask' => 'Berapa Lama Waktu yang Dibutuhkan Agar Akun Dapat Terpublikasi?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '10', 'ask' => 'Jika Vendor Registrasi Sebagai Pengguna, Apa yang Terjadi?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '10', 'ask' => 'Bagaimana Cara Login?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '10', 'ask' => 'Bagaimana Cara Mengubah Detail Akun?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '10', 'ask' => 'Siapa Yang Dapat Login ke Akun Vendor Saya?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '10', 'ask' => 'Bagaimana Cara Menambahkan Foto Profil Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '11', 'ask' => 'Apa Itu Akun Premium Weddinc?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '11', 'ask' => 'Berapa Harga Akun Premium Weddinc?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '11', 'ask' => 'Apa Keuntungan Dari Memiliki Akun Premium Weddinc?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '11', 'ask' => 'Bagaimana Cara Membayar Akun Premium Weddinc?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '11', 'ask' => 'Apabila Saya Memiliki Lebih Dari Satu Vendor, Apa Yang Harus Saya Lakukan?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '11', 'ask' => 'Dapatkah Saya Meningkatkan atau Menurunkan Tingkat Akun Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '11', 'ask' => 'Dapatkah Saya Membatalkan Akun Premium Setelah Menyelesaikan Pembayaran?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '11', 'ask' => 'Dapatkah Saya Membatalkan Akun Premium Ketika Akun Premium Sedang Aktif?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '12', 'ask' => 'Bagaimana Cara Mengupload Portofolio Pada Halaman Profil Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '12', 'ask' => 'Bagaimana Cara Menyisipkan Video ke Halaman Profil Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '12', 'ask' => 'Apa Itu Penandaan Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '12', 'ask' => 'Dapatkah Saya Menambah Portfolio Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '12', 'ask' => 'Apa Yang Dilarang Dipublikasikan Pada Halaman Profil Vendor?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '13', 'ask' => 'Apa Itu WWW (World Wide Weddinc)?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '13', 'ask' => 'Bagaimana Cara Mendapat Pembaharuan Dari WWW (World Wide Weddinc)?', 'question' => ''],
            ['faq_for' => '1', 'faq_category_id' => '13', 'ask' => 'Bagaimana Cara Mengikuti Kegiatan WWW (World Wide Weddinc)?', 'question' => ''],
        ];

        foreach ($faqvendor as $key => $value)
        {
            Faq::create($value);
        }
    }
}
