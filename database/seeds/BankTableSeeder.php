<?php

use App\Bank;
use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bank = [
            ['name' => 'BRI', 'code' => '002'],
            ['name' => 'MANDIRI', 'code' => '008'],
            ['name' => 'BNI', 'code' => '009'],
            ['name' => 'BCA', 'code' => '014'],
        ];

        foreach ($bank as $key => $value)
        {
            Bank::create($value);
        }
    }
}
