<?php

use App\FaqCategory;
use Illuminate\Database\Seeder;

class FAQCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faqcategory = [
            ['id' => '1', 'categories_for' => '0', 'name' => 'Intro'],
            ['id' => '2', 'categories_for' => '0', 'name' => 'Akun'],
            ['id' => '3', 'categories_for' => '0', 'name' => 'Aplikasi'],
            ['id' => '4', 'categories_for' => '0', 'name' => 'World Wide Weddinc (WWW)'],
            ['id' => '5', 'categories_for' => '0', 'name' => 'Kalkulator'],
            ['id' => '6', 'categories_for' => '0', 'name' => 'Temukan Vendor'],
            ['id' => '7', 'categories_for' => '0', 'name' => 'Blog & Updates'],
            ['id' => '8', 'categories_for' => '1', 'name' => 'Umum'],
            ['id' => '9', 'categories_for' => '1', 'name' => 'Partnership'],
            ['id' => '10', 'categories_for' => '1', 'name' => 'Akun'],
            ['id' => '11', 'categories_for' => '1', 'name' => 'Akun Premium'],
            ['id' => '12', 'categories_for' => '1', 'name' => 'Vendor Project'],
            ['id' => '13', 'categories_for' => '1', 'name' => 'World Wide Weddinc (WWW)'],
        ];

        foreach ($faqcategory as $key => $value)
        {
            FaqCategory::create($value);
        }
    }
}
