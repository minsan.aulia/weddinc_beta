<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CityTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(BankTableSeeder::class);
         $this->call(DocumentTableSeeder::class);
         $this->call(FAQCategoriesTableSeeder::class);
         $this->call(FAQUserTableSeeder::class);
         $this->call(FAQVendorTableSeeder::class);
    }
}
